#include "ForwardEulerSceneSolver.h"

namespace MiniEngine {

void ForwardEulerSceneSolver::solve(
    const float dt, const std::vector<std::shared_ptr<PhysicsBody>>& bodies,
    const std::vector<std::shared_ptr<ForceField>>& forceSources)
{
    std::vector<glm::vec3> forces(bodies.size(), glm::vec3(0.0f));

    for (size_t iBody = 0; iBody < bodies.size(); iBody++)
    {
        for (const auto& source : forceSources)
        {
            forces[iBody] += source->computeForce(*bodies[iBody]);
        }
    }

    for (size_t iBody = 0; iBody < bodies.size(); iBody++)
    {
        auto& body = bodies[iBody];

        if (body->getMass() > 0)
        {
            const auto newVelocity = body->getVelocity() + forces[iBody] * (dt / body->getMass());
            body->setVelocity(newVelocity);
        }

        body->translate(body->getVelocity() * dt);

        std::cerr << "new pos: " << body->getPosition() << std::endl;
    }
}

} // namespace MiniEngine
