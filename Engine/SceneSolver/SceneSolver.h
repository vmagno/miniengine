#ifndef MINI_ENGINE_SCENE_SOLVER_H
#define MINI_ENGINE_SCENE_SOLVER_H

#include "Engine/ForceField/ForceField.h"
#include "Engine/PhysicsBody.h"

namespace MiniEngine {

class SceneSolver
{
public:
    virtual ~SceneSolver();
    virtual void solve(
        const float dt, const std::vector<std::shared_ptr<PhysicsBody>>& bodies,
        const std::vector<std::shared_ptr<ForceField>>& forceSources) = 0;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_SCENE_SOLVER_H
