#ifndef MINI_ENGINE_FORWARD_EULER_SCENE_SOLVER_H
#define MINI_ENGINE_FORWARD_EULER_SCENE_SOLVER_H

#include "SceneSolver.h"

namespace MiniEngine {

class ForwardEulerSceneSolver : public SceneSolver
{
public:
    void solve(
        const float dt, const std::vector<std::shared_ptr<PhysicsBody>>& bodies,
        const std::vector<std::shared_ptr<ForceField>>& forceSources) override;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_FORWARD_EULER_SCENE_SOLVER_H
