#ifndef MINI_ENGINE_EVENT_MANAGER_H
#define MINI_ENGINE_EVENT_MANAGER_H

#include <functional>
#include <memory>
#include <unordered_map>

#include "Engine/GlmInclude.h"

namespace MiniEngine {

struct MouseButtonState
{
    bool isLeftButtonPressed_   = false;
    bool isRightButtonPressed_  = false;
    bool isMiddleButtonPressed_ = false;
};

using KeypressTask   = std::function<void(const int)>;
using MouseClickTask = std::function<void(const int)>;
using MouseMoveTask =
    std::function<void(const glm::vec<2, double>&, const glm::vec<2, double>&, const MouseButtonState&)>;

class EventManager
{
    template <class... Args>
    class InputAction;

    using KeypressAction   = InputAction<const int>;
    using MouseClickAction = InputAction<const int>;
    using MouseMoveAction =
        InputAction<const glm::vec<2, double>&, const glm::vec<2, double>&, const MouseButtonState&>;

public:
    template <class Owner>
    void addKeypressEvent(const int key, const std::shared_ptr<Owner>& owner, const KeypressTask& task)
    {
        keypressActions_[key] = std::make_shared<KeypressWithOwner<Owner>>(owner, task);
    }

    void addKeypressEvent(const int key, const KeypressTask& task)
    {
        keypressActions_[key] = std::make_shared<KeypressAction>(task);
    }

    template <class Owner>
    void setMouseMoveEvent(const std::shared_ptr<Owner>& owner, const MouseMoveTask& task)
    {
        mouseMoveAction_ = std::make_shared<MouseMoveWithOwner<Owner>>(owner, task);
    }

    void handleKeypress(const int key, const int action) const;
    void handleMouseClick(const int button, const int action);
    void handleMouseMove(const double xPos, const double yPos);

    void setCursorPosition(const double xPos, const double yPos)
    {
        previousXPos_ = xPos;
        previousYPos_ = yPos;
    }

private:
    MouseButtonState mouseButtonState_;
    double           previousXPos_ = 0.0;
    double           previousYPos_ = 0.0;

    std::unordered_map<int, std::shared_ptr<KeypressAction>>   keypressActions_;
    std::unordered_map<int, std::shared_ptr<MouseClickAction>> mouseClickActions_;
    std::shared_ptr<MouseMoveAction>                           mouseMoveAction_;

private: // classes
    template <class... Args>
    class InputAction
    {
    public:
        InputAction(const std::function<void(Args...)>& task)
            : task_(task)
        {
        }

        inline void doTask(const Args... args) { task_(args...); }

    private:
        std::function<void(Args...)> task_;
    };

    template <class Owner>
    class KeypressWithOwner : public KeypressAction
    {
    public:
        KeypressWithOwner(const std::shared_ptr<Owner>& owner, const KeypressTask& task)
            : KeypressAction(task)
            , owner_(owner)
        {
        }

    private:
        std::shared_ptr<Owner> owner_;
    };

    template <class Owner>
    class MouseMoveWithOwner : public MouseMoveAction
    {
    public:
        MouseMoveWithOwner(const std::shared_ptr<Owner>& owner, const MouseMoveTask& task)
            : MouseMoveAction(task)
            , owner_(owner)
        {
        }

    private:
        std::shared_ptr<Owner> owner_;
    };
};

} // namespace MiniEngine

#endif // MINI_ENGINE_EVENT_MANAGER_H
