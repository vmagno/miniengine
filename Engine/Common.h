#ifndef MINI_ENGINE_COMMON_H
#define MINI_ENGINE_COMMON_H

#include <cstdint>
#include <limits>

namespace MiniEngine {

/// Invalid value for a 32-bit unsigned
const uint32_t INVALID = std::numeric_limits<uint32_t>::max();

namespace Global {

} // namespace Global

} // namespace MiniEngine

#endif // MINI_ENGINE_COMMON_H
