#include "VirtualScreen.h"

#include "Engine/DisplayItem.inl"
#include "Engine/Vertex/Vertex_tex.h"
#include "Engine/VulkanResources/Texture/VariableTexture.h"

namespace MiniEngine {

VirtualScreen::VirtualScreen(const std::shared_ptr<Vk::LogicalDevice>& device, const VirtualScreenParams& params)
    : DisplayItem(
          Vk::VariableTexture::create(device, params.resolution_, params.samplerOptions_, params.deviceOnly_),
          params.makeVertices(), params.makeIndices(), ModelParams())
{
}

VirtualScreen::~VirtualScreen()
{
}

const std::vector<Vertex_tex> VirtualScreenParams::makeVertices() const
{
    return {{{-coord_.x, -coord_.y, coord_.z}, {0.0f, 0.0f}},
            {{coord_.x, -coord_.y, coord_.z}, {1.0f, 0.0f}},
            {{coord_.x, coord_.y, coord_.z}, {1.0f, 1.0f}},
            {{-coord_.x, coord_.y, coord_.z}, {0.0f, 1.0f}}};
}

} // namespace MiniEngine
