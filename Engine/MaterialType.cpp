#include "MaterialType.h"

#include "Engine/VulkanCommon.h"

#include "Engine/Vertex/Vertex_col.h"
#include "Engine/Vertex/Vertex_col_tex.h"
#include "Engine/Vertex/Vertex_phong.h"
#include "Engine/Vertex/Vertex_tex.h"

namespace MiniEngine {

MaterialType::MaterialType(
    const std::shared_ptr<Vk::LogicalDevice>& device, const std::string& vertexShaderFile,
    const std::string& fragmentShaderFile, const VkVertexInputBindingDescription& bindingDesc,
    const std::vector<VkVertexInputAttributeDescription>& attribDesc, const std::vector<UboType>& uboTypes,
    const uint32_t numSamplers)
    : vertexShaderModule_(std::make_shared<Vk::ShaderModule>(device, vertexShaderFile))
    , fragmentShaderModule_(std::make_shared<Vk::ShaderModule>(device, fragmentShaderFile))
    , bindingDesc_(bindingDesc)
    , attribDesc_(attribDesc)
    , uboTypes_(uboTypes)
    , descriptorSetLayout_(std::make_shared<Vk::DescriptorSetLayout>(device, uboTypes.size(), numSamplers, 0))
{
}

static std::shared_ptr<MaterialType>
    createMaterialInstance(const std::shared_ptr<Vk::LogicalDevice>& device, const VertexInfo info)
{
    return std::make_shared<MaterialType>(
        device, Global::SHADER_DIR + info.vertexShaderFile_, Global::SHADER_DIR + info.fragmentShaderFile_,
        info.bindingDescription_, info.attributeDescriptions_, info.uboTypes_, info.numSamplers_);
}

const std::shared_ptr<MaterialType>
    MaterialType::createInstance(const std::shared_ptr<Vk::LogicalDevice>& device, const MatType type)
{
    switch (type)
    {
    case MatType::color: return createMaterialInstance(device, Vertex_col::getVertexInfo());
    case MatType::color_texture: return createMaterialInstance(device, Vertex_col_tex::getVertexInfo());
    case MatType::texture: return createMaterialInstance(device, Vertex_tex::getVertexInfo());
    case MatType::phong1Light: return createMaterialInstance(device, Vertex_phong::getVertexInfo());
    case MatType::none: return nullptr;
    }
}

} // namespace MiniEngine
