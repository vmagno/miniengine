#ifndef MINI_ENGINE_PRE_RENDERED_VIEW_H
#define MINI_ENGINE_PRE_RENDERED_VIEW_H

#include "View.h"

#include "Engine/VirtualScreen.h"
#include "Engine/VulkanResources/Texture/VariableTexture.h"

namespace MiniEngine {

class DisplayItem;

class PreRenderedView : public View
{
public:
    PreRenderedView(
        const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec2& offset, const glm::vec2& size,
        const std::shared_ptr<Scene>& scene, const std::shared_ptr<Camera>& camera);

    PreRenderedView(
        const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec2& offset, const glm::vec2& size,
        const VirtualScreenParams& screenParams, const std::shared_ptr<Scene>& scene,
        const std::shared_ptr<Camera>& camera);

    void createResourceSet(const std::shared_ptr<Vk::SwapChain>& swapChain) override;
    void updateBuffers(const uint32_t index) override;

    void updateScreen();

    const std::shared_ptr<Vk::VariableTexture>       getScreenTexture() const;
    const std::shared_ptr<Vk::HostAccessibleTexture> getHostScreenTexture() const;

private:
    std::shared_ptr<DisplayItem> screenRect_;
    VirtualScreenParams          screenParams_;
    std::shared_ptr<Camera>      virtualCamera_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_PRE_RENDERED_VIEW_H
