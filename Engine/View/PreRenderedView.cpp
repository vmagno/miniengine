#include "PreRenderedView.h"

#include "Engine/Camera/Camera.h"
#include "Engine/View/ViewResourceSet.h"

namespace MiniEngine {

PreRenderedView::PreRenderedView(
    const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec2& offset, const glm::vec2& size,
    const std::shared_ptr<Scene>& scene, const std::shared_ptr<Camera>& camera)
    : PreRenderedView(device, offset, size, VirtualScreenParams(), scene, camera)
{
}

PreRenderedView::PreRenderedView(
    const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec2& offset, const glm::vec2& size,
    const VirtualScreenParams& screenParams, const std::shared_ptr<Scene>& scene, const std::shared_ptr<Camera>& camera)
    : View(offset, size, scene, camera)
    , screenRect_(std::make_shared<VirtualScreen>(device, screenParams))
    , screenParams_(screenParams)
    , virtualCamera_(Camera::makeVirtualCamera(device, screenParams_))
{
}

void PreRenderedView::createResourceSet(const std::shared_ptr<Vk::SwapChain>& swapChain)
{
    if (screenRect_ == nullptr)
        screenRect_ = std::make_shared<VirtualScreen>(swapChain->getDevice(), screenParams_);

    resourceSet_ = std::make_shared<ViewResourceSet>(
        swapChain->getDevice(), swapChain, offset_, size_, std::vector<std::shared_ptr<DisplayItem>>({screenRect_}),
        std::vector<UboType>({UboType::camera}));
    resourceSet_->updateUniformBuffers(
        virtualCamera_, static_cast<float>(screenParams_.resolution_.x) / screenParams_.resolution_.y);
}

void PreRenderedView::updateBuffers(const uint32_t /* index */)
{
}

void PreRenderedView::updateScreen()
{
    getScreenTexture()->updateTexture();
}

const std::shared_ptr<Vk::VariableTexture> PreRenderedView::getScreenTexture() const
{
    return std::static_pointer_cast<Vk::VariableTexture>(screenRect_->getTexture());
}

const std::shared_ptr<Vk::HostAccessibleTexture> PreRenderedView::getHostScreenTexture() const
{
    if (screenParams_.deviceOnly_)
    {
        std::cerr << "[PreRenderedView] The virtual screen is available to the device only. Cannot return a pointer to "
                     "a host-accessible one."
                  << std::endl;
        return nullptr;
    }

    return std::static_pointer_cast<Vk::HostAccessibleTexture>(screenRect_->getTexture());
}

} // namespace MiniEngine
