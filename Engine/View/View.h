#ifndef MINI_ENGINE_VIEW_H
#define MINI_ENGINE_VIEW_H

#include <memory>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/SwapChain.h"

namespace MiniEngine {

class Camera;
class Scene;
class ViewResourceSet;

class View
{
public:
    View() = delete;
    View(
        const glm::vec2& offset, const glm::vec2& size, const std::shared_ptr<Scene>& scene,
        const std::shared_ptr<Camera>& camera);
    virtual ~View() {}

    inline const std::shared_ptr<Scene>&           getScene() const { return scene_; }
    inline const std::shared_ptr<Camera>&          getCamera() const { return camera_; }
    inline const std::shared_ptr<ViewResourceSet>& getResourceSet() const { return resourceSet_; }

    virtual void createResourceSet(const std::shared_ptr<Vk::SwapChain>& swapChain);

    /**
     * @brief Update the command buffers (for the render pass) and the uniform buffers (for shader information)
     * @param index Position of the desired image within the swap chain
     */
    virtual void updateBuffers(const uint32_t index);

protected:
    glm::vec2 offset_; //!< Position of the view relative to the whole window (should be between 0 and 1)
    glm::vec2 size_;   //!< Width and height of the view relative to the whole window (should be between 0 and 1)

    std::shared_ptr<Scene>           scene_;
    std::shared_ptr<Camera>          camera_;
    std::shared_ptr<ViewResourceSet> resourceSet_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VIEW_H
