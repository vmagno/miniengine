#ifndef MINI_ENGINE_RESOURCE_SET_H
#define MINI_ENGINE_RESOURCE_SET_H

#include <memory>
#include <unordered_map>
#include <vector>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/CommandBuffer.h"
#include "Engine/VulkanResources/Pipeline/GraphicsPipeline.h"
#include "Engine/VulkanResources/SwapChain.h"

namespace MiniEngine {

enum class MatType;
enum class UboType;

class Camera;
class DisplayItem;

class ViewResourceSet
{
public:
    ViewResourceSet() = delete;
    ViewResourceSet(
        const std::shared_ptr<Vk::LogicalDevice>& device, const std::shared_ptr<Vk::SwapChain>& swapChain,
        const glm::vec2& offset, const glm::vec2& size, const std::vector<std::shared_ptr<DisplayItem>>& displayItems,
        const std::vector<UboType>& uboTypes);

    inline const std::shared_ptr<Vk::CommandBuffer>& getCommandBuffer(const uint32_t index)
    {
        return commandBuffers_[index];
    }

    void createBuffers(
        const glm::vec2& offset, const glm::vec2& size, const std::vector<std::shared_ptr<DisplayItem>>& displayItems,
        const std::vector<UboType>& uboTypes);

    /**
     * @brief Record the commands that will be executed during the render pass.
     * @param offsetRel Offset of the corner of the render area with respect to the window (between 0 and 1 in either
     * dimension)
     * @param sizeRel Size of the render area with respect to the window (between 0 and 1 in either dimension)
     * @param displayItems List of items that will be rendered (provide their position + their vertex/index buffers
     * @param imageIndex The desired position within the swap chain
     * @param doCreate Whether we are recording the commands for the first time. If true, the CommandBuffer objects will
     * be created. If false the command buffers will simply be reset.
     */
    void recordCommandBuffer(
        const glm::vec2& offsetRel, const glm::vec2& sizeRel,
        const std::vector<std::shared_ptr<DisplayItem>>& displayItems, const uint32_t imageIndex, const bool doCreate);
    void updateUniformBuffer(const uint32_t index, const std::shared_ptr<Camera>& camera, const float viewRatio);
    void updateUniformBuffers(const std::shared_ptr<Camera>& camera, const float viewRatio);

private:
    std::shared_ptr<Vk::LogicalDevice>                 device_;    //!< Device on which the resources are allocated
    std::shared_ptr<Vk::SwapChain>                     swapChain_; //!< Swap chain used to present images
    std::vector<std::shared_ptr<Vk::GraphicsPipeline>> pipelines_;
    std::shared_ptr<Vk::DescriptorPool>                descriptorPool_;
    std::vector<std::vector<std::shared_ptr<Vk::DescriptorSet>>>                 descriptorSets_;
    std::unordered_map<MatType, std::vector<std::shared_ptr<Vk::DescriptorSet>>> descriptorSetMap_;
    std::vector<std::shared_ptr<Vk::CommandBuffer>>                              commandBuffers_;
    std::unordered_map<UboType, std::vector<std::shared_ptr<Vk::UniformBuffer>>> uniformBufferMap_;

private: // functions
    void createUniformBuffers(const uint32_t count, const std::vector<UboType>& uboTypes);
    void createDescriptorSets(
        const uint32_t numImages, const std::vector<std::shared_ptr<Vk::GraphicsPipeline>>& pipelines,
        const std::vector<std::shared_ptr<DisplayItem>>& items);
};

} // namespace MiniEngine

#endif // MINI_ENGINE_RESOURCE_SET_H
