#include "ViewResourceSet.h"

#include <iostream>

#include "Engine/BasicUbo.h"
#include "Engine/Camera/Camera.h"
#include "Engine/DisplayItem.h"
#include "Engine/HighResolutionTimer.h"
#include "Engine/Util.h"
#include "Engine/VulkanResources/DescriptorPool.h"
#include "Engine/VulkanResources/DescriptorSet.h"
#include "Engine/VulkanResources/DeviceBuffer/BufferObject.h"
#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"
#include "Engine/VulkanResources/DeviceBuffer/UniformBuffer.h"
#include "Engine/VulkanResources/Framebuffer.h"
#include "Engine/VulkanResources/Pipeline/PipelineLayout.h"
#include "Engine/VulkanResources/RenderPass.h"

#include "Engine/LightUbo.h"

namespace MiniEngine {

ViewResourceSet::ViewResourceSet(
    const std::shared_ptr<Vk::LogicalDevice>& device, const std::shared_ptr<Vk::SwapChain>& swapChain,
    const glm::vec2& offset, const glm::vec2& size, const std::vector<std::shared_ptr<DisplayItem>>& displayItems,
    const std::vector<UboType>& uboTypes)
    : device_(device)
    , swapChain_(swapChain)
{
    createBuffers(offset, size, displayItems, uboTypes);
}

void ViewResourceSet::createBuffers(
    const glm::vec2& offset, const glm::vec2& size, const std::vector<std::shared_ptr<DisplayItem>>& displayItems,
    const std::vector<UboType>& uboTypes)
{
    pipelines_.resize(displayItems.size());
    for (uint32_t iObject = 0; iObject < displayItems.size(); iObject++)
    {
        const auto& item    = displayItems[iObject];
        pipelines_[iObject] = std::make_shared<Vk::GraphicsPipeline>(
            device_, swapChain_->getWidth() * offset.x, swapChain_->getHeight() * offset.y,
            glm::uvec2{swapChain_->getWidth() * size.x, swapChain_->getHeight() * size.y}, item->getModel(),
            swapChain_->getRenderPass());
    }

    if (uniformBufferMap_.empty())
    {
        createUniformBuffers(swapChain_->getNumImages(), uboTypes);
    }

    if (descriptorSets_.empty())
    {
        // TODO Just need a layout, not the pipeline itself
        createDescriptorSets(swapChain_->getNumImages(), pipelines_, displayItems);
    }

    commandBuffers_.resize(swapChain_->getNumImages());

    for (uint32_t iBuffer = 0; iBuffer < commandBuffers_.size(); iBuffer++)
    {
        recordCommandBuffer(offset, size, displayItems, iBuffer, true);
    }
}

void ViewResourceSet::recordCommandBuffer(
    const glm::vec2& offsetRel, const glm::vec2& sizeRel, const std::vector<std::shared_ptr<DisplayItem>>& displayItems,
    const uint32_t imageIndex, const bool doCreate)
{
    auto& buffer = commandBuffers_[imageIndex];

    if (doCreate)
    {
        buffer = std::make_shared<Vk::CommandBuffer>(device_);
    }
    else
    {
        vkResetCommandBuffer(buffer->get(), 0);
    }

    buffer->beginSimultaneous();

    const VkOffset2D offset = {static_cast<int32_t>(offsetRel.x * swapChain_->getWidth()),
                               static_cast<int32_t>(offsetRel.y * swapChain_->getHeight())};
    const VkExtent2D extent = {std::min(
                                   static_cast<uint32_t>(sizeRel.x * swapChain_->getWidth()),
                                   swapChain_->getWidth() - static_cast<uint32_t>(offset.x)),
                               std::min(
                                   static_cast<uint32_t>(sizeRel.y * swapChain_->getHeight()),
                                   swapChain_->getHeight() - static_cast<uint32_t>(offset.y))};

    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType                 = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass            = swapChain_->getRenderPass()->get();
    renderPassInfo.framebuffer           = swapChain_->getFramebuffer(imageIndex)->get();
    renderPassInfo.renderArea.offset     = offset;
    renderPassInfo.renderArea.extent     = extent;
    //    renderPassInfo.renderArea.extent     = {extent_.width / 2, extent_.height / 2};

    std::array<VkClearValue, 2> clearValues = {};
    clearValues[0].color                    = {{0.4f, 0.3f, 0.2f, 1.0f}};
    clearValues[1].depthStencil             = {1.0f, 0};
    renderPassInfo.clearValueCount          = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues             = clearValues.data();

    vkCmdBeginRenderPass(buffer->get(), &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (uint32_t iObject = 0; iObject < displayItems.size(); iObject++)
    {
        const auto& item     = displayItems[iObject];
        auto&       pipeline = pipelines_[iObject];
        pipeline->updateIfNeeded();
        vkCmdBindPipeline(buffer->get(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->get());

        const auto modelMatrix = item->computeModelMatrix();
        vkCmdPushConstants(
            buffer->get(), pipeline->getLayout()->get(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(modelMatrix),
            &modelMatrix);

        // Bind the vertex buffer
        VkBuffer     vertexBuffers[] = {item->getVertexBuffer()->get()};
        VkDeviceSize offsets[]       = {0};
        vkCmdBindVertexBuffers(buffer->get(), 0, 1, vertexBuffers, offsets);

        vkCmdBindIndexBuffer(buffer->get(), item->getIndexBuffer()->get(), 0, item->getIndexBuffer()->getIndexType());

        vkCmdBindDescriptorSets(
            buffer->get(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getLayout()->get(), 0, 1,
            &descriptorSets_[iObject][imageIndex]->get(), 0, nullptr);
        vkCmdDrawIndexed(buffer->get(), item->getIndexBuffer()->getNumElements(), 1, 0, 0, 0);
    }

    vkCmdEndRenderPass(buffer->get());

    checkVk(vkEndCommandBuffer(buffer->get()));
}

void ViewResourceSet::updateUniformBuffer(
    const uint32_t index, const std::shared_ptr<Camera>& camera, const float viewRatio)
{
    for (auto& bufferSet : uniformBufferMap_)
    {
        switch (bufferSet.first)
        {
        case UboType::camera: {
            const float screenRatio =
                static_cast<float>(swapChain_->getExtent().width) / swapChain_->getExtent().height;

            BasicUbo ubo    = {};
            ubo.viewMatrix_ = camera->computeViewMatrix();
            ubo.projMatrix_ = camera->computeProjectionMatrix(screenRatio, viewRatio);

            bufferSet.second[index]->copyData(&ubo, sizeof(ubo));
        }
        break;

        case UboType::light: {
            LightUbo ubo       = {};
            ubo.lightPosition_ = glm::vec4(camera->getPosition(), 1.f);
            //        ubo.lightPosition_  = glm::vec3(3.f, 0.f, 0.f);
            ubo.cameraPosition_ = glm::vec4(camera->getPosition(), 1.f);

            bufferSet.second[index]->copyData(&ubo, sizeof(ubo));
        }
        break;
        case UboType::none: std::cerr << "[ViewResourceSet] Unknown UBO type!" << std::endl; break;
        }
    }
}

void ViewResourceSet::updateUniformBuffers(const std::shared_ptr<Camera>& camera, const float viewRatio)
{
    for (uint32_t i = 0; i < commandBuffers_.size(); i++)
        updateUniformBuffer(i, camera, viewRatio);
}

void ViewResourceSet::createUniformBuffers(const uint32_t count, const std::vector<UboType>& uboTypes)
{
    uniformBufferMap_.clear();

    for (const auto type : uboTypes)
    {
        size_t size = 0;
        switch (type)
        {
        case UboType::camera: size = sizeof(BasicUbo); break;
        case UboType::light: size = sizeof(LightUbo); break;
        case UboType::none: break;
        }

        std::vector<std::shared_ptr<Vk::UniformBuffer>> ubos;
        for (uint32_t i = 0; i < count; i++)
        {
            ubos.push_back(std::make_shared<Vk::UniformBuffer>(device_, size));
        }
        uniformBufferMap_[type] = ubos;
    }
}

void ViewResourceSet::createDescriptorSets(
    const uint32_t numImages, const std::vector<std::shared_ptr<Vk::GraphicsPipeline>>& pipelines,
    const std::vector<std::shared_ptr<DisplayItem>>& items)
{
    if (descriptorPool_ == nullptr)
    {
        descriptorPool_ =
            Vk::DescriptorPool::makeGraphicsDescriptorPool(device_, numImages * static_cast<uint32_t>(items.size()));
    }

    descriptorSets_.resize(items.size());
    for (uint32_t iItem = 0; iItem < items.size(); iItem++)
    {
        const auto& item = items[iItem];

        if (descriptorSetMap_.count(item->getMaterialType()) == 0)
        {
            // Create the descriptor sets
            std::vector<std::shared_ptr<Vk::DescriptorSet>> sets(numImages);
            for (uint32_t iImage = 0; iImage < numImages; iImage++)
            {
                std::vector<std::shared_ptr<Vk::UniformBuffer>> ubos;
                for (const UboType uboType : pipelines[iItem]->getMaterialType()->getUboTypes())
                {
                    ubos.push_back(uniformBufferMap_[uboType][iImage]);
                }
                // TODO Gonna have to do something if (when) the texture views are not the same across items...
                sets[iImage] = Vk::DescriptorSet::makeGraphicsDescriptorSet(
                    device_, descriptorPool_, pipelines[iItem]->getDescriptorSetLayout(), ubos,
                    items[iItem]->getTexture());
            }
            descriptorSetMap_[item->getMaterialType()] = sets;
        }

        // Assign the descriptor sets to this item
        descriptorSets_[iItem] = descriptorSetMap_[item->getMaterialType()];
    }
}

} // namespace MiniEngine
