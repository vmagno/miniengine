#include "View.h"

#include "Engine/MaterialType.h"
#include "Engine/Scene/Scene.h"
#include "Engine/View/ViewResourceSet.h"

namespace MiniEngine {

View::View(
    const glm::vec2& offset, const glm::vec2& size, const std::shared_ptr<Scene>& scene,
    const std::shared_ptr<Camera>& camera)
    : offset_(glm::clamp(offset, {0, 0}, {1, 1}))
    , size_(glm::clamp(size, {0, 0}, {1, 1}))
    , scene_(scene)
    , camera_(camera)
{
}

void View::createResourceSet(const std::shared_ptr<Vk::SwapChain>& swapChain)
{
    resourceSet_ = std::make_shared<ViewResourceSet>(
        swapChain->getDevice(), swapChain, offset_, size_, scene_->getObjects(),
        std::vector<UboType>({UboType::camera, UboType::light}));
}

void View::updateBuffers(const uint32_t index)
{
    resourceSet_->recordCommandBuffer(offset_, size_, scene_->getObjects(), index, false);
    resourceSet_->updateUniformBuffer(index, camera_, size_.x / size_.y);
}

} // namespace MiniEngine
