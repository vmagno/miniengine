#ifndef MINI_ENGINE_BASIC_UBO_H
#define MINI_ENGINE_BASIC_UBO_H

#include "GlmInclude.h"

namespace MiniEngine {

struct BasicUbo
{
    glm::mat4 viewMatrix_;
    glm::mat4 projMatrix_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_BASIC_UBO_H
