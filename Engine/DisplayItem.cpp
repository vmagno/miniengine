#include "DisplayItem.h"

#include <iostream>

#include "GlmInclude.h"
#include "HighResolutionTimer.h"

using Util::Quaternion;

namespace MiniEngine {

DisplayItem::DisplayItem(const std::shared_ptr<Model>& model, const glm::vec3& position, const glm::vec3& scale)
    : model_(model)
    , position_(position)
    , scale_(scale)
{
}

DisplayItem::~DisplayItem()
{
}

const glm::mat4 DisplayItem::computeModelMatrix() const
{
    const glm::mat<4, 4, Real> translation(glm::translate(glm::mat<4, 4, Real>(Real(1)), position_));
    const glm::mat<4, 4, Real> rotation(orientation_.toMat4());
    const glm::mat<4, 4, Real> scaling(glm::scale(glm::mat<4, 4, Real>(Real(1)), scale_));

    return translation * rotation * scaling;
}

void DisplayItem::setupCallbacks(const std::shared_ptr<EventManager>& eventManager)
{
    if (listensToInput_)
    {
        doSetupCallbacks();
    }

    for (const auto& keyTaskPair : keypressTasks_)
    {
        eventManager->addKeypressEvent(keyTaskPair.first, shared_from_this(), keyTaskPair.second);
    }

    if (!mouseMoveTasks_.empty())
    {
        eventManager->setMouseMoveEvent(shared_from_this(), mouseMoveTasks_.back());
    }
}

void DisplayItem::setPositionLimits(const glm::vec<3, Real>& minPos, const glm::vec<3, Real>& maxPos)
{
    minPos_ = minPos;
    maxPos_ = maxPos;
}

} // namespace MiniEngine
