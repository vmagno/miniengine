#ifndef MINI_ENGINE_PIXEL_H
#define MINI_ENGINE_PIXEL_H

namespace MiniEngine {

struct Pixel
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_PIXEL_H
