#ifndef MINI_ENGINE_APPLICATION_H
#define MINI_ENGINE_APPLICATION_H

#include <memory>
#include <vector>

#include "GlmInclude.h"
#include "HighResolutionTimer.h"

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {

class EventManager;
class Scene;
class View;
class Window;

/**
 * @brief A MiniEngine application object. Contains everything needed to run something with that engine (windows,
 * scene/world, device, rendering loop, etc.).
 * - An application will have at least one window in which it will be displayed.
 * - An application has a Vulkan instance which contains the state of the application and interfaces with the driver.
 * - Each window has a logical device which manages rendering in that window.
 * - Each window has one or more views on scenes.
 * - Each scene constitutes a self-contained "world" and represent an internal state of the application.
 * - Each view is associated with a camera that is present in a scene.
 */
class Application
{
public:
    Application();
    virtual ~Application();

    virtual void run(); //!< Run the application

private:
    std::shared_ptr<Window> window_;     //!< GLFW window that appears on the monitor
    glm::uvec2              resolution_; //!< Resolution of the window in pixels

    std::shared_ptr<Vk::VulkanInstance> vulkanInstance_; //!< Interface with Vulkan

    float targetFrameTimeMs_ = 1000.f / 60; //!< Minimum time between frames (ms)

    uint32_t frameCount_; //!< Number of frames elapsed since last reset (used to count frame rate)
    float    currentFps_; //!< Current frame rate

    //!@{ \name Timers
    Util::HighResolutionTimer fpsTimer_;   //!< Keep track of the time since the last frame (to keep constant framerate)
    Util::HighResolutionTimer frameTimer_; //!< To compute the time taken by frames only
    Util::HighResolutionTimer animationTimer_; //!< Keep track of time since last animcation was performed
    Util::HighResolutionTimer outputTimer_;    //!< Keep track of time since last output to console
    //!@}

protected:
    std::shared_ptr<Vk::LogicalDevice> device_; //!< Device that will render the application
    std::shared_ptr<Scene>             scene_;  //!< Scene displayed in the window
    std::vector<std::shared_ptr<View>> views_;  //!< Windows into the world (or UI?)
    std::shared_ptr<EventManager>      eventManager_;

    std::string windowTitle_ = "";

    /**
     * @brief Update components of the application
     * @param deltaS Time in seconds since last update
     */
    virtual void animate(const float deltaS);

protected:
    /**
     * @brief Initialize everything: Vulkan instance + device, window, scene, views
     */
    void init();

private: // functions
    /**
     * @brief Initialize the scene: every objects and cameras that are in it
     */
    virtual void initScene();

    /**
     * @brief Initialize the views: sections of windows, with the content of which camera, etc.
     */
    virtual void initViews();

    /**
     * @brief Render and animate the world, for as long as we must.
     */
    void mainLoop();

    /**
     * @brief Draw a frame
     */
    void draw();

private: // GLFW initialization
    /**
     * @brief Class whose only purpose is to initialize the GLFW library (and de-initialize it when it's over)
     */
    class GlfwInitializer
    {
    public:
        GlfwInitializer();
        ~GlfwInitializer();
    };
};

} // namespace MiniEngine

#endif // MINI_ENGINE_APPLICATION_H
