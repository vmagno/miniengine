#ifndef MINI_ENGINE_MATH_H
#define MINI_ENGINE_MATH_H

#include <cmath>

#include "GlmInclude.h"

namespace Util {

template <class T>
const T VECTOR_LENGTH_ALMOST_ZERO = T(1e-6);

inline const glm::vec3 projectOnNormal(const glm::vec3& vector, const glm::vec3& normal)
{
    const float cosAngle = glm::dot(glm::normalize(normal), vector);
    return normal * cosAngle;
}

inline const glm::vec3 projectOnPlane(const glm::vec3& vector, const glm::vec3& planeNormal)
{
    return vector - projectOnNormal(vector, planeNormal);
}

} // namespace Util

#endif // MINI_ENGINE_MATH_H
