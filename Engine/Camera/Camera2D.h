#ifndef MINI_ENGINE_CAMERA_2D_H
#define MINI_ENGINE_CAMERA_2D_H

#include "Camera.h"

namespace MiniEngine {

class Camera2D : public Camera
{
    using Camera::Camera;

private:
    const glm::vec<2, Real> doComputeFrameRotation(const Real /* angle */) const override { return {0, 0}; }
    void                    doSetupCallbacks() override;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_CAMERA_2D_H
