#ifndef MINI_ENGINE_CAMERA_H
#define MINI_ENGINE_CAMERA_H

#include "Engine/DisplayItem.h"
#include "Engine/EventManager.h"
#include "Engine/VirtualScreen.h"

namespace MiniEngine {

class Camera : public DisplayItem
{
public:
    enum class ProjectionType
    {
        Perspective = 0,
        Ortho,
    };

public:
    Camera() = delete;
    Camera(
        const std::shared_ptr<Model>& model, const glm::vec3& position, const glm::vec3& direction,
        const glm::vec3& up);
    ~Camera() override;

    /**
     * @brief Factory function to create a camera
     * @param device Device on which the resources will be held
     * @param position Initial position of the camera
     * @param direction Initial direction in which the camera is looking
     * @param up Initial direction of the top of the camera
     * @return A (shared) pointer to the newly created camera object
     */
    static std::shared_ptr<Camera> makeCamera(
        const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec3& position, const glm::vec3& direction,
        const glm::vec3& up, const bool make2d);

    static std::shared_ptr<Camera>
        makeVirtualCamera(const std::shared_ptr<Vk::LogicalDevice>& device, const VirtualScreenParams& screenParams);

    /**
     * @brief Compute the field of view of the camera in degrees (vertical angle)
     * @return The field of view, in degrees
     */
    float getFieldOfView() const;

    //!@{ \name Getters
    inline const auto& getDirectionVector() const { return direction_; }
    inline const auto& getUpVector() const { return up_; }
    inline float       getZoomFactor() const { return zoom_; }
    inline auto        getFovHeight() const { return fovHeight_; }
    //!@}

    //!@{ \name Movement setters
    inline void setMoveForward(const bool move = true) { moveForward_ = move; }
    inline void setMoveBackward(const bool move = true) { moveBackward_ = move; }
    inline void setMoveLeft(const bool move = true) { moveLeft_ = move; }
    inline void setMoveRight(const bool move = true) { moveRight_ = move; }
    inline void setMoveUp(const bool move = true) { moveUp_ = move; }
    inline void setMoveDown(const bool move = true) { moveDown_ = move; }
    inline void setTurnLeft(const bool turn = true) { turnLeft_ = turn; }
    inline void setTurnRight(const bool turn = true) { turnRight_ = turn; }
    inline void setTurnUp(const bool turn = true) { turnUp_ = turn; }
    inline void setTurnDown(const bool turn = true) { turnDown_ = turn; }

    inline void setZoomIn(const bool zoom = true) { zoomIn_ = zoom; }
    inline void setZoomOut(const bool zoom = true) { zoomOut_ = zoom; }

    inline void setBaseSpeed(const float newSpeed) { baseSpeed_ = newSpeed; }

    void        setClipPlanes(const glm::vec3& min, const glm::vec3& max);
    inline void setProjectionType(const ProjectionType newType) { projectionType_ = newType; }
    //!@}

    /**
     * @brief Rotate the camera
     * @param horizontalAngle Angle of rotation around the vertical axis (yaw) in radians
     * @param verticalAngle Angle of rotation around horizontal + sideways axis (pitch) in radians
     */
    void rotateCamera(const Real horizontalAngle, const Real verticalAngle);

    inline void updateZoom(const float newZoomValue) { zoom_ = glm::clamp(newZoomValue, minZoom_, maxZoom_); }

    void animate(const float deltaT) override;

    inline const glm::mat4 computeViewMatrix() const { return glm::lookAt(position_, position_ + direction_, up_); }
    const glm::mat4        computeProjectionMatrix(const float screenRatio, const float viewRatio) const;

protected:
    //!@{ \name Camera state
    glm::vec<3, Real> direction_; //!< Where the camera is looking (unit vector)
    glm::vec<3, Real> up_;        //!< Up side of the camera (unit vector)
    glm::vec<3, Real> left_;      //!< Vector that points to the left from the point of view of the camera (unit vector)

    glm::vec<3, Real> initialUp_; //!< Vector that was determined to be up during init. Yaw is determined around it.

    float fovHeight_ = 0.5f; //! Half-height of a virtual screen placed at a distance of 1 unit from the camera
    float zoom_      = 1.0f; //!< How many times the image is increased in size

    ProjectionType projectionType_ = ProjectionType::Perspective;
    float          nearPlane_      = 0.1f;
    float          farPlane_       = 300.0f;
    glm::vec2      minScreen_      = glm::vec2(0.0f); //!< Min coord for orthogonal projection
    glm::vec2      maxScreen_      = glm::vec2(0.0f); //!< Max coord for orthogonal projection
    //!@}

    //!@{ \name Movement parameters
    float baseSpeed_        = 2.0f;   //!< Units per second
    float rotationSpeed_    = 100.0f; //!< Degrees per second
    float wheelZoomSpeed_   = 0.2f;   //!< In percentage of current zoom
    float animateZoomSpeed_ = 0.5f;   //!< In percentage of current zoom, per second
    //!@}

    //!@{ \name Limits on movement
    float minZoom_ = 0.1f;
    float maxZoom_ = 100000.0f;
    //!@}

    //!@{ \name Whether the camera will move during animation
    bool moveForward_  = false;
    bool moveBackward_ = false;
    bool moveLeft_     = false;
    bool moveRight_    = false;
    bool moveUp_       = false;
    bool moveDown_     = false;
    bool turnLeft_     = false;
    bool turnRight_    = false;
    bool turnUp_       = false;
    bool turnDown_     = false;

    bool zoomIn_  = false;
    bool zoomOut_ = false;
    //!@}

protected: // functions
    void setupCommonCameraCallbacks();

    static const std::shared_ptr<Model> makeCameraModel(const std::shared_ptr<Vk::LogicalDevice>& device);

private: // functions
    virtual const glm::vec<3, Real> doComputeFrameDisplacement(const Real distance) const;
    virtual const glm::vec<2, Real> doComputeFrameRotation(const Real angle) const;

    inline void rotateFromMouseMovement(
        const glm::vec<2, double>& newPosition, const glm::vec<2, double>& oldPosition,
        const MouseButtonState& buttonState);

    void doSetupCallbacks() override;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_CAMERA_H
