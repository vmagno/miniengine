#include "VirtualCamera.h"

namespace MiniEngine {

VirtualCamera::VirtualCamera(const std::shared_ptr<Model>& model, const VirtualScreenParams& screenParams)
    : Camera(
          model, glm::vec3(0.0f, 0.0f, screenParams.coord_.z + 1), glm::vec3(0.0f, 0.0f, screenParams.coord_.z),
          glm::vec3(0.0f, 1.0f, 0.0f))
{
    setClipPlanes(
        {-screenParams.coord_.x, -screenParams.coord_.y, screenParams.coord_.z + 1.0f},
        {screenParams.coord_.x, screenParams.coord_.y, screenParams.coord_.z + 1.0001f});

    setProjectionType(ProjectionType::Ortho);
}

VirtualCamera::~VirtualCamera()
{
}

} // namespace MiniEngine
