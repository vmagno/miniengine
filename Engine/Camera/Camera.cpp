#include "Camera.h"

#include <iostream>

#include <GLFW/glfw3.h>

#include "Engine/Camera/Camera2D.h"
#include "Engine/Camera/VirtualCamera.h"
#include "Engine/DisplayItem.h"
#include "Engine/EventManager.h"
#include "Engine/MaterialType.h"
#include "Engine/Math.h"
#include "Engine/Quaternion.h"
#include "Engine/Vertex/Vertex_col.h"
#include "Engine/VulkanCommon.h"
#include "Engine/VulkanResources/ShaderModule.h"

using Util::Quaternion;

namespace MiniEngine {

Camera::Camera(
    const std::shared_ptr<Model>& model, const glm::vec3& position, const glm::vec3& direction, const glm::vec3& up)
    : DisplayItem(model, position)
    , direction_(direction)
    , up_(up)
    , initialUp_(0)
{
    setScale(glm::vec3(0.1, 0.1, 0.1));

    if (glm::length(direction_) < Real(1e-6))
    {
        std::cerr << "Direction vector is kinda short! Using (0, 0, 1) instead" << std::endl;
        direction_ = glm::vec3(0, 0, 1);
    }

    if (glm::length(up_) < Real(1e-6))
    {
        std::cerr << "Up vector is kinda short! Using (0, 1, 0) instead" << std::endl;
        direction_ = glm::vec3(0, 1, 0);
    }

    direction_ = glm::normalize(direction_);

    //    std::cerr << "dir: " << direction_[0] << ", " << direction_[1] << ", " << direction_[2] << std::endl;

    up_        = glm::normalize(up_);
    initialUp_ = up_;
    left_      = glm::normalize(glm::cross(up_, direction_));
    up_        = glm::normalize(glm::cross(direction_, left_)); // Make sure the 3 vectors are orthonormal

    orientation_ = Quaternion<Real>::fromBasis(direction_, up_, left_);
    //    std::cerr << "Cam orientation: " << orientation_ << std::endl;

    left_      = getOrientation().rotateVec(Global::LEFT);
    direction_ = getOrientation().rotateVec(Global::FORWARD);
    up_        = getOrientation().rotateVec(Global::UP);

    //    const auto& pos = getPosition();
    //    std::cerr << "pos: " << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    //    std::cerr << "dir: " << direction_[0] << ", " << direction_[1] << ", " << direction_[2] << std::endl;
    //    std::cerr << "pos: " << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;

    //    { // Error checking
    //        Real error = 0;
    //        error += glm::length(direction_ - orientation_.rotateVec(Global::FORWARD));
    //        error += glm::length(up_ - orientation_.rotateVec(Global::UP));
    //        error += glm::length(left_ - orientation_.rotateVec(Global::LEFT));

    //        std::cerr << "Error = " << error << std::endl;
    //    }
}

Camera::~Camera()
{
}

std::shared_ptr<Camera> Camera::makeCamera(
    const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec3& position, const glm::vec3& direction,
    const glm::vec3& up, const bool make2d)
{
    const auto model  = makeCameraModel(device);
    const auto camera = make2d ? std::make_shared<Camera2D>(model, position, direction, up)
                               : std::make_shared<Camera>(model, position, direction, up);
    return camera;
}

std::shared_ptr<Camera>
    Camera::makeVirtualCamera(const std::shared_ptr<Vk::LogicalDevice>& device, const VirtualScreenParams& screenParams)
{
    return std::make_shared<VirtualCamera>(makeCameraModel(device), screenParams);
}

float Camera::getFieldOfView() const
{
    return 2.0f * glm::degrees(glm::atan(fovHeight_ / zoom_));
}

void Camera::setClipPlanes(const glm::vec3& min, const glm::vec3& max)
{
    minScreen_ = {min.x, min.y};
    maxScreen_ = {max.x, max.y};
    nearPlane_ = min.z;
    farPlane_  = max.z;
}

void Camera::rotateCamera(const Real horizontalAngle, const Real verticalAngle)
{
    Quaternion<Real> rotation(Quaternion<Real>::fromAngleAxis(horizontalAngle, initialUp_));

    // Limit vertical rotation
    // First find angle from vertical (initial up)
    //    const Real altitude = std::acos(glm::dot(direction_, glm::normalize(Util::projectOnPlane(initialUp_,
    //    left_)))); if (altitude > -verticalAngle * Real(1.1) && Real(M_PI) - altitude > verticalAngle * Real(1.1))
    {
        rotation *= Quaternion<Real>::fromAngleAxis(verticalAngle, left_);
    }

    rotate(rotation);

    left_      = getOrientation().rotateVec(Global::LEFT);
    direction_ = getOrientation().rotateVec(Global::FORWARD);
    up_        = getOrientation().rotateVec(Global::UP);
}

void Camera::animate(const float deltaT)
{
    const Real distance     = Real(baseSpeed_ * deltaT * (1.0f / zoom_));
    const auto displacement = doComputeFrameDisplacement(distance);
    translate(displacement);

    const Real horizontalAngle = glm::radians(rotationSpeed_) * (turnLeft_ ? deltaT : turnRight_ ? -deltaT : 0);
    const Real verticalAngle   = glm::radians(rotationSpeed_) * (turnDown_ ? deltaT : turnUp_ ? -deltaT : 0);
    rotateCamera(horizontalAngle, verticalAngle);

    const float zoomAmount = zoom_ * animateZoomSpeed_ * deltaT;
    if (zoomIn_)
    {
        updateZoom(zoom_ + zoomAmount);
    }
    else if (zoomOut_)
    {
        updateZoom(zoom_ - zoomAmount);
    }
}

const glm::mat4 Camera::computeProjectionMatrix(const float screenRatio, const float viewRatio) const
{
    glm::mat4 projMatrix;
    switch (projectionType_)
    {
    case ProjectionType::Perspective:
        projMatrix = glm::perspective(glm::radians(getFieldOfView()), screenRatio * viewRatio, 0.1f, 300.0f);
        break;
    case ProjectionType::Ortho:
        const glm::vec2 mid = (minScreen_ + maxScreen_) * 0.5f;
        const glm::vec2 factor(
            screenRatio <= viewRatio ? screenRatio / viewRatio : 1.f,
            screenRatio > viewRatio ? viewRatio / screenRatio : 1.f);
        const auto newMin = (minScreen_ - mid) * factor + mid;
        const auto newMax = (maxScreen_ - mid) * factor + mid;
        projMatrix        = glm::ortho(newMin.x, newMax.x, newMin.y, newMax.y, nearPlane_, farPlane_);
        break;
    }

    projMatrix[1][1] *= -1; // Vertical clip coord are NOT inverted in Vulkan

    return projMatrix;
}

void Camera::setupCommonCameraCallbacks()
{
    keypressTasks_.push_back({GLFW_KEY_A, [this](const int action) { this->setMoveLeft(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_D, [this](const int action) { this->setMoveRight(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_KP_ADD, [this](const int action) { this->setZoomIn(action == GLFW_PRESS); }});
    keypressTasks_.push_back(
        {GLFW_KEY_KP_SUBTRACT, [this](const int action) { this->setZoomOut(action == GLFW_PRESS); }});
}

const std::shared_ptr<Model> Camera::makeCameraModel(const std::shared_ptr<Vk::LogicalDevice>& device)
{
    const std::vector<Vertex_col> vertices = {
        {{-1.0f, -1.0f, -1.0f}, {0.0f, 0.0f, 1.0f}}, {{1.0f, -1.0f, -1.0f}, {0.0f, 0.0f, 1.0f}},
        {{1.0f, 1.0f, -1.0f}, {1.0f, 1.0f, 1.0f}},   {{-1.0f, 1.0f, -1.0f}, {1.0f, 1.0f, 1.0f}},
        {{-1.0f, -1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}},  {{1.0f, -1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}},
        {{1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}},    {{-1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}},
    };
    const std::vector<uint16_t> indices = {0, 2, 1, 3, 2, 0, 0, 5, 4, 0, 1, 5, 3, 0, 4, 4, 7, 3,
                                           1, 2, 6, 6, 5, 1, 4, 5, 6, 6, 7, 4, 2, 3, 6, 3, 7, 6};

    return std::make_shared<Model>(device, VertexList(vertices), indices, ModelParams());
}

void Camera::doSetupCallbacks()
{
    setupCommonCameraCallbacks();

    keypressTasks_.push_back({GLFW_KEY_W, [this](const int action) { this->setMoveForward(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_S, [this](const int action) { this->setMoveBackward(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_Q, [this](const int action) { this->setTurnLeft(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_E, [this](const int action) { this->setTurnRight(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_Z, [this](const int action) { this->setTurnUp(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_X, [this](const int action) { this->setTurnDown(action == GLFW_PRESS); }});

    mouseMoveTasks_.push_back(
        [this](
            const glm::vec<2, double>& newPos, const glm::vec<2, double>& oldPos, const MouseButtonState& buttonState) {
            this->rotateFromMouseMovement(newPos, oldPos, buttonState);
        });
}

const glm::vec<3, Real> Camera::doComputeFrameDisplacement(const Real distance) const
{
    return direction_ * (moveForward_ ? distance : moveBackward_ ? -distance : 0) +
           left_ * (moveLeft_ ? distance : moveRight_ ? -distance : 0) +
           up_ * (moveUp_ ? distance : moveDown_ ? -distance : 0);
}

const glm::vec<2, Real> Camera::doComputeFrameRotation(const Real angle) const
{
    return {turnLeft_ ? angle : turnRight_ ? -angle : 0, turnDown_ ? angle : turnUp_ ? -angle : 0};
}

void Camera::rotateFromMouseMovement(
    const glm::vec<2, double>& newPosition, const glm::vec<2, double>& oldPosition, const MouseButtonState& buttonState)
{
    if (buttonState.isLeftButtonPressed_)
    {
        const Real horizontal = Real((newPosition.x - oldPosition.x) * -0.005);
        const Real vertical   = Real((newPosition.y - oldPosition.y) * 0.005);
        rotateCamera(horizontal, vertical);
    }
}

} // namespace MiniEngine
