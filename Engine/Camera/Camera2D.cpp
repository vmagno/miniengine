#include "Camera2D.h"

#include <GLFW/glfw3.h>

namespace MiniEngine {

void Camera2D::doSetupCallbacks()
{
    setupCommonCameraCallbacks();

    keypressTasks_.push_back({GLFW_KEY_W, [this](const int action) { this->setMoveUp(action == GLFW_PRESS); }});
    keypressTasks_.push_back({GLFW_KEY_S, [this](const int action) { this->setMoveDown(action == GLFW_PRESS); }});
}

} // namespace MiniEngine
