#ifndef MINI_ENGINE_VIRTUAL_CAMERA_H
#define MINI_ENGINE_VIRTUAL_CAMERA_H

#include "Camera.h"

namespace MiniEngine {

class VirtualCamera : public Camera
{
public:
    VirtualCamera(const std::shared_ptr<Model>& model, const VirtualScreenParams& screenParams);
    ~VirtualCamera() override;

private:
    void doSetupCallbacks() override {}
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VIRTUAL_CAMERA_H
