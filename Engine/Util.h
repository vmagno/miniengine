#ifndef MINI_ENGINE_UTIL_H
#define MINI_ENGINE_UTIL_H

#include <sstream>
#include <stdexcept>
#include <vector>

#include <vulkan/vulkan.h>

/// Check the return value of a Vulkan call and throw an exception with error message if there was a problem
#define checkVk(val) Util::check((val), #val, __FILE__, __LINE__)

namespace Util {

/**
 * @brief Read the content of a file into a char buffer
 * @param filename The file to read
 * @return A vector with the content of the file
 */
std::vector<char> readFile(const std::string& filename);

inline bool hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

/**
 * @brief Get the string that corresponds to a certain VkResult value
 * @param result The value for which to get the name
 * @return Name of the given value
 */
inline const char* getVkErrorName(const VkResult result)
{
    switch (result)
    {
    case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
    case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
    case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
    case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
    case VK_ERROR_FRAGMENTATION_EXT: return "VK_ERROR_FRAGMENTATION_EXT";
    case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
    case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
    case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";
    case VK_ERROR_INVALID_DEVICE_ADDRESS_EXT: return "VK_ERROR_INVALID_DEVICE_ADDRESS_EXT";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT:
        return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";
    case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
    case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
    case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
    case VK_ERROR_NOT_PERMITTED_EXT: return "VK_ERROR_NOT_PERMITTED_EXT";
    case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";
    case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
    case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
    case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
    case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
    case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
    case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
    case VK_EVENT_RESET: return "VK_EVENT_RESET";
    case VK_EVENT_SET: return "VK_EVENT_SET";
    case VK_INCOMPLETE: return "VK_INCOMPLETE";
    case VK_NOT_READY: return "VK_NOT_READY";
    case VK_RESULT_MAX_ENUM: return "VK_RESULT_MAX_ENUM";
    case VK_RESULT_RANGE_SIZE: return "VK_RESULT_RANGE_SIZE";
    case VK_SUBOPTIMAL_KHR: return "VK_SUBOPTIMAL_KHR";
    case VK_SUCCESS: return "VK_SUCCESS";
    case VK_TIMEOUT: return "VK_TIMEOUT";
    }

    return "UNKNOWN ERROR CODE !!!";
}

/**
 * @brief Verify that the given Vulkan call returns successfully and if not, print the corresponding error code and
 * name. Will throw an exception if there was an error. Based on a similar function found in the Cuda SDK
 * @param result Return value of the Vulkan call
 * @param call String of the code of the Vulkan call
 * @param filename File where it was called
 * @param line Line number in the file where it was called
 */
template <class T>
inline void check(T result, const char* const call, const char* const filename, const int line)
{
    if (result != VK_SUCCESS)
    {
        std::stringstream errorMessage;
        errorMessage << "Vulkan error at " << filename << ":" << line << " with code " << result << " ("
                     << getVkErrorName(result) << ") for " << call << std::endl;

        throw std::runtime_error(errorMessage.str());
    }
}

void printBacktrace();

} // namespace Util

#endif // MINI_ENGINE_UTIL_H
