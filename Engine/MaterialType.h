#ifndef MINI_ENGINE_MATERIAL_TYPE_H
#define MINI_ENGINE_MATERIAL_TYPE_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/DescriptorSetLayout.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/ShaderModule.h"

namespace MiniEngine {

enum class MatType
{
    none = 0,
    color,
    color_texture,
    texture,
    phong1Light,
};

enum class UboType
{
    none = 0,
    camera,
    light
};

class MaterialType
{
public:
    MaterialType() = delete;
    MaterialType(
        const std::shared_ptr<Vk::LogicalDevice>& device, const std::string& vertexShaderFile,
        const std::string& fragmentShaderFile, const VkVertexInputBindingDescription& bindingDesc,
        const std::vector<VkVertexInputAttributeDescription>& attribDesc, const std::vector<UboType>& uboTypes,
        const uint32_t numSamplers);

    const auto& getVertexShader() const { return vertexShaderModule_; }
    const auto& getFragmentShader() const { return fragmentShaderModule_; }
    const auto& getBindingDescription() const { return bindingDesc_; }
    const auto& getAttributeDescriptions() const { return attribDesc_; }
    const auto& getUboTypes() const { return uboTypes_; }
    const auto& getDescriptorSetLayout() const { return descriptorSetLayout_; }

    static const std::shared_ptr<MaterialType>
                    createInstance(const std::shared_ptr<Vk::LogicalDevice>& device, const MatType type);
    static uint32_t getMatId(const MatType type) { return static_cast<uint32_t>(type); }

private:
    std::shared_ptr<Vk::ShaderModule> vertexShaderModule_;
    std::shared_ptr<Vk::ShaderModule> fragmentShaderModule_;

    VkVertexInputBindingDescription                bindingDesc_;
    std::vector<VkVertexInputAttributeDescription> attribDesc_;
    std::vector<UboType>                           uboTypes_;

    std::shared_ptr<Vk::DescriptorSetLayout> descriptorSetLayout_;

private: // functions
};

} // namespace MiniEngine

#endif // MINI_ENGINE_MATERIAL_TYPE_H
