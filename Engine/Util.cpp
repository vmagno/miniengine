#include "Util.h"

#include <execinfo.h>
#include <fstream>
#include <iostream>

#include "VulkanCommon.h"

namespace Util {

std::vector<char> readFile(const std::string& filename)
{
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    if (!file.is_open())
    {
        file.open(MiniEngine::Global::SHADER_DIR + filename, std::ios::ate | std::ios::binary);
        if (!file.is_open())
        {
            file.open(MiniEngine::Global::TEXTURE_DIR + filename, std::ios::ate | std::ios::binary);
            if (!file.is_open())
            {
                throw std::runtime_error("Failed to open file \"" + filename + "\" !!!");
            }
        }
    }

    // Compute file size in bytes
    auto              fileSize = file.tellg();
    std::vector<char> buffer(static_cast<size_t>(fileSize));

    // Read and close the file
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    return buffer;
}

void printBacktrace()
{
    void*  trace[16];
    char** messages = nullptr;
    int    i, trace_size = 0;

    trace_size = backtrace(trace, 16);
    messages   = backtrace_symbols(trace, trace_size);
    /* skip first stack frame (points here) */
    fprintf(stderr, "[bt] Execution path:\n");
    for (i = 1; i < trace_size; ++i)
    {
        fprintf(stderr, "[bt] %s\n", messages[i]);
    }
}

} // namespace Util
