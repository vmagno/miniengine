#ifndef MINI_ENGINE_VERTEX_LIST_H
#define MINI_ENGINE_VERTEX_LIST_H

#include <cstring>
#include <vector>

#include "Engine/GlmInclude.h"
#include "Engine/Vertex/VertexInfo.h"

namespace MiniEngine {

template <class List>
class VertexListIterator
{
public:
    VertexListIterator(List& list, const uint32_t index)
        : list_(list)
        , index_(index)
    {
    }

    bool                operator!=(const VertexListIterator& other) const { return index_ != other.index_; }
    VertexListIterator& operator++()
    {
        index_++;
        return *this;
    }
    auto operator*() { return list_[index_]; }

private:
    List&    list_;
    uint32_t index_;
};

class VertexList
{
public:
    template <class T>
    struct VertexRef
    {
        VertexRef(T& pos)
            : position_(pos)
        {
        }

        T& position_;
    };

public:
    VertexList() {}

    template <class VertexType>
    VertexList(const std::vector<VertexType>& vertices)
        : vertexInfo_(VertexType::getVertexInfo())
        , vertexData_(vertices.size() * sizeof(VertexType))
    {
        std::memcpy(vertexData_.data(), vertices.data(), vertexData_.size());

        if (vertexInfo_.positionIndex_ < 0)
        {
            throw std::runtime_error("[VertexList] Index of \"position\" attribute is not set in given VertexInfo");
        }

        positionOffset_ =
            vertexInfo_.attributeDescriptions_[static_cast<unsigned int>(vertexInfo_.positionIndex_)].offset;
        vertexSize_  = vertexInfo_.size_;
        numVertices_ = static_cast<unsigned int>(vertices.size());
    }

    const auto& getInfo() const { return vertexInfo_; }
    auto        size() const { return vertexData_.size(); }
    const void* data() const { return vertexData_.data(); }
    auto        getNumVertices() const { return numVertices_; }

    auto begin() const { return VertexListIterator<const VertexList>(*this, 0); }
    auto end() const { return VertexListIterator<const VertexList>(*this, numVertices_); }

    auto begin() { return VertexListIterator<VertexList>(*this, 0); }
    auto end() { return VertexListIterator<VertexList>(*this, numVertices_); }

    auto operator[](const size_t index) const
    {
        using PosType = const glm::vec3;
        return VertexRef<PosType>(*reinterpret_cast<PosType*>(&vertexData_[index * vertexSize_ + positionOffset_]));
    }

    auto operator[](const size_t index)
    {
        using PosType = glm::vec3;
        return VertexRef<PosType>(*reinterpret_cast<PosType*>(&vertexData_[index * vertexSize_ + positionOffset_]));
    }

    void setVertexPosition(const uint32_t vertexId, const glm::vec3 newPosition)
    {
        (*this)[vertexId].position_ = newPosition;
    }

    void setVertexPositions(const std::vector<glm::vec3> newPositions)
    {
        if (numVertices_ != newPositions.size())
        {
            throw std::runtime_error("[VertexList] Trying to set positions for the wrong number of vertices");
        }

        for (uint32_t iVertex = 0; iVertex < numVertices_; iVertex++)
        {
            (*this)[iVertex].position_ = newPositions[iVertex];
        }
    }

private:
    VertexInfo        vertexInfo_;
    std::vector<char> vertexData_;

    uint32_t positionOffset_;
    uint32_t vertexSize_  = 0;
    uint32_t numVertices_ = 0;

private:
    template <class PosType>
    VertexRef<PosType> get(const size_t index) const
    {
        return VertexRef<PosType>(*reinterpret_cast<PosType*>(&vertexData_[index * vertexSize_ + positionOffset_]));
    }
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VERTEX_LIST_H
