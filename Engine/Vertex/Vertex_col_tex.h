#ifndef MINI_ENGINE_VERTEX_COL_TEX_H
#define MINI_ENGINE_VERTEX_COL_TEX_H

#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/GlmInclude.h"
#include "Engine/MaterialType.h"
#include "Engine/Vertex/VertexInfo.h"

namespace MiniEngine {

class Vertex_col_tex
{
public:
    glm::vec3 position_;
    glm::vec3 color_;
    glm::vec2 texCoord_;

    static inline VertexInfo getVertexInfo()
    {
        VertexInfo info;
        info.size_               = sizeof(Vertex_col_tex);
        info.materialType_       = MatType::color_texture;
        info.hasTexCoord_        = true;
        info.numSamplers_        = 1;
        info.vertexShaderFile_   = "BaseShader.vert.spv";
        info.fragmentShaderFile_ = "BaseShader.frag.spv";
        info.uboTypes_           = {UboType::camera};

        info.bindingDescription_.binding   = 0;
        info.bindingDescription_.stride    = sizeof(Vertex_col_tex);
        info.bindingDescription_.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        info.attributeDescriptions_.push_back({});
        info.attributeDescriptions_.push_back({});
        info.attributeDescriptions_.push_back({});

        info.positionIndex_ = 0;

        info.attributeDescriptions_[0].binding  = 0;
        info.attributeDescriptions_[0].location = 0;                          // location in shader
        info.attributeDescriptions_[0].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in position param
        info.attributeDescriptions_[0].offset   = offsetof(Vertex_col_tex, position_);

        info.attributeDescriptions_[1].binding  = 0;
        info.attributeDescriptions_[1].location = 1;                        // Location in shader
        info.attributeDescriptions_[1].format = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in color param in shader
        info.attributeDescriptions_[1].offset = offsetof(Vertex_col_tex, color_);

        info.attributeDescriptions_[2].binding  = 0;
        info.attributeDescriptions_[2].location = 2;
        info.attributeDescriptions_[2].format   = VK_FORMAT_R32G32_SFLOAT;
        info.attributeDescriptions_[2].offset   = offsetof(Vertex_col_tex, texCoord_);

        return info;
    }

    //    static constexpr MatType                 type               = MatType::color_texture;
    //    static constexpr bool                    hasTexCoord        = true;
    //    static constexpr uint32_t                numSamplers        = 1;
    //    static inline const std::string          vertexShaderFile   = "BaseShader.vert.spv";
    //    static inline const std::string          fragmentShaderFile = "BaseShader.frag.spv";
    //    static inline const std::vector<UboType> uboTypes           = {UboType::camera};

    //    static VkVertexInputBindingDescription getBindingDescription()
    //    {
    //        VkVertexInputBindingDescription bindingDescription = {};

    //        bindingDescription.binding = 0;
    //        bindingDescription.stride  = sizeof(Vertex_col_tex);
    //        bindingDescription.inputRate =
    //            VK_VERTEX_INPUT_RATE_VERTEX; // Move to next data entry after each vertex (wrt instance rate)

    //        return bindingDescription;
    //    }

    //    static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions()
    //    {
    //        std::vector<VkVertexInputAttributeDescription> attributeDescriptions(3);

    //        attributeDescriptions[0].binding  = 0;
    //        attributeDescriptions[0].location = 0;                          // location in shader
    //        attributeDescriptions[0].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in position param in
    //        shader attributeDescriptions[0].offset   = offsetof(Vertex_col_tex, position_);

    //        attributeDescriptions[1].binding  = 0;
    //        attributeDescriptions[1].location = 1;                          // Location in shader
    //        attributeDescriptions[1].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in color param in
    //        shader attributeDescriptions[1].offset   = offsetof(Vertex_col_tex, color_);

    //        attributeDescriptions[2].binding  = 0;
    //        attributeDescriptions[2].location = 2;
    //        attributeDescriptions[2].format   = VK_FORMAT_R32G32_SFLOAT;
    //        attributeDescriptions[2].offset   = offsetof(Vertex_col_tex, texCoord_);

    //        return attributeDescriptions;
    //    }
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VERTEX_COL_TEX_H
