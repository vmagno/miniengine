#ifndef MINI_ENGINE_VERTEX_PHONG_H
#define MINI_ENGINE_VERTEX_PHONG_H

#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/GlmInclude.h"
#include "Engine/MaterialType.h"

namespace MiniEngine {

class Vertex_phong
{
public:
    glm::vec3 position_;
    glm::vec3 normal_;
    glm::vec3 color_;

    static VertexInfo getVertexInfo()
    {
        VertexInfo info;

        info.size_               = sizeof(Vertex_phong);
        info.materialType_       = MatType::phong1Light;
        info.hasTexCoord_        = false;
        info.numSamplers_        = 0;
        info.vertexShaderFile_   = "Phong1Light.vert.spv";
        info.fragmentShaderFile_ = "Phong1Light.frag.spv";
        info.uboTypes_           = {UboType::camera, UboType::light};

        info.bindingDescription_.binding   = 0;
        info.bindingDescription_.stride    = sizeof(Vertex_phong);
        info.bindingDescription_.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        info.attributeDescriptions_.push_back({});
        info.attributeDescriptions_.push_back({});
        info.attributeDescriptions_.push_back({});

        info.positionIndex_ = 0;

        info.attributeDescriptions_[0].binding  = 0;
        info.attributeDescriptions_[0].location = 0;                          // location in shader
        info.attributeDescriptions_[0].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in position param
        info.attributeDescriptions_[0].offset   = offsetof(Vertex_phong, position_);

        info.attributeDescriptions_[1].binding  = 0;
        info.attributeDescriptions_[1].location = 1;                          // Location in shader
        info.attributeDescriptions_[1].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in color param
        info.attributeDescriptions_[1].offset   = offsetof(Vertex_phong, normal_);

        info.attributeDescriptions_[2].binding  = 0;
        info.attributeDescriptions_[2].location = 1;                          // Location in shader
        info.attributeDescriptions_[2].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in color param
        info.attributeDescriptions_[2].offset   = offsetof(Vertex_phong, color_);

        return info;
    }
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VERTEX_PHONG_H
