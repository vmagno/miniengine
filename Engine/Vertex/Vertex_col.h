#ifndef MINI_ENGINE_VERTEX_COL_H
#define MINI_ENGINE_VERTEX_COL_H

#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/GlmInclude.h"
#include "Engine/MaterialType.h"
#include "Engine/Vertex/VertexInfo.h"

namespace MiniEngine {

class Vertex_col
{
public:
    Vertex_col(const glm::vec3 pos, const glm::vec3 col = glm::vec3(1.f)) // White by default
        : position_(pos)
        , color_(col)
    {
    }

    glm::vec3 position_;
    glm::vec3 color_;

    static VertexInfo getVertexInfo()
    {
        VertexInfo info;

        info.size_               = sizeof(Vertex_col);
        info.materialType_       = MatType::color;
        info.hasTexCoord_        = false;
        info.numSamplers_        = 0;
        info.vertexShaderFile_   = "BaseShader_col.vert.spv";
        info.fragmentShaderFile_ = "BaseShader_col.frag.spv";
        info.uboTypes_           = {UboType::camera};

        info.bindingDescription_.binding   = 0;
        info.bindingDescription_.stride    = sizeof(Vertex_col);
        info.bindingDescription_.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        info.attributeDescriptions_.push_back({});
        info.attributeDescriptions_.push_back({});

        info.positionIndex_ = 0;

        info.attributeDescriptions_[0].binding  = 0;
        info.attributeDescriptions_[0].location = 0;                          // location in shader
        info.attributeDescriptions_[0].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in position param
        info.attributeDescriptions_[0].offset   = offsetof(Vertex_col, position_);

        info.attributeDescriptions_[1].binding  = 0;
        info.attributeDescriptions_[1].location = 1;                          // Location in shader
        info.attributeDescriptions_[1].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in color param
        info.attributeDescriptions_[1].offset   = offsetof(Vertex_col, color_);

        return info;
    }
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VERTEX_COL_H
