#ifndef MINI_ENGINE_INDEX_LIST_H
#define MINI_ENGINE_INDEX_LIST_H

#include <cstdint>
#include <cstring>
#include <vector>

namespace MiniEngine {

class IndexList
{
public:
    IndexList() {}

    template <class IndexType>
    IndexList(const std::vector<IndexType>& indices)
        : indexSize_(sizeof(IndexType))
        , numIndices_(static_cast<uint32_t>(indices.size()))
        , indexData_(indices.size() * sizeof(IndexType))
    {
        static_assert(
            std::is_same<IndexType, uint16_t>::value || std::is_same<IndexType, uint32_t>::value,
            "Only 16- and 32-bit unsigned ints are valid index types");

        std::memcpy(indexData_.data(), indices.data(), numIndices_ * indexSize_);
    }

    uint32_t operator[](const size_t index) const
    {
        if (indexSize_ == 2)
        {
            return static_cast<uint32_t>(*reinterpret_cast<const uint16_t*>(&indexData_[index * 2]));
        }
        else if (indexSize_ == 4)
        {
            return *reinterpret_cast<const uint32_t*>(&indexData_[index * 4]);
        }

        return 0;
    }

    auto data() const { return indexData_.data(); }
    auto size() const { return indexData_.size(); }    //!< Total size of index array in bytes
    auto getNumIndices() const { return numIndices_; } //!< Number of elements in the index array
    auto getIndexSize() const { return indexSize_; }

    auto begin() const { return IlIterator(*this, 0); }
    auto end() const { return IlIterator(*this, numIndices_); }

    class IlIterator
    {
    public:
        IlIterator(const IndexList& list, const uint32_t pos)
            : list_(list)
            , position_(pos)
        {
        }

        bool        operator!=(const IlIterator& other) const { return position_ != other.position_; }
        IlIterator& operator++()
        {
            position_++;
            return *this;
        }

        uint32_t operator*() const { return list_[position_]; }

    private:
        const IndexList& list_;
        uint32_t         position_;
    };

private:
    uint32_t             indexSize_  = 0;
    uint32_t             numIndices_ = 0;
    std::vector<uint8_t> indexData_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_INDEX_LIST_H
