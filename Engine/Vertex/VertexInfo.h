#ifndef MINI_ENGINE_VERTEX_INFO_H
#define MINI_ENGINE_VERTEX_INFO_H

#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/MaterialType.h"

namespace MiniEngine {

struct VertexInfo
{
    uint32_t size_         = 0;
    MatType  materialType_ = MatType::none;
    bool     hasTexCoord_  = false;
    uint32_t numSamplers_  = 0;

    int positionIndex_ = -1;

    std::string vertexShaderFile_;
    std::string fragmentShaderFile_;

    std::vector<UboType> uboTypes_;

    VkVertexInputBindingDescription                bindingDescription_;
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VERTEX_INFO_H
