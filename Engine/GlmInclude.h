#ifndef MINI_ENGINE_GLM_INCLUDE_H
#define MINI_ENGINE_GLM_INCLUDE_H

/** \file GlmInclude Ensures the proper flags are always set when including GLM headers
 */

#include <ostream>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

using Real = float;

namespace MiniEngine {
namespace Global {

const glm::vec<3, Real> UP(0.f, 1.f, 0.f);       //!< Definition of UP direction
const glm::vec<3, Real> FORWARD(0.f, 0.f, -1.f); //!< Definition of FORWARD direction
const glm::vec<3, Real> RIGHT(1.f, 0.f, 0.f);    //!< Definition of RIGHT direction

const glm::vec<3, Real> DOWN(-UP);      //!< Definition of UP direction
const glm::vec<3, Real> BACK(-FORWARD); //!< Definition of BACK direction
const glm::vec<3, Real> LEFT(-RIGHT);   //!< Definition of LEFT direction

} // namespace Global

std::ostream& operator<<(std::ostream& out, const glm::vec3& vec);
std::ostream& operator<<(std::ostream& out, const glm::mat3& mat);

} // namespace MiniEngine

#endif // MINI_ENGINE_GLM_INCLUDE_H
