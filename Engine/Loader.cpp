#include "Loader.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include "Vertex/Vertex_col.h"
#include "VulkanCommon.h"

namespace MiniEngine {

const auto& AssimpLoader::getScene()
{
    loadFile();
    return scene_;
}

void AssimpLoader::loadFile()
{
    importer_ = std::make_shared<Assimp::Importer>();
    scene_    = importer_->ReadFile(
        Global::MODEL_DIR + filename_,
        aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);

    if (!scene_)
    {
        std::cerr << "[Loader] Unable to load scene from " << filename_ << ". " << importer_->GetErrorString()
                  << std::endl;
    }
    else
    {
        std::cerr << "[Loader] Successfully loaded scene from " << filename_ << ". " << importer_->GetErrorString()
                  << std::endl;
    }
}

const std::shared_ptr<Model>
    Loader::toModel(const std::shared_ptr<Vk::LogicalDevice> device, const bool isDynamic /* = false */)
{
    const auto scene = sceneLoader_.getScene();

    if (scene != nullptr)
    {
        if (scene->mNumMeshes > 0)
        {
            const auto mesh = scene->mMeshes[0];
            if (mesh->HasPositions() && mesh->mNumFaces > 0 && mesh->mPrimitiveTypes == aiPrimitiveType_TRIANGLE)
            {
                std::cerr << "[Loader] Mesh has " << mesh->mNumFaces << " triangles" << std::endl;

                std::vector<Vertex_col> vertices;
                const auto              pos = mesh->mVertices;
                for (uint32_t iVertex = 0; iVertex < mesh->mNumVertices; iVertex++)
                {
                    vertices.push_back(Vertex_col(glm::vec3(pos[iVertex][0], pos[iVertex][1], pos[iVertex][2])));
                }

                std::vector<uint32_t> indices;
                const auto            faces = mesh->mFaces;
                for (uint32_t iFace = 0; iFace < mesh->mNumFaces; iFace++)
                {
                    indices.push_back(faces[iFace].mIndices[0]);
                    indices.push_back(faces[iFace].mIndices[1]);
                    indices.push_back(faces[iFace].mIndices[2]);
                }

                ModelParams params;
                params.primitiveType_ = PrimitiveType::triangleList;
                params.isDynamic_     = isDynamic;
                return std::make_shared<Model>(device, vertices, indices, params);
            }
            else
            {
                std::cerr << "Not a valid mesh!" << std::endl;
            }
        }
        else
        {
            std::cerr << "No mesh" << std::endl;
        }
    }
    else
    {
        std::cerr << "No scene!!" << std::endl;
    }

    return nullptr;
}

} // namespace MiniEngine
