#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject
{
    mat4 viewMatrix;
    mat4 projMatrix;
}
ubo;

layout(binding = 1) uniform Lighting
{
    vec4 lightPosition;
    vec4 cameraPosition;
}
lighting;

layout(push_constant) uniform Matrix
{
    mat4 val;
}
modelMatrix;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 lightDir;
layout(location = 3) out vec3 cameraDir;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    vec3 worldPosition = vec3(modelMatrix.val * vec4(inPosition, 1.f));
    //    fragNormal = transpose(inverse(mat3(ubo.viewMatrix * modelMatrix.val))) * inNormal;
    fragNormal = mat3(modelMatrix.val) * inNormal; // in world coord

    lightDir = vec3(lighting.lightPosition) - worldPosition; // in world coord
    //    lightDir = vec3(ubo.viewMatrix * vec4(lighting.lightPosition - worldPosition, 1.f));
    cameraDir = vec3(lighting.cameraPosition) - worldPosition; // in world coord
    //    cameraDir = vec3(lighting.cameraPosition); // in world coord

    fragColor   = inColor;
    gl_Position = ubo.projMatrix * ubo.viewMatrix * vec4(worldPosition, 1.0);
}
