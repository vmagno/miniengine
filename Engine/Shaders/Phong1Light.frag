#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec3 lightDir;
layout(location = 3) in vec3 cameraDir;

layout(location = 0) out vec4 outColor;

void main()
{
    vec4 color = vec4(0);

    const vec3 unitLightDir = normalize(lightDir);
    const vec3 unitNormal   = normalize(fragNormal);

    // Ambient component
    const float ambientFactor = 0.2;
    color += vec4(ambientFactor * fragColor, 1.f);

    // Diffuse component
    const float nDotL         = dot(unitNormal, unitLightDir);
    const float diffuseFactor = max(nDotL, 0.f);
    color += vec4(diffuseFactor * fragColor, 1.f);

    // Specular component
    if (diffuseFactor > 0.f)
    {
        const vec3  incident       = -unitLightDir;
        const vec3  reflection     = normalize(reflect(incident, unitNormal));
        const float cosAngle       = max(dot(normalize(cameraDir), reflection), 0.f);
        const float specularFactor = pow(cosAngle, 20.f);
        color += vec4(specularFactor * vec3(1.f, 1.f, 1.f), 1.f);
    }

    outColor = clamp(color, 0.f, 1.f);
    //    outColor = vec4(1.f, 1.f, 1.f, 1.f);
    //    outColor = vec4(normalize(fragNormal), 1.f);
    //    outColor = vec4(normalize(-lightDir), 1.f);
    //    outColor = vec4(normalize(lightDir), 1.f);
    //    outColor = vec4(normalize(cameraDir), 1.f);
}
