#ifndef MINI_ENGINE_VULKAN_COMMON_H
#define MINI_ENGINE_VULKAN_COMMON_H

#include <string>
#include <vector>

#include <vulkan/vulkan.h>

#include "VulkanCommonConfig.h"

namespace MiniEngine {

/// Global constants
namespace Global {

/// Validation layers that we want to activate (when enabled)
const std::vector<const char*> validationLayers = {"VK_LAYER_LUNARG_standard_validation"};

/// Device extensions that we want to use
const std::vector<const char*> deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

const std::string SHADER_DIR  = ShaderDirectoryPathString;
const std::string TEXTURE_DIR = TextureDirectoryPathString;
const std::string MODEL_DIR   = ModelDirectoryPathString;

} // namespace Global

} // namespace MiniEngine

#endif // MINI_ENGINE_VULKAN_COMMON_H
