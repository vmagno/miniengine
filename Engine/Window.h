#ifndef MINI_ENGINE_WINDOW_H
#define MINI_ENGINE_WINDOW_H

#include <iostream>
#include <limits>
#include <memory>
#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "Common.h"
#include "GlmInclude.h"
#include "HighResolutionTimer.h"

#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/SwapChain.h"
#include "Engine/VulkanResources/Synchronization/Fence.h"
#include "Engine/VulkanResources/Synchronization/Semaphore.h"

namespace MiniEngine {

class EventManager;
class Scene;
class View;

class Window
{
public:
    Window(
        const glm::uvec2 resolution, const std::shared_ptr<Vk::VulkanInstance>& instance,
        const std::shared_ptr<EventManager>& eventManager);
    ~Window();

    const std::shared_ptr<Vk::LogicalDevice>& getDevice() const { return device_; }

    /**
     * @brief Perform the steps needed to actually draw a frame based on the previously done setup.
     */
    void drawFrame();

    void setShouldClose();
    bool shouldClose() const;
    void pollEvents();
    void setTitle(const std::string& newTitle);
    void waitDeviceIdle() const;

    void addView(const std::shared_ptr<View>& view);

private:
    GLFWwindow* window_;     //!< GLFW Window that appears on the monitor
    glm::uvec2  resolution_; //!< Resolution of the window in pixels

    std::shared_ptr<Vk::VulkanInstance> vulkanInstance_; //!< Application states, interface with Vulkan
    std::shared_ptr<Vk::LogicalDevice>  device_;         //!< Interface to physical device
    std::shared_ptr<Vk::Surface>        surface_;
    std::shared_ptr<EventManager>       eventManager_;

    std::shared_ptr<Vk::SwapChain> swapChain_; //!< Buffer infrastructure of images waiting to be presented

    std::vector<std::shared_ptr<View>> views_; //!< Views that appear in the window

    //! @name  Sync objects
    //! @{
    std::vector<std::shared_ptr<Vk::Semaphore>> imageAvailableSemaphores_;
    std::vector<std::shared_ptr<Vk::Semaphore>> renderFinishedSemaphores_;
    std::vector<std::shared_ptr<Vk::Fence>>     inFlightFences_;
    uint32_t                                    currentFrame_;
    //! @}

    bool hasResizedFramebuffer_;

    double xLastPos_;
    double yLastPos_;

    //! Maximum number of frames that can be "in flight" (work submitted by CPU, but not processed by GPU yet)
    static const uint32_t MAX_FRAMES_IN_FLIGHT = 2;

    std::shared_ptr<Vk::CommandPool>           commandPool_;
    std::shared_ptr<Vk::VulkanMemoryAllocator> memoryAllocator_;
    std::shared_ptr<Vk::VulkanMemoryAllocator> devOnlyMemoryAllocator_;

private: // functions
    void initWindow();
    void initVulkan();

    const std::shared_ptr<Vk::Surface> createSurface(const std::shared_ptr<Vk::VulkanInstance>& instance);

    /**
     * @brief Create the semaphores and fences that will be used to synchronize the GPU and CPU respectively.
     */
    void createSyncObjects();

    /**
     * @brief Re-create the swap chain and everything that depends on it, as well as other components that depend on
     * window size.
     */
    void recreateSwapChain();

    uint32_t acquireImage();
    void     updateAndSubmitCommandBuffers(const uint32_t imageIndex, const VkSemaphore& signalSemaphore);
    void     presentImage(const uint32_t imageIndex, const VkSemaphore& signalSemaphore);

    static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
    static void keyPressCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void mouseMoveCallback(GLFWwindow* window, double xpos, double ypos);
    static void mouseClickCallback(GLFWwindow* window, int button, int action, int mods);
};

} // namespace MiniEngine

#endif // MINI_ENGINE_WINDOW_H
