#ifndef MINI_ENGINE_VIRTUAL_SCREEN_H
#define MINI_ENGINE_VIRTUAL_SCREEN_H

#include "DisplayItem.h"

namespace MiniEngine {

class Vertex_tex;

struct VirtualScreenParams
{
    // Size param
    glm::vec3  coord_      = glm::vec3(0.5f, 0.5f, 0.0f);
    glm::uvec2 resolution_ = glm::uvec2(960, 540);

    // Sampler param
    Vk::SamplerOptions samplerOptions_ = {VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_ADDRESS_MODE_REPEAT, 0};

    bool deviceOnly_ = true; //!< Whether this texture will be read by the host

    const std::vector<Vertex_tex>             makeVertices() const;
    static inline const std::vector<uint16_t> makeIndices() { return {0, 1, 2, 2, 3, 0}; }
};

class VirtualScreen : public DisplayItem
{
public:
    VirtualScreen(const std::shared_ptr<Vk::LogicalDevice>& device, const VirtualScreenParams& params);
    ~VirtualScreen() override;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_VIRTUAL_SCREEN_H
