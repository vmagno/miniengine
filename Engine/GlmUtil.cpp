#include "GlmInclude.h"

#include <iomanip>

namespace MiniEngine {

std::ostream& operator<<(std::ostream& out, const glm::vec3& vec)
{
    out << vec.x << ", " << vec.y << ", " << vec.z;

    return out;
}

std::ostream& operator<<(std::ostream& out, const glm::mat3& mat)
{
    out << std::endl;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            out << std::setw(10) << std::fixed << std::setprecision(7) << mat[i][j] << ", ";
        }
        out << std::endl;
    }

    return out;
}

} // namespace MiniEngine
