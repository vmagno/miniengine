#ifndef MINI_ENGINE_PHYSICS_BODY_H
#define MINI_ENGINE_PHYSICS_BODY_H

#include "DisplayItem.h"

namespace MiniEngine {

class PhysicsBody : public DisplayItem
{
    using DisplayItem::DisplayItem;

public:
    auto        getMass() const { return mass_; }
    const auto& getVelocity() const { return velocity_; }

    void setMass(const float newMass) { mass_ = newMass; }
    void setVelocity(const glm::vec3& newVelocity) { velocity_ = newVelocity; }

private:
    glm::vec3 velocity_ = glm::vec3(0.0f);
    float     mass_     = 0.0f;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_PHYSICS_BODY_H
