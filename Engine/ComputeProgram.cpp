#include "ComputeProgram.h"

#include <iostream>

#include "Engine/VulkanCommon.h"

namespace MiniEngine {

ComputeProgram::ComputeProgram(const std::shared_ptr<Vk::LogicalDevice> device, const ComputeProgramParams& params)
    : shaderModule_(std::make_shared<Vk::ShaderModule>(device, Global::SHADER_DIR + params.shaderFilename_))
    , fence_(std::make_shared<Vk::Fence>(device, false))
    , numWorkGroups_(params.numWorkGroups_)
{
    const auto descriptorSetLayout = std::make_shared<Vk::DescriptorSetLayout>(
        device, params.uniformBuffers_.size(), 0, params.storageBuffers_.size());
    const auto descriptorPool =
        Vk::DescriptorPool::makeComputeDescriptorPool(device, params.storageBuffers_.size() + 1);
    const auto descriptorSet = std::make_shared<Vk::DescriptorSet>(
        device, descriptorPool, descriptorSetLayout, params.uniformBuffers_, nullptr, params.storageBuffers_);

    init(device, descriptorSetLayout, descriptorSet);
}

float ComputeProgram::run(const VkQueue& queue)
{
    programTimer_.start();
    launch(queue);
    wait();
    programTimer_.stop();

    return programTimer_.getLastTimeMs();
}

void ComputeProgram::launch(const VkQueue& queue)
{
    if (isRunning_)
    {
        std::cerr << "[ComputeProgram] Program was already running!" << std::endl;
    }

    fence_->reset();
    commandBuffer_->submit(queue, fence_);

    isRunning_ = true;
}

void ComputeProgram::wait()
{
    if (isRunning_)
        fence_->wait();

    isRunning_ = false;
}

void ComputeProgram::init(
    const std::shared_ptr<Vk::LogicalDevice>&       device,
    const std::shared_ptr<Vk::DescriptorSetLayout>& descriptorSetLayout,
    const std::shared_ptr<Vk::DescriptorSet>&       descriptorSet)
{
    pipeline_      = std::make_shared<Vk::ComputePipeline>(device, shaderModule_, descriptorSetLayout, descriptorSet);
    commandBuffer_ = std::make_shared<Vk::CommandBuffer>(device);
    commandBuffer_->recordComputeCommand(pipeline_, descriptorSet, numWorkGroups_);
}

} // namespace MiniEngine
