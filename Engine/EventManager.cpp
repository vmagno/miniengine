#include "EventManager.h"

#include <iostream>

#include <GLFW/glfw3.h>

namespace MiniEngine {

void EventManager::handleKeypress(const int key, const int action) const
{
    auto entry = keypressActions_.find(key);

    if (entry != keypressActions_.end())
    {
        entry->second->doTask(action);
        // std::cout << "[EventManager] Performing action for key " << key << std::endl;
    }
}

void EventManager::handleMouseClick(const int button, const int action)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT: mouseButtonState_.isLeftButtonPressed_ = (action == GLFW_PRESS); break;
    case GLFW_MOUSE_BUTTON_RIGHT: mouseButtonState_.isRightButtonPressed_ = (action == GLFW_PRESS); break;
    case GLFW_MOUSE_BUTTON_MIDDLE: mouseButtonState_.isMiddleButtonPressed_ = (action == GLFW_PRESS); break;
    }

    auto entry = mouseClickActions_.find(button);
    if (entry != mouseClickActions_.end())
    {
        entry->second->doTask(action);
    }
}

void EventManager::handleMouseMove(const double xPos, const double yPos)
{
    if (mouseMoveAction_ != nullptr)
    {
        mouseMoveAction_->doTask({xPos, yPos}, {previousXPos_, previousYPos_}, mouseButtonState_);
    }

    previousXPos_ = xPos;
    previousYPos_ = yPos;
}

} // namespace MiniEngine
