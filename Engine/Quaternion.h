#ifndef MINI_ENGINE_QUATERNION_H
#define MINI_ENGINE_QUATERNION_H

#include <ostream>

#include "GlmInclude.h"
#include "Math.h"

namespace Util {

/**
 * @brief Quaternion representation of an orientation or rotation with respect to the frame of reference.
 */
template <class Real>
class Quaternion
{
public:
    /// Default constructor: no rotation
    Quaternion()
        : Quaternion(0, 0, 0, 1)
    {
    }

    Quaternion(glm::vec4& vec)
        : x_(vec.x)
        , y_(vec.y)
        , z_(vec.z)
        , w_(vec.w)
    {
    }

    Quaternion(glm::quat& quat)
        : Quaternion(quat.x, quat.y, quat.z, quat.w)
    {
    }

    Quaternion(const Real x, const Real y, const Real z, const Real w)
        : x_(x)
        , y_(y)
        , z_(z)
        , w_(w)
    {
    }

    /// Compose quaternion with another one (*not* commutative)
    inline const Quaternion operator*(const Quaternion& rhs) const;
    /// Compose this quaternion with another one
    inline const Quaternion& operator*=(const Quaternion& rhs);

    /// Multiply the angle of rotation by a given amount
    inline const Quaternion operator*(const Real rhs) const;

    /// Get axis of rotation as a unit vector
    inline glm::vec<3, Real> getNormalAxis() const { return glm::normalize(getAxis()); }
    /// Get angle of rotation in radians
    inline Real getAngle() const;
    /// Get the axis component of the quaternion (*not* unit)
    inline glm::vec<3, Real> getAxis() const { return glm::vec<3, Real>(x_, y_, z_); }
    /// Get Euler angles of rotation in radians
    inline glm::vec<3, Real> getEulerAngles() const;
    /// Get the norm of the quaternion
    inline Real getNorm() const { return glm::length(asVec4()); }
    /// Return a glm::vec4 representation of the quaternion
    inline glm::vec<4, Real> asVec4() const { return glm::vec<4, Real>(x_, y_, z_, w_); }

    inline void checkNorm() const;

    /// Return a 3x3 matrix R to rotate a vector using v'= Rv
    inline const glm::mat<3, 3, Real> toMat3() const;
    /// Return a 4x4 matrix R to rotate a vector using v'= Rv
    inline const glm::mat<4, 4, Real> toMat4() const;

    /// Rotate vector according to orientation described by this quat. TODO make more efficient
    inline glm::vec<3, Real> rotateVec(const glm::vec<3, Real>& vec) const;

    /// Rotate vector according to reverse orientation described by this quat. TODO make more efficient
    inline glm::vec<3, Real> unrotateVec(const glm::vec<3, Real>& vec) const;

    /// Create a quaternion from a given angle (in radians) and axis of rotation (not necessarily unit)
    inline static const Quaternion fromAngleAxis(const Real angle, const glm::vec<3, Real>& axis);
    /// Create a quaterion from a given basis (better to have an orthonormal basis, not tested otherwise)
    inline static const Quaternion
        fromBasis(const glm::vec<3, Real>& forward, const glm::vec<3, Real>& up, const glm::vec<3, Real>& left);

    /// Create a quaternion from a set of Euler angles (in radians)
    inline static const Quaternion fromEulerAngles(const Real yaw, const Real pitch, const Real roll);

    inline static const glm::vec<3, Real>
        rotateVec(const glm::vec<3, Real>& vec, const Real angle, const glm::vec<3, Real>& axis);

    template <class T>
    inline friend std::ostream& operator<<(std::ostream& out, const Quaternion<T> quat);

private:
    Real x_, y_, z_; //!< Rotation axis (unit times size of the rotation angle)
    Real w_;         //!< Cosine of the angle represented by the quaternion
};

template <class Real>
const Quaternion<Real> Quaternion<Real>::operator*(const Quaternion& rhs) const
{
    Quaternion result;
    const auto axisLeft  = getAxis();
    const auto axisRight = rhs.getAxis();

    result.w_ = w_ * rhs.w_ - glm::dot(axisLeft, axisRight);

    glm::vec<3, Real> resultAxis = w_ * axisRight + rhs.w_ * axisLeft - glm::cross(axisLeft, axisRight);
    result.x_                    = resultAxis.x;
    result.y_                    = resultAxis.y;
    result.z_                    = resultAxis.z;

    return result;
}

template <class Real>
const Quaternion<Real>& Quaternion<Real>::operator*=(const Quaternion& rhs)
{
    *this = *this * rhs;
    return *this;
}

template <class Real>
const Quaternion<Real> Quaternion<Real>::operator*(const Real rhs) const
{
    return fromAngleAxis(getAngle() * rhs, getAxis());
}

template <class Real>
Real Quaternion<Real>::getAngle() const
{
    const Real sinHalfAngle = glm::length(getAxis());
    const Real halfAngle    = std::acos(w_);
    const Real angle        = halfAngle * (sinHalfAngle > 0 ? Real(2) : Real(-2));

    return angle;
}

template <class Real>
glm::vec<3, Real> Quaternion<Real>::getEulerAngles() const
{
    // From http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/

    const Real test = x_ * y_ + z_ * w_;
    if (test > Real(0.4999))
    { // singularity at north pole
        return {2 * std::atan2(x_, w_), M_PI / 2, 0};
    }
    if (test < Real(-0.4999))
    { // singularity at south pole
        return {-2 * std::atan2(x_, w_), -M_PI / 2, 0};
    }

    Real sqx = x_ * x_;
    Real sqy = y_ * y_;
    Real sqz = z_ * z_;

    return {std::atan2(2 * y_ * w_ - 2 * x_ * z_, 1 - 2 * sqy - 2 * sqz), std::asin(2 * test),
            std::atan2(2 * x_ * w_ - 2 * y_ * z_, 1 - 2 * sqx - 2 * sqz)};
}

template <class Real>
void Quaternion<Real>::checkNorm() const
{
    std::cerr << "Checking quat" << std::endl;
    if (std::abs(getNorm() - Real(1)) > Real(1e6) || std::isnan(w_))
    {
        std::cerr << "Quaternion size is not 1!!!! It's " << getNorm() << std::endl;
        std::cerr << *this << std::endl;
    }
}

template <class Real>
const glm::mat<3, 3, Real> Quaternion<Real>::toMat3() const
{
    const Real x2 = x_ * x_;
    const Real y2 = y_ * y_;
    const Real z2 = z_ * z_;
    const Real xy = x_ * y_;
    const Real xz = x_ * z_;
    const Real yz = y_ * z_;
    const Real wx = w_ * x_;
    const Real wy = w_ * y_;
    const Real wz = w_ * z_;

    // GLM is column major by default
    const glm::mat3 result(
        1 - 2 * (y2 + z2), 2 * (xy + wz), 2 * (xz - wy),  // 1st col
        2 * (xy - wz), 1 - 2 * (x2 + z2), 2 * (yz + wx),  // 2nd col
        2 * (xz + wy), 2 * (yz - wx), 1 - 2 * (x2 + y2)); // 3rd col
    return result;
}

template <class Real>
const glm::mat<4, 4, Real> Quaternion<Real>::toMat4() const
{
    return glm::mat<4, 4, Real>(toMat3());
}

template <class Real>
glm::vec<3, Real> Quaternion<Real>::rotateVec(const glm::vec<3, Real>& vec) const
{
    return toMat3() * vec;
}

template <class Real>
glm::vec<3, Real> Quaternion<Real>::unrotateVec(const glm::vec<3, Real>& vec) const
{
    return vec * toMat3();
}

template <class Real>
const glm::vec<3, Real>
    Quaternion<Real>::rotateVec(const glm::vec<3, Real>& vec, const Real angle, const glm::vec<3, Real>& axis)
{
    return fromAngleAxis(angle, axis).rotateVec(vec);
}

template <class Real>
const Quaternion<Real> Quaternion<Real>::fromAngleAxis(const Real angle, const glm::vec<3, Real>& axis)
{
    const auto normal    = glm::normalize(axis);
    const Real halfAngle = angle / Real(2);
    const Real sinHA     = std::sin(halfAngle);

    const Real x = normal.x * sinHA;
    const Real y = normal.y * sinHA;
    const Real z = normal.z * sinHA;
    const Real w = std::cos(halfAngle);

    const auto quat = Quaternion(x, y, z, w);

    //    std::cerr << "Angle: " << angle << std::endl;
    //    std::cerr << "Axis:  " << axis.x << ", " << axis.y << ", " << axis.z << std::endl;
    //    std::cerr << quat << std::endl;

#ifndef NDEBUG
//    quat.checkNorm();
#endif // NDEBUG

    return quat;
}

template <class Real>
const Quaternion<Real> Quaternion<Real>::fromBasis(
    const glm::vec<3, Real>& forward, const glm::vec<3, Real>& up, const glm::vec<3, Real>& left)
{
    using MiniEngine::Global::FORWARD;
    using MiniEngine::Global::LEFT;
    using MiniEngine::Global::RIGHT;
    using MiniEngine::Global::UP;

    Quaternion result;
    Quaternion yaw, pitch, roll;

    auto computeComponent = [&](const glm::vec<3, Real>& direction, const glm::vec<3, Real>& planeNormal,
                                const glm::vec3& axis) {
        // Project vector to only get angle along a certain axis
        // Must rotate the plane normal to take into account already-computed rotations
        glm::vec<3, Real> newDir = Util::projectOnPlane(direction, result.rotateVec(planeNormal));
        const Real        length = glm::length(newDir);
        newDir                   = glm::normalize(newDir);

        // Determine angle
        const Real cosAngle =
            length > VECTOR_LENGTH_ALMOST_ZERO<Real> ? std::min(glm::dot(direction, newDir), Real(1)) : Real(0);
        Real angle = std::acos(cosAngle);
        if (glm::dot(direction, planeNormal) < 0)
            angle = -angle;

        return fromAngleAxis(angle, axis);
    };

    yaw    = computeComponent(forward, LEFT, UP);
    result = yaw;

    pitch  = computeComponent(up, FORWARD, LEFT);
    result = pitch * result; // Apply in reverse order

    roll   = computeComponent(left, UP, FORWARD);
    result = roll * result; // Apply in reverse order

    //    std::cerr << "YAW   " << yaw << std::endl;
    //    std::cerr << "PITCH " << pitch << std::endl;
    //    std::cerr << "ROLL  " << roll << std::endl;

    return result;
}

template <class Real>
inline std::ostream& operator<<(std::ostream& out, const Quaternion<Real> quat)
{
    out << quat.x_ << ", " << quat.y_ << ", " << quat.z_ << ", cos(t/2) = " << quat.w_ << std::endl;

    const auto axis = quat.getNormalAxis();
    out << "Angle: " << glm::degrees(quat.getAngle()) << ", axis: " << axis.x << ", " << axis.y << ", " << axis.z
        << std::endl;

    const auto eulerAngles = quat.getEulerAngles();
    out << "Euler angles: " << glm::degrees(eulerAngles.x) << ", " << glm::degrees(eulerAngles.y) << ", "
        << glm::degrees(eulerAngles.z);

    return out;
}

template <class Real>
const Quaternion<Real> Quaternion<Real>::fromEulerAngles(const Real yaw, const Real pitch, const Real roll)
{
    // See https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

    const Real cosy = std::cos(yaw * 0.5);
    const Real siny = std::sin(yaw * 0.5);
    const Real cosp = std::cos(pitch * 0.5);
    const Real sinp = std::sin(pitch * 0.5);
    const Real cosr = std::cos(roll * 0.5);
    const Real sinr = std::sin(roll * 0.5);

    return Quaternion(
        cosy * cosp * sinr - siny * sinp * cosr, siny * cosp * sinr + cosy * sinp * cosr,
        siny * cosp * cosr - cosy * sinp * sinr, cosy * cosp * cosr + siny * sinp * sinr);
}

} // namespace Util

#endif // MINI_ENGINE_QUATERNION_H
