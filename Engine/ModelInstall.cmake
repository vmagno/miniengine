

foreach(item ${MODEL_FILES})
    get_filename_component(FILE_NAME ${item} NAME)
    set(MOD_DEST "${PROJECT_BINARY_DIR}/Models/${FILE_NAME}")
    add_custom_command(
        OUTPUT ${MOD_DEST}
        COMMAND ${CMAKE_COMMAND} -E make_directory "${PROJECT_BINARY_DIR}/Models/"
        COMMAND ${CMAKE_COMMAND} -E copy ${item} ${MOD_DEST}
        DEPENDS ${item}
    )
    list(APPEND MODEL_DEST_FILES ${MOD_DEST})
endforeach()

add_custom_target(
    ${PROJECT_NAME}_Models ALL
    DEPENDS ${MODEL_DEST_FILES}
    )

add_dependencies(${PROJECT_NAME} ${PROJECT_NAME}_Models)

install(DIRECTORY ${PROJECT_BINARY_DIR}/Models
    DESTINATION ${CMAKE_INSTALL_PREFIX}
    )
