#ifndef MINI_ENGINE_LIGHT_UBO_H
#define MINI_ENGINE_LIGHT_UBO_H

#include "GlmInclude.h"

namespace MiniEngine {

struct LightUbo
{
    glm::vec4 lightPosition_;
    glm::vec4 cameraPosition_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_LIGHT_UBO_H
