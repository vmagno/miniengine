#ifndef MINI_ENGINE_LOADER_H
#define MINI_ENGINE_LOADER_H

#include <string>

#include "Model/Model.h"

struct aiScene;

namespace Assimp {
class Importer;
}

namespace MiniEngine {

class AssimpLoader
{
public:
    AssimpLoader(const std::string& filename)
        : filename_(filename)
    {
    }

    const auto& getScene();

private:
    std::string    filename_;
    const aiScene* scene_ = nullptr;

    std::shared_ptr<Assimp::Importer> importer_;

private:
    void loadFile();
};

class Loader
{
public:
    Loader(const std::string& filename)
        : sceneLoader_(filename)
    {
    }

    const std::shared_ptr<Model> toModel(const std::shared_ptr<Vk::LogicalDevice> device, const bool isDynamic = false);

private:
    AssimpLoader sceneLoader_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_LOADER_H
