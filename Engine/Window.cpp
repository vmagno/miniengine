#include "Window.h"

#include <cstring>
#include <iomanip>
#include <set>

#include "Engine/Camera/Camera.h"
#include "Engine/EventManager.h"
#include "Engine/Scene/Scene.h"
#include "Engine/Util.h"
#include "Engine/View/View.h"
#include "Engine/View/ViewResourceSet.h"

namespace MiniEngine {

Window::Window(
    const glm::uvec2 resolution, const std::shared_ptr<Vk::VulkanInstance>& instance,
    const std::shared_ptr<EventManager>& eventManager)
    : window_(nullptr)
    , resolution_(resolution)
    , vulkanInstance_(instance)
    , eventManager_(eventManager)
    , currentFrame_(0)
    , hasResizedFramebuffer_(false)
    , xLastPos_(0)
    , yLastPos_(0)
{
    initWindow();
    initVulkan();
}

Window::~Window()
{
    glfwDestroyWindow(window_);
}

void Window::initWindow()
{
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window_ =
        glfwCreateWindow(static_cast<int>(resolution_.x), static_cast<int>(resolution_.y), "Vulkan", nullptr, nullptr);

    // Set a resize callback in case the out-of-date flag is not set automatically
    glfwSetFramebufferSizeCallback(window_, framebufferResizeCallback);
    glfwSetWindowUserPointer(window_, this);

    glfwSetKeyCallback(window_, keyPressCallback);
    glfwSetCursorPosCallback(window_, mouseMoveCallback);
    glfwSetMouseButtonCallback(window_, mouseClickCallback);

    glfwGetCursorPos(window_, &xLastPos_, &yLastPos_);
}

void Window::initVulkan()
{
    surface_                = createSurface(vulkanInstance_);
    device_                 = Vk::LogicalDevice::create(surface_, vulkanInstance_);
    commandPool_            = device_->createCommandPool();
    memoryAllocator_        = device_->createMemoryAllocator();
    devOnlyMemoryAllocator_ = device_->createDevOnlyMemoryAllocator();

    swapChain_ = std::make_shared<Vk::SwapChain>(device_, resolution_);

    createSyncObjects();
}

const std::shared_ptr<Vk::Surface> Window::createSurface(const std::shared_ptr<Vk::VulkanInstance>& instance)
{
    VkSurfaceKHR windowSurface;
    checkVk(glfwCreateWindowSurface(instance->get(), window_, nullptr, &windowSurface));
    return std::make_shared<Vk::Surface>(instance, windowSurface);
}

void Window::addView(const std::shared_ptr<View>& view)
{
    view->createResourceSet(swapChain_);
    views_.push_back(view);
}

void Window::createSyncObjects()
{
    imageAvailableSemaphores_.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores_.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences_.resize(MAX_FRAMES_IN_FLIGHT);

    for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
    {
        imageAvailableSemaphores_[i] = std::make_shared<Vk::Semaphore>(device_);
        renderFinishedSemaphores_[i] = std::make_shared<Vk::Semaphore>(device_);
        inFlightFences_[i]           = std::make_shared<Vk::Fence>(device_, true);
    }
}

void Window::drawFrame()
{
    // First wait until the next buffer is ready to be drawn to
    inFlightFences_[currentFrame_]->wait();

    const uint32_t imageIndex = acquireImage();

    if (imageIndex == INVALID)
        return;

    VkSemaphore signalSemaphores[] = {renderFinishedSemaphores_[currentFrame_]->get()};
    updateAndSubmitCommandBuffers(imageIndex, signalSemaphores[0]);
    presentImage(imageIndex, signalSemaphores[0]);

    currentFrame_ = (currentFrame_ + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Window::setShouldClose()
{
    glfwSetWindowShouldClose(window_, GLFW_TRUE);
}

bool Window::shouldClose() const
{
    return glfwWindowShouldClose(window_);
}

void Window::pollEvents()
{
    glfwPollEvents();
}

void Window::setTitle(const std::string& newTitle)
{
    glfwSetWindowTitle(window_, newTitle.c_str());
}

void Window::waitDeviceIdle() const
{
    checkVk(vkDeviceWaitIdle(device_->get()));
}

void Window::recreateSwapChain()
{
    // Handle minimization (buffer size = 0) by just waiting for size to be something else
    int width = 0, height = 0;
    while (width == 0 || height == 0)
    {
        glfwGetFramebufferSize(window_, &width, &height);
        glfwWaitEvents();
    }

    resolution_ = {width, height};

    device_->waitIdle();

    Util::HighResolutionTimer recreateTime;
    recreateTime.start();

    surface_ = createSurface(vulkanInstance_);
    device_->getPhysicalDevice()->setNewSurface(surface_);
    swapChain_->resize(resolution_);
    for (const auto& view : views_)
    {
        view->createResourceSet(swapChain_);
    }

    recreateTime.stop();
    //    std::cerr << "Swap chain took " << std::fixed << std::setprecision(1) << recreateTime.getLastTimeMs()
    //              << " ms to recreate" << std::endl;
}

uint32_t Window::acquireImage()
{
    uint32_t imageIndex;
    VkResult acquireResult = vkAcquireNextImageKHR(
        device_->get(), swapChain_->get(), std::numeric_limits<uint64_t>::max(),
        imageAvailableSemaphores_[currentFrame_]->get(), VK_NULL_HANDLE, &imageIndex);

    if (acquireResult == VK_ERROR_OUT_OF_DATE_KHR)
    {
        //        std::cerr << "OUT OF DATE" << std::endl;
        recreateSwapChain();
        return INVALID;
    }
    else if (acquireResult == VK_SUBOPTIMAL_KHR)
    {
        // Just go on drawing, we already have the image anyway
        //        std::cerr << "SUBOPTIMAL" << std::endl;
    }
    else
    {
        checkVk(acquireResult);
    }

    return imageIndex;
}

void Window::updateAndSubmitCommandBuffers(const uint32_t imageIndex, const VkSemaphore& signalSemaphore)
{
    // Update and retrieve the command buffers that will be executed
    std::vector<VkCommandBuffer> commandBuffers;
    for (const auto& view : views_)
    {
        view->updateBuffers(imageIndex);
        commandBuffers.push_back(view->getResourceSet()->getCommandBuffer(imageIndex)->get());
    }

    VkSubmitInfo submitInfo = {};
    submitInfo.sType        = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore          waitSemaphores[] = {imageAvailableSemaphores_[currentFrame_]->get()};
    VkPipelineStageFlags waitStages[]     = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount         = 1;
    submitInfo.pWaitSemaphores            = waitSemaphores;
    submitInfo.pWaitDstStageMask          = waitStages;
    submitInfo.commandBufferCount         = static_cast<uint32_t>(commandBuffers.size());
    submitInfo.pCommandBuffers            = commandBuffers.data();

    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores    = &signalSemaphore;

    inFlightFences_[currentFrame_]->reset();
    checkVk(vkQueueSubmit(device_->getGraphicsQueue(), 1, &submitInfo, inFlightFences_[currentFrame_]->get()));
}

void Window::presentImage(const uint32_t imageIndex, const VkSemaphore& signalSemaphore)
{
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType            = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores    = &signalSemaphore;

    VkSwapchainKHR swapChains[] = {swapChain_->get()};
    presentInfo.swapchainCount  = 1;
    presentInfo.pSwapchains     = swapChains;
    presentInfo.pImageIndices   = &imageIndex;
    presentInfo.pResults        = nullptr; // Optional. Useful when multiple swap chains, so can check result of each

    VkResult presentResult = vkQueuePresentKHR(device_->getPresentQueue(), &presentInfo);
    if (presentResult == VK_ERROR_OUT_OF_DATE_KHR || presentResult == VK_SUBOPTIMAL_KHR || hasResizedFramebuffer_)
    {
        if (presentResult == VK_ERROR_OUT_OF_DATE_KHR)
        {
            //            std::cerr << "OUT OF DATE (after)" << std::endl;
        }
        else if (presentResult == VK_SUBOPTIMAL_KHR)
        {
            //            std::cerr << "SUBOPTIMAL (after)" << std::endl;
        }
        else
        {
            //            std::cerr << "HAS RESIZED" << std::endl;
        }

        hasResizedFramebuffer_ = false;
        recreateSwapChain();
    }
    else
    {
        checkVk(presentResult);
    }
}

void Window::framebufferResizeCallback(GLFWwindow* window, int /* width */, int /* height */)
{
    // Simply set the resized fb flag to true. Application will take care of the rest
    auto app                    = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    app->hasResizedFramebuffer_ = true;
}

void Window::keyPressCallback(GLFWwindow* window, int key, int /* scancode */, int action, int /* mods */)
{
    auto app = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));

    if (action != GLFW_REPEAT)
        app->eventManager_->handleKeypress(key, action);
}

void Window::mouseMoveCallback(GLFWwindow* window, double xpos, double ypos)
{
    auto app = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    app->eventManager_->handleMouseMove(xpos, ypos);
}

void Window::mouseClickCallback(GLFWwindow* window, int button, int action, int /* mods */)
{
    auto app = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    app->eventManager_->handleMouseClick(button, action);
}

} // namespace MiniEngine
