#ifndef MINI_ENGINE_DISPLAY_ITEM_H
#define MINI_ENGINE_DISPLAY_ITEM_H

#include <memory>
#include <vector>

#include "Engine/VulkanResources/DeviceBuffer/BufferObject.h"
#include "Engine/VulkanResources/LogicalDevice.h"

#include "DisplayProperties.h"
#include "EventManager.h"
#include "GlmInclude.h"
#include "MaterialType.h"
#include "Model/Model.h"
#include "Quaternion.h"
#include "Vertex/IndexList.h"
#include "Vertex/VertexList.h"

namespace MiniEngine {

/**
 * @brief Object that can be displayed as part of a scene.
 */
class DisplayItem : public std::enable_shared_from_this<DisplayItem>
{
public:
    DisplayItem() = delete;

    /**
     * @brief Create a display item from existing rendering resources. All items created from these resources will share
     * them.
     * @param model Visual model/resources of the display item
     * @param position World position of the item (optional)
     * @param scale Scale to apply to the visual model (optional)
     */
    DisplayItem(
        const std::shared_ptr<Model>& model, const glm::vec3& position = glm::vec3(0, 0, 0),
        const glm::vec3& scale = glm::vec3(1, 1, 1));

    DisplayItem(
        const std::shared_ptr<Vk::Texture>& texture, const VertexList& vertices, const IndexList& indices,
        const ModelParams params)
        : DisplayItem(std::make_shared<Model>(vertices, indices, texture, params))
    {
    }

    DisplayItem(
        const std::shared_ptr<Vk::LogicalDevice>& device, const VertexList& vertices, const IndexList& indices,
        const ModelParams params)
        : DisplayItem(std::make_shared<Model>(device, vertices, indices, params))
    {
    }

    virtual ~DisplayItem();

    //!@{ \name Factory methods
    /**
     * @brief Factory function to create a new display item
     * @param device Device on which the item rendering resources will be held
     * @param materialType Type of the material of the item
     * @param textureFile File where the item's texture is located (optional)
     * @param vertices Vertices of the object
     * @param indices Triangles forming the object
     * @param params Parameters to pass for Model construction
     * @return A shared pointer to the newly created item
     */
    template <class Item = DisplayItem, class Vertex, class Index>
    static std::shared_ptr<Item> makeItem(
        const std::shared_ptr<Vk::LogicalDevice>& device, const std::string& textureFile,
        const std::vector<Vertex>& vertices, const std::vector<Index>& indices, const ModelParams params);

    template <class Item = DisplayItem, class Vertex, class Index>
    static std::shared_ptr<Item> makeItem(
        const std::shared_ptr<Vk::Texture>& texture, const std::vector<Vertex>& vertices,
        const std::vector<Index>& indices, const ModelParams params);

    template <class Item, class Vertex, class Index>
    static std::shared_ptr<Item> makeItem(
        const std::shared_ptr<Vk::LogicalDevice>& device, const std::vector<Vertex>& vertices,
        const std::vector<Index>& indices, const ModelParams params);

    template <class Item = DisplayItem>
    static std::shared_ptr<Item> makeWeepingAngelSquare(const std::shared_ptr<Vk::LogicalDevice>& device);

    template <class Item = DisplayItem>
    static std::shared_ptr<Item>
        makeSphere(const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec3& color = {1.f, 1.f, 1.f});

    template <class Item = DisplayItem>
    static std::shared_ptr<Item> makeAxisSet(const std::shared_ptr<Vk::LogicalDevice>& device);

    template <class Item = DisplayItem>
    static std::shared_ptr<Item> makeCube(const std::shared_ptr<Vk::LogicalDevice>& device);
    //!@}

    //!@{ \name Resource getters
    inline const auto& getModel() const { return model_; }
    inline const auto& getVertexBuffer() const { return model_->getVertexBuffer(); }
    inline const auto& getIndexBuffer() const { return model_->getIndexBuffer(); }
    inline const auto& getTexture() const { return model_->getTexture(); }
    inline auto        getMaterialType() const { return model_->getMaterialType(); }
    //!@}

    //!@{ \name Parameter getters
    inline const auto& getPosition() const { return position_; }
    inline const auto& getOrientation() const { return orientation_; }
    inline auto        getPolygonMode() const { return model_->getPolygonMode(); }
    //!@}

    /**
     * @brief Compute the model matrix that will be used in shaders to transform object coordinates to world
     * coordinates, according to the position, orientation and scale of the object.
     * @return The 4x4 model matrix
     */
    const glm::mat4 computeModelMatrix() const;

    /**
     * @brief Perform modifications on the item based on elapsed time.
     * @param deltaT Elapsed time since last animation (in seconds)
     */
    virtual void animate(const float /* deltaT */) {}

    /**
     * @brief Set up the keys that this item listens to/acts on. Must implement doSetupCallbacks for an item to actually
     * acts on user input.
     * @param eventManager The event manager that manages all inputs
     */
    void setupCallbacks(const std::shared_ptr<EventManager>& eventManager);

    //!@{ @name Parameter setters
    inline void setPosition(const glm::vec<3, Real>& newPos) { position_ = newPos; }
    inline void setOrientation(const Util::Quaternion<Real>& newOrientation) { orientation_ = newOrientation; }
    inline void setScale(const glm::vec<3, Real>& newScale) { scale_ = newScale; }
    inline void setScale(const Real newScale) { setScale({newScale, newScale, newScale}); }

    inline void setPolygonMode(const PolygonMode newPolyMode) { model_->setPolygonMode(newPolyMode); }
    inline void setLineWidth(const float newLineWidth) { model_->setLineWidth(newLineWidth); }

    inline void setListenToInput(const bool newListen = true) { listensToInput_ = newListen; }
    //!@}

    void setPositionLimits(const glm::vec<3, Real>& minPos, const glm::vec<3, Real>& maxPos);

    /**
     * @brief Translate the object by the prescribed displacement vector
     * @param displacement Vector of the displacement to add to the object's position
     */
    inline void translate(const glm::vec<3, Real>& displacement)
    {
        position_ = glm::clamp(position_ + displacement, minPos_, maxPos_);
    }

    /**
     * @brief Apply the specified rotation to the object
     * @param rotation Quaternion of the rotation to apply
     */
    inline void rotate(const Util::Quaternion<Real>& rotation);

private:
    std::shared_ptr<Model> model_; //!< Visual model of the object, holds device resources necessary for rendering

protected:
    glm::vec<3, Real>      position_ = glm::vec<3, Real>(0); //!< Position of the object in world coordinates
    Util::Quaternion<Real> orientation_;                     //!< Orientation of the object in world coordinates
    glm::vec<3, Real>      scale_ = glm::vec<3, Real>(1);    //!< Scale of the object along each axis

    glm::vec<3, Real> minPos_ = glm::vec<3, Real>(-100.0);
    glm::vec<3, Real> maxPos_ = glm::vec<3, Real>(100.0);

    //!@{ \name User input management
    bool listensToInput_ = false;

    std::vector<std::pair<int, KeypressTask>> keypressTasks_;  //!< Actions executed when key is pressed
    std::vector<MouseMoveTask>                mouseMoveTasks_; //!< Action executed when mouse moves (only the last one)
    //!@}

private: // functions
    virtual void doSetupCallbacks() {}
};

} // namespace MiniEngine

#include "DisplayItem.inl"

#endif // MINI_ENGINE_DISPLAY_ITEM_H
