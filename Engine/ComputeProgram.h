#ifndef MINI_ENGINE_COMPUTE_PROGRAM_H
#define MINI_ENGINE_COMPUTE_PROGRAM_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/HighResolutionTimer.h"

#include "Engine/VulkanResources/CommandBuffer.h"
#include "Engine/VulkanResources/DescriptorSet.h"
#include "Engine/VulkanResources/DescriptorSetLayout.h"
#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/Pipeline/ComputePipeline.h"
#include "Engine/VulkanResources/ShaderModule.h"
#include "Engine/VulkanResources/Synchronization/Fence.h"

namespace MiniEngine {

struct ComputeProgramParams
{
    std::string shaderFilename_;
    glm::uvec3  numWorkGroups_ = glm::uvec3(1, 1, 1);

    std::vector<std::shared_ptr<Vk::UniformBuffer>> uniformBuffers_;
    std::vector<std::shared_ptr<Vk::DeviceBuffer>>  storageBuffers_;
};

class ComputeProgram
{
public:
    ComputeProgram() = delete;
    ComputeProgram(const std::shared_ptr<Vk::LogicalDevice> device, const ComputeProgramParams& params);

    const auto& getCommandBuffer() const { return commandBuffer_; }

    float run(const VkQueue& queue);
    void  launch(const VkQueue& queue);
    void  wait();

private:
    std::shared_ptr<Vk::ShaderModule>    shaderModule_;
    std::shared_ptr<Vk::CommandBuffer>   commandBuffer_;
    std::shared_ptr<Vk::ComputePipeline> pipeline_;
    std::shared_ptr<Vk::Fence>           fence_;

    glm::uvec3 numWorkGroups_;

    Util::HighResolutionTimer programTimer_;
    bool                      isRunning_ = false;

private: // functions
    void init(
        const std::shared_ptr<Vk::LogicalDevice>&       device,
        const std::shared_ptr<Vk::DescriptorSetLayout>& descriptorSetLayout,
        const std::shared_ptr<Vk::DescriptorSet>&       descriptorSet);
};

} // namespace MiniEngine

#endif // MINI_ENGINE_COMPUTE_PROGRAM_H
