#ifndef MINI_ENGINE_MODEL_H
#define MINI_ENGINE_MODEL_H

#include <memory>
#include <vector>

#include "Engine/VulkanResources/DeviceBuffer/BufferObject.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/Texture/Texture.h"

#include "Engine/DisplayProperties.h"
#include "Engine/MaterialType.h"
#include "Engine/Vertex/IndexList.h"
#include "Engine/Vertex/VertexList.h"
#include "Engine/Vertex/Vertex_col.h"

namespace MiniEngine {

struct ModelParams
{
    PrimitiveType primitiveType_ = PrimitiveType::triangleList;
    bool          isDynamic_     = false;
};

class Model
{
public:
    Model() = delete;

    Model(
        const std::shared_ptr<Vk::LogicalDevice>& device, const VertexList& vertices, const IndexList& indices,
        const ModelParams params = ModelParams())
        : Model(device, nullptr, vertices, indices, params)
    {
    }

    Model(
        const VertexList& vertices, const IndexList& indices, const std::shared_ptr<Vk::Texture>& texture,
        const ModelParams params = ModelParams())
        : Model(texture->getDevice(), texture, vertices, indices, params)
    {
    }

    //!@{ \name Getters
    const std::shared_ptr<Vk::BufferObject>& getVertexBuffer() const { return vertexBuffer_; }
    const std::shared_ptr<Vk::BufferObject>& getIndexBuffer() const { return indexBuffer_; }
    const std::shared_ptr<Vk::Texture>&      getTexture() const { return texture_; }

    auto getNumIndices() const { return indexBuffer_->getNumElements(); }
    auto getNumVertices() const { return vertexBuffer_->getNumElements(); }

    MatType       getMaterialType() const { return materialType_; }
    PrimitiveType getPrimitiveType() const { return primitiveType_; }
    PolygonMode   getPolygonMode() const { return polyMode_; }
    float         getLineWidth() const { return lineWidth_; }
    bool          requiresPipelineUpdate() const { return requirePipelineUpdate_; }

    const auto& getVertices() const { return vertices_; }
    const auto& getIndices() const { return indices_; }
    //!@}

    void setPolygonMode(const PolygonMode newPolyMode);
    void setLineWidth(const float newWidth);
    void resetPipelineUpdate() { requirePipelineUpdate_ = false; }

    //!@{ \name Factory methods
    static std::shared_ptr<Model> makeCube(const std::shared_ptr<Vk::LogicalDevice>& device, const bool dynamic);
    //!@}

    //! @{ \name Dynamic model functions
    void updateBuffer() { vertexBuffer_->copyData(vertices_.data(), vertices_.size()); }

    void moveVertex(const uint32_t vertexId, const glm::vec3 newPosition)
    {
        vertices_[vertexId].position_ = newPosition;
        updateBuffer();
    }

    void setVertices(const VertexList& newVertices)
    {
        vertices_ = newVertices;
        updateBuffer();
    }

    void translateVertices(const std::vector<glm::vec3>& translations)
    {
        vertices_.setVertexPositions(translations);
        updateBuffer();
    }
    //! @}

private:
    std::shared_ptr<Vk::BufferObject> vertexBuffer_;
    std::shared_ptr<Vk::BufferObject> indexBuffer_;
    std::shared_ptr<Vk::Texture>      texture_;

    MatType       materialType_;
    PrimitiveType primitiveType_;                 //!< What kind of primitive (triangle, line, etc.) compose this object
    PolygonMode   polyMode_  = PolygonMode::fill; //!< To draw fill, wireframe or points only
    float         lineWidth_ = 1.0f;

    VertexList vertices_;
    IndexList  indices_;

    bool requirePipelineUpdate_ = false; //!< Whether an update to the graphics pipeline is required

private:
    Model(
        const std::shared_ptr<Vk::LogicalDevice>& device, const std::shared_ptr<Vk::Texture>& texture,
        const VertexList& vertices, const IndexList& indices, const ModelParams params)
        : vertexBuffer_(std::make_shared<Vk::BufferObject>(device, vertices, params.isDynamic_))
        , indexBuffer_(std::make_shared<Vk::BufferObject>(device, indices))
        , texture_(texture)
        , materialType_(vertices.getInfo().materialType_)
        , primitiveType_(params.primitiveType_)
    {
        if (params.isDynamic_)
        {
            vertices_ = vertices;
            indices_  = indices;
        }
    }

protected:
    static const inline std::vector<Vertex_col> cubeVertices = {
        {{-1.0f, -1.0f, -1.0f}, {0.0f, 0.0f, 0.0f}}, //
        {{-1.0f, -1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}},  //
        {{-1.0f, 1.0f, -1.0f}, {0.0f, 1.0f, 0.0f}},  //
        {{-1.0f, 1.0f, 1.0f}, {0.0f, 1.0f, 1.0f}},   //
        {{1.0f, -1.0f, -1.0f}, {1.0f, 0.0f, 0.0f}},  //
        {{1.0f, -1.0f, 1.0f}, {1.0f, 0.0f, 1.0f}},   //
        {{1.0f, 1.0f, -1.0f}, {1.0f, 1.0f, 0.0f}},   //
        {{1.0f, 1.0f, 1.0f}, {1.0f, 1.0f, 1.0f}}     //
    };

    static const inline std::vector<uint16_t> cubeIndices = {
        0, 1, 3, //
        0, 3, 2, //
        0, 2, 6, //
        4, 0, 6, //
        0, 5, 1, //
        0, 4, 5, //
        1, 5, 3, //
        3, 5, 7, //
        5, 4, 6, //
        5, 6, 7, //
        7, 2, 3, //
        7, 6, 2  //
    };
};

} // namespace MiniEngine

#endif // MINI_ENGINE_MODEL_H
