#include "Model.h"

#include "Engine/Vertex/Vertex_col.h"

namespace MiniEngine {

void Model::setPolygonMode(const PolygonMode newPolyMode)
{
    if (newPolyMode != polyMode_)
    {
        polyMode_              = newPolyMode;
        requirePipelineUpdate_ = true;
    }
}

void Model::setLineWidth(const float newWidth)
{
    if (std::abs(lineWidth_ - newWidth) > 0.f)
    {
        lineWidth_             = newWidth;
        requirePipelineUpdate_ = true;
    }
}

std::shared_ptr<Model> MiniEngine::Model::makeCube(const std::shared_ptr<Vk::LogicalDevice>& device, const bool dynamic)
{
    ModelParams params;
    params.isDynamic_ = dynamic;
    return std::make_shared<Model>(device, VertexList(cubeVertices), cubeIndices, params);
}

} // namespace MiniEngine
