
foreach(item ${TEXTURE_FILES})
    get_filename_component(FILE_NAME ${item} NAME)
    set(TEX_DEST "${PROJECT_BINARY_DIR}/Textures/${FILE_NAME}")
    add_custom_command(
        OUTPUT ${TEX_DEST}
        COMMAND ${CMAKE_COMMAND} -E make_directory "${PROJECT_BINARY_DIR}/Textures/"
        COMMAND ${CMAKE_COMMAND} -E copy ${item} ${TEX_DEST}
        DEPENDS ${item}
    )
    list(APPEND TEXTURE_DEST_FILES ${TEX_DEST})
endforeach()

add_custom_target(
    ${PROJECT_NAME}_Textures ALL
    DEPENDS ${TEXTURE_DEST_FILES}
    )

add_dependencies(${PROJECT_NAME} ${PROJECT_NAME}_Textures)

install(DIRECTORY ${PROJECT_BINARY_DIR}/Textures
    DESTINATION ${CMAKE_INSTALL_PREFIX}
    )
