#include "Application.h"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "Engine/Camera/Camera.h"
#include "Engine/EventManager.h"
#include "Engine/Scene/Scene.h"
#include "Engine/View/View.h"
#include "Engine/VulkanResources/VulkanInstance.h"
#include "Engine/Window.h"

namespace MiniEngine {

static const bool useValidationLayers =
#ifdef NDEBUG
    false
#else
    true
#endif
    ;

Application::Application()
    : window_(nullptr)
    , resolution_{800, 600}
    , frameCount_(0)
    , currentFps_(0.f)
{
    static GlfwInitializer GlfwInitializerInstance;
}

Application::~Application()
{
}

void Application::run()
{
    init();
    mainLoop();
}

void Application::init()
{
    vulkanInstance_ = std::make_shared<Vk::VulkanInstance>(useValidationLayers);
    eventManager_   = std::make_shared<EventManager>();
    window_         = std::make_shared<Window>(resolution_, vulkanInstance_, eventManager_);
    device_         = window_->getDevice();

    eventManager_->addKeypressEvent(GLFW_KEY_Q, [this](const int /* action */) { this->window_->setShouldClose(); });
    eventManager_->addKeypressEvent(
        GLFW_KEY_ESCAPE, [this](const int /* action */) { this->window_->setShouldClose(); });

    scene_ = std::make_shared<Scene>();
    initScene();
    initViews();

    for (const auto& view : views_)
    {
        window_->addView(view);
    }

    for (auto& item : scene_->getObjects())
    {
        item->setupCallbacks(eventManager_);
    }
}

void Application::initScene()
{
    auto camera = Camera::makeCamera(device_, {0.0, 0.0, 10.0}, {0.0, 0.0, -1.0}, {0.0, 1.0, 0.0}, false);
    camera->setListenToInput(true);
    scene_->addCamera(camera);
}

void Application::initViews()
{
    views_.push_back(
        std::make_shared<View>(glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), scene_, scene_->getCamera(0)));
}

void Application::animate(const float deltaS)
{
    scene_->animate(deltaS);
}

void Application::mainLoop()
{
    fpsTimer_.start();
    animationTimer_.start();
    outputTimer_.start();

    while (!window_->shouldClose())
    {
        while (fpsTimer_.getTimeSinceLastStartMs() < targetFrameTimeMs_)
        {
        }
        fpsTimer_.start();

        window_->pollEvents();

        const float delta = animationTimer_.getTimeAndRestartMs() / 1000.f;
        animate(delta);

        frameTimer_.start();
        draw();
        frameTimer_.stop();

        frameCount_++;

        // Estimate FPS
        const float elapsed = outputTimer_.getTimeSinceLastStartS();

        if (elapsed > 0.8f)
        {
            currentFps_ = frameCount_ / elapsed;
            frameCount_ = 0;
            outputTimer_.start();

            if (windowTitle_.empty())
            {
                std::stringstream title;
                title << "Vulkan " << currentFps_ << " FPS (potential " << int(1000.f / frameTimer_.getAvgTimeMs())
                      << ")";
                window_->setTitle(title.str());
            }
            else
            {
                window_->setTitle(windowTitle_);
            }

            // std::cout << "Average frame time: " << frameTimer_.getAvgTimeMs() << " ms" << std::endl;
            frameTimer_.reset();
        }
    }

    window_->waitDeviceIdle();
}

void Application::draw()
{
    window_->drawFrame();
}

Application::GlfwInitializer::GlfwInitializer()
{
    glfwInit();
}

Application::GlfwInitializer::~GlfwInitializer()
{
    glfwTerminate();
}

} // namespace MiniEngine
