#ifndef MINI_ENGINE_DISPLAY_PROPERTIES_H
#define MINI_ENGINE_DISPLAY_PROPERTIES_H

#include <vulkan/vulkan.h>

namespace MiniEngine {

enum class PolygonMode
{
    fill,
    line,
    point
};

enum class PrimitiveType
{
    triangleList = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    lineList     = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
    pointList    = VK_PRIMITIVE_TOPOLOGY_POINT_LIST,

    triangleFan   = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,
    triangleStrip = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP
};

} // namespace MiniEngine

#endif // MINI_ENGINE_DISPLAY_PROPERTIES_H
