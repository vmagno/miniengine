#ifndef MINI_ENGINE_PLANE_FORCE_FIELD_H
#define MINI_ENGINE_PLANE_FORCE_FIELD_H

#include "ForceField.h"

namespace MiniEngine {

class PlaneForceField : public ForceField
{
public:
    PlaneForceField(const glm::vec3& position, const glm::vec3& normal, const float stiffness)
        : position_(position)
        , normal_(glm::normalize(normal))
        , stiffness_(stiffness)
    {
    }

    glm::vec3 computeForce(const PhysicsBody& body) const override;

private:
    glm::vec3 position_;
    glm::vec3 normal_;
    float     stiffness_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_PLANE_FORCE_FIELD_H
