#include "UniformForceField.h"

namespace MiniEngine {

glm::vec3 UniformForceField::computeForce(const PhysicsBody&) const
{
    return force_;
}

} // namespace MiniEngine
