#include "PlaneForceField.h"

namespace MiniEngine {

glm::vec3 PlaneForceField::computeForce(const PhysicsBody& body) const
{
    // Like a spring: f = k * dx
    const auto  vector = body.getPosition() - position_;
    const float dist   = glm::dot(vector, normal_);

    glm::vec3 planeForce(0.f);

    if (dist < 0)
        planeForce = normal_ * -dist * stiffness_;

    std::cerr << "Plane force = " << planeForce << std::endl;

    return planeForce;
}

} // namespace MiniEngine
