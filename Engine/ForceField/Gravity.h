#ifndef MINI_ENGINE_GRAVITY_H
#define MINI_ENGINE_GRAVITY_H

#include "ForceField.h"

namespace MiniEngine {

class Gravity : public ForceField
{
public:
    glm::vec3 computeForce(const PhysicsBody& body) const override;

private:
    glm::vec3 acceleration_ = glm::vec3(0.0f, -9.81f, 0.0f);
};

} // namespace MiniEngine

#endif // MINI_ENGINE_GRAVITY_H
