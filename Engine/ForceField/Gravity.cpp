#include "Gravity.h"

namespace MiniEngine {

glm::vec3 Gravity::computeForce(const PhysicsBody& body) const
{
    return body.getMass() * acceleration_;
}

} // namespace MiniEngine
