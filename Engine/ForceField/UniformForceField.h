#ifndef MINI_ENGINE_UNIFORM_FORCE_FIELD_H
#define MINI_ENGINE_UNIFORM_FORCE_FIELD_H

#include "ForceField.h"

namespace MiniEngine {

class UniformForceField : public ForceField
{
public:
    glm::vec3 computeForce(const PhysicsBody&) const override;

private:
    glm::vec3 force_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_UNIFORM_FORCE_FIELD_H
