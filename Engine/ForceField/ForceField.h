#ifndef MINI_ENGINE_FORCE_FIELD_H
#define MINI_ENGINE_FORCE_FIELD_H

#include "Engine/GlmInclude.h"
#include "Engine/PhysicsBody.h"

namespace MiniEngine {

class ForceField
{
public:
    virtual ~ForceField();
    virtual glm::vec3 computeForce(const PhysicsBody& body) const = 0;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_FORCE_FIELD_H
