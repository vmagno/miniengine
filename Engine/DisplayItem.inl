#ifndef MINI_ENGINE_DISPLAY_ITEM_INL
#define MINI_ENGINE_DISPLAY_ITEM_INL

#include "DisplayItem.h"

#include "Engine/Quaternion.h"
#include "Engine/Vertex/Vertex_col.h"
#include "Engine/Vertex/Vertex_col_tex.h"
#include "Engine/Vertex/Vertex_phong.h"
#include "Engine/Vertex/Vertex_tex.h"
#include "Engine/VulkanCommon.h"
#include "Engine/VulkanResources/ShaderModule.h"
#include "Engine/VulkanResources/Texture/HostAccessibleTexture.h"
#include "Engine/VulkanResources/Texture/Texture.h"
#include "Engine/VulkanResources/Texture/VariableTexture.h"

namespace MiniEngine {

using Quat = Util::Quaternion<float>;

void DisplayItem::rotate(const Util::Quaternion<Real>& rotation)
{
    orientation_ *= rotation;
}

template <class Item, class Vertex, class Index>
std::shared_ptr<Item> DisplayItem::makeItem(
    const std::shared_ptr<Vk::LogicalDevice>& device, const std::string& textureFile,
    const std::vector<Vertex>& vertices, const std::vector<Index>& indices, const ModelParams params)
{
    if (textureFile.empty())
        throw std::runtime_error("Must give a texture file name!");

    const auto texture = std::make_shared<Vk::Texture>(device, textureFile);
    return makeItem<Item>(texture, vertices, indices, params);
}

template <class Item, class Vertex, class Index>
std::shared_ptr<Item> DisplayItem::makeItem(
    const std::shared_ptr<Vk::LogicalDevice>& device, const std::vector<Vertex>& vertices,
    const std::vector<Index>& indices, const ModelParams params)
{
    return std::make_shared<Item>(device, vertices, indices, params);
}

template <class Item, class Vertex, class Index>
std::shared_ptr<Item> DisplayItem::makeItem(
    const std::shared_ptr<Vk::Texture>& texture, const std::vector<Vertex>& vertices, const std::vector<Index>& indices,
    const ModelParams params)
{
    if (texture == nullptr)
        throw std::runtime_error("Trying to create a textured item without a texture!");

    return std::make_shared<Item>(texture, vertices, indices, params);
}

template <class Item>
std::shared_ptr<Item> DisplayItem::makeWeepingAngelSquare(const std::shared_ptr<Vk::LogicalDevice>& device)
{
    static std::weak_ptr<DisplayItem> square;

    if (square.lock() == nullptr)
    {
        const std::vector<Vertex_col_tex> objectVertices = {{{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                                                            {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
                                                            {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
                                                            {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}};

        const std::vector<uint16_t> objectIndices = {0, 1, 2, 2, 3, 0};

        const auto item = makeItem<Item>(device, "weeping_angel_512.jpg", objectVertices, objectIndices, ModelParams());
        square          = item;
        return item;
    }
    else
    {
        const auto item = std::make_shared<Item>(square.lock()->getModel());
        return item;
    }
}

template <class Item>
std::shared_ptr<Item> DisplayItem::makeSphere(
    const std::shared_ptr<Vk::LogicalDevice>& device, const glm::vec3& color
    /* = {1, 1, 1} */)
{
    const glm::vec3              base(0.f, 0.f, 1.f);
    const glm::vec3              second(Quat::rotateVec(base, glm::radians(180.f / 3.f), {1.f, 0.f, 0.f}));
    const glm::vec3              third(Quat::rotateVec(
        Quat::rotateVec(base, glm::radians(2 * 180.f / 3.f), {1.f, 0.f, 0.f}), glm::radians(180.f / 5.f),
        {0.f, 0.f, 1.f}));
    const std::vector<glm::vec3> positions = {base,
                                              second,
                                              Quat::rotateVec(second, glm::radians(360.f / 5.f), {0.f, 0.f, 1.f}),
                                              Quat::rotateVec(second, glm::radians(2 * 360.f / 5.f), {0.f, 0.f, 1.f}),
                                              Quat::rotateVec(second, glm::radians(3 * 360.f / 5.f), {0.f, 0.f, 1.f}),
                                              Quat::rotateVec(second, glm::radians(4 * 360.f / 5.f), {0.f, 0.f, 1.f}),
                                              third,
                                              Quat::rotateVec(third, glm::radians(360.f / 5.f), {0.f, 0.f, 1.f}),
                                              Quat::rotateVec(third, glm::radians(2 * 360.f / 5.f), {0.f, 0.f, 1.f}),
                                              Quat::rotateVec(third, glm::radians(3 * 360.f / 5.f), {0.f, 0.f, 1.f}),
                                              Quat::rotateVec(third, glm::radians(4 * 360.f / 5.f), {0.f, 0.f, 1.f}),
                                              base * -1.f};

    std::vector<Vertex_phong> vertices;
    //    std::vector<Vertex_col> vertices;
    for (const auto& p : positions)
    {
        vertices.push_back({p, p, color});
        //        vertices.push_back({p, color});
    }

    const std::vector<uint16_t> indices = {0, 1,  2, 0, 2,  3, 0, 3,  4, 0, 4,  5,  0,  5,  1, // first (top) cap
                                           1, 6,  2, 2, 7,  3, 3, 8,  4, 4, 9,  5,  5,  10, 1, // first half of belt
                                           6, 7,  2, 7, 8,  3, 8, 9,  4, 9, 10, 5,  10, 6,  1, // second half of belt
                                           6, 11, 7, 7, 11, 8, 8, 11, 9, 9, 11, 10, 10, 11, 6};

    const auto item = makeItem<Item>(device, vertices, indices, ModelParams());
    return item;
}

template <class Item>
std::shared_ptr<Item> DisplayItem::makeAxisSet(const std::shared_ptr<Vk::LogicalDevice>& device)
{
    const std::vector<Vertex_col> vertices = {
        {{0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}}, {{1.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}},
        {{0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}}, {{0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}}, {{0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}}};

    const std::vector<uint16_t> indices = {0, 1, 2, 3, 4, 5};

    ModelParams params;
    params.primitiveType_ = PrimitiveType::lineList;
    auto item             = makeItem<Item>(device, vertices, indices, params);
    item->setLineWidth(2.0f);

    return item;
}

template <class Item>
std::shared_ptr<Item> DisplayItem::makeCube(const std::shared_ptr<Vk::LogicalDevice>& device)
{
    auto item = std::make_shared<Item>(Model::makeCube(device, false));
    return item;
}

} // namespace MiniEngine

#endif // MINI_ENGINE_DISPLAY_ITEM_INL
