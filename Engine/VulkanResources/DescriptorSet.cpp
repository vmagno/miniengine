#include "DescriptorSet.h"

#include <vector>

#include "Engine/Util.h"
#include "Engine/VulkanResources/DescriptorSetLayout.h"

namespace MiniEngine {
namespace Vk {

std::shared_ptr<DescriptorSet> DescriptorSet::makeGraphicsDescriptorSet(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
    const std::shared_ptr<DescriptorSetLayout>& layout, const std::vector<std::shared_ptr<UniformBuffer>>& ubos,
    const std::shared_ptr<Texture>& texture)
{
    return std::make_shared<DescriptorSet>(
        device, pool, layout, ubos, texture, std::vector<std::shared_ptr<DeviceBuffer>>());
}

std::shared_ptr<DescriptorSet> DescriptorSet::makeComputeDescriptorSet(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
    const std::shared_ptr<DescriptorSetLayout>& layout, const std::vector<std::shared_ptr<DeviceBuffer>>& buffers)
{
    return std::make_shared<DescriptorSet>(
        device, pool, layout, std::vector<std::shared_ptr<UniformBuffer>>(), nullptr, buffers);
}

DescriptorSet::DescriptorSet(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
    const std::shared_ptr<DescriptorSetLayout>& layout, const std::vector<std::shared_ptr<UniformBuffer>>& ubos,
    const std::shared_ptr<Texture>& texture, const std::vector<std::shared_ptr<DeviceBuffer>>& storageBuffers)
    : device_(device)
    , pool_(pool)
{
    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType                       = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool              = pool_->get();
    allocInfo.descriptorSetCount          = 1;
    allocInfo.pSetLayouts                 = &layout->get();

    checkVk(vkAllocateDescriptorSets(device_->get(), &allocInfo, &descriptorSet_));

    const uint32_t numUbos           = static_cast<uint32_t>(ubos.size());
    const uint32_t numTextures       = texture != nullptr ? 1 : 0;
    const uint32_t numStorageBuffers = static_cast<uint32_t>(storageBuffers.size());

    std::vector<VkWriteDescriptorSet> descriptorWrites(
        numUbos + numTextures + numStorageBuffers, VkWriteDescriptorSet({}));

    std::vector<VkDescriptorBufferInfo> uboInfos(numUbos, VkDescriptorBufferInfo({}));
    for (uint32_t iUbo = 0; iUbo < ubos.size(); iUbo++)
    {
        const auto& ubo = ubos[iUbo];

        uboInfos[iUbo].buffer = ubo->get();
        uboInfos[iUbo].offset = 0;
        uboInfos[iUbo].range  = ubo->getSize();

        const uint32_t index                    = iUbo;
        descriptorWrites[index].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[index].dstSet          = descriptorSet_;
        descriptorWrites[index].dstBinding      = iUbo;
        descriptorWrites[index].dstArrayElement = 0;
        descriptorWrites[index].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[index].descriptorCount = 1;
        descriptorWrites[index].pBufferInfo     = &uboInfos[iUbo];
    }

    VkDescriptorImageInfo imageInfo = {};
    if (texture != nullptr)
    {
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView   = texture->getResource()->getView()->get();
        imageInfo.sampler     = texture->getSampler()->get();

        const uint32_t index                    = numUbos;
        descriptorWrites[index].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[index].dstSet          = descriptorSet_;
        descriptorWrites[index].dstBinding      = numUbos;
        descriptorWrites[index].dstArrayElement = 0;
        descriptorWrites[index].descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[index].descriptorCount = 1;
        descriptorWrites[index].pImageInfo      = &imageInfo;
    }

    std::vector<VkDescriptorBufferInfo> storageInfos(numStorageBuffers, VkDescriptorBufferInfo({}));
    for (uint32_t iStorage = 0; iStorage < numStorageBuffers; iStorage++)
    {
        const auto& storageBuffer = storageBuffers[iStorage];

        storageInfos[iStorage].buffer = storageBuffer->get();
        storageInfos[iStorage].offset = 0;
        storageInfos[iStorage].range  = storageBuffer->getSize();

        const uint32_t index = numUbos + numTextures + iStorage;

        descriptorWrites[index].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[index].dstSet          = descriptorSet_;
        descriptorWrites[index].dstBinding      = iStorage + numUbos + numTextures;
        descriptorWrites[index].dstArrayElement = 0;
        descriptorWrites[index].descriptorType  = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[index].descriptorCount = 1;
        descriptorWrites[index].pBufferInfo     = &storageInfos[iStorage];
    }

    vkUpdateDescriptorSets(
        device_->get(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
}

DescriptorSet::~DescriptorSet()
{
    // Don't free if pool was not created with some flag
    //    vkFreeDescriptorSets(device_->get(), pool_->get(), 1, &descriptorSet_);
}

} // namespace Vk
} // namespace MiniEngine
