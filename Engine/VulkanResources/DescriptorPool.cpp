#include "DescriptorPool.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

std::shared_ptr<DescriptorPool>
    DescriptorPool::makeGraphicsDescriptorPool(const std::shared_ptr<LogicalDevice>& device, const uint32_t numImages)
{
    return std::make_shared<DescriptorPool>(device, numImages, 0);
}

std::shared_ptr<DescriptorPool>
    DescriptorPool::makeComputeDescriptorPool(const std::shared_ptr<LogicalDevice>& device, const uint32_t numSets)
{
    return std::make_shared<DescriptorPool>(device, 0, numSets);
}

DescriptorPool::DescriptorPool(
    const std::shared_ptr<LogicalDevice>& device, const uint32_t numImages, const uint32_t numStorageBuffers)
    : device_(device)
{
    //    std::array<VkDescriptorPoolSize, 2> poolSizes = {};
    std::vector<VkDescriptorPoolSize> poolSizes((numImages > 0 ? 2 : 0) + (numStorageBuffers > 0 ? 1 : 0));

    uint32_t poolSizeCount = 0;
    if (numImages > 0)
    {
        poolSizes[poolSizeCount].type            = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        poolSizes[poolSizeCount].descriptorCount = numImages;
        poolSizeCount++;
        poolSizes[poolSizeCount].type            = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        poolSizes[poolSizeCount].descriptorCount = numImages;
        poolSizeCount++;
    }

    if (numStorageBuffers > 0)
    {
        poolSizes[poolSizeCount].type            = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        poolSizes[poolSizeCount].descriptorCount = numStorageBuffers;
        poolSizeCount++;
    }
    //    poolSizes[0].type            = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    //    poolSizes[0].descriptorCount = numImages;
    //    poolSizes[1].type            = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    //    poolSizes[1].descriptorCount = numImages;

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType                      = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount              = static_cast<uint32_t>(poolSizes.size());
    poolInfo.pPoolSizes                 = poolSizes.data();
    poolInfo.maxSets                    = numImages + numStorageBuffers;

    checkVk(vkCreateDescriptorPool(device_->get(), &poolInfo, nullptr, &pool_));
}

DescriptorPool::~DescriptorPool()
{
    vkDestroyDescriptorPool(device_->get(), pool_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
