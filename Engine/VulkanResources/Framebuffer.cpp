#include "Framebuffer.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

Framebuffer::Framebuffer(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<RenderPass>& renderPass, const uint32_t width,
    const uint32_t height, const std::shared_ptr<ImageView>& swapChainImageView,
    const std::shared_ptr<ImageView>& depthImageView)
    : device_(device)
    , renderPass_(renderPass)
    , imageViews_(1, swapChainImageView)
{
    //    std::array<VkImageView, 2> attachments = {swapChainImageViews_[i], depthImageView_};

    if (depthImageView != nullptr)
    {
        imageViews_.push_back(depthImageView);
    }

    std::vector<VkImageView> attachments;
    for (const auto& imageView : imageViews_)
    {
        attachments.push_back(imageView->get());
    }

    VkFramebufferCreateInfo framebufferInfo = {};
    framebufferInfo.sType                   = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferInfo.renderPass              = renderPass_->get();
    framebufferInfo.attachmentCount         = static_cast<uint32_t>(attachments.size());
    framebufferInfo.pAttachments            = attachments.data();
    framebufferInfo.width                   = width;
    framebufferInfo.height                  = height;
    framebufferInfo.layers                  = 1;

    checkVk(vkCreateFramebuffer(device_->get(), &framebufferInfo, nullptr, &buffer_));
}

Framebuffer::~Framebuffer()
{
    vkDestroyFramebuffer(device_->get(), buffer_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
