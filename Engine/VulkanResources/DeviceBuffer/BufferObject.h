#ifndef MINI_ENGINE_BUFFER_OBJECT_H
#define MINI_ENGINE_BUFFER_OBJECT_H

#include <vector>

#include "Engine/Vertex/IndexList.h"
#include "Engine/Vertex/VertexList.h"
#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"

namespace MiniEngine {
namespace Vk {

class BufferObject : public DeviceBuffer
{
public:
    enum class Usage
    {
        Vertex,
        ChangeableVertex,
        Index,
    };

public:
    BufferObject() = delete;
    BufferObject(const std::shared_ptr<LogicalDevice>& device, const VertexList& vertices, const bool dynamic = false);
    BufferObject(const std::shared_ptr<LogicalDevice>& device, const IndexList& indices);

    uint32_t    getNumElements() const { return numElements_; }
    VkIndexType getIndexType() const { return indexType_; }

private:
    uint32_t    numElements_ = 0;
    VkIndexType indexType_   = VK_INDEX_TYPE_MAX_ENUM;

private: // functions
    static VkBufferUsageFlags usageToUsageFlag(const Usage usage)
    {
        switch (usage)
        {
        case Usage::Vertex:
        case Usage::ChangeableVertex: return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
        case Usage::Index: return VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
        }

        return VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM;
    }

    static VkMemoryPropertyFlags usageToMemoryFlags(const Usage usage)
    {
        switch (usage)
        {
        case Usage::Vertex:
        case Usage::Index: return VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        case Usage::ChangeableVertex: return VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        }

        return VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM;
    }

    static VkIndexType indexType(unsigned int size = 0)
    {
        switch (size)
        {
        case 2: return VK_INDEX_TYPE_UINT16;
        case 4: return VK_INDEX_TYPE_UINT32;
        default: return VK_INDEX_TYPE_MAX_ENUM;
        }
    }
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_BUFFER_OBJECT_H
