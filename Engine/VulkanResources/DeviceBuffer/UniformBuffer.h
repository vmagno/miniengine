#ifndef MINI_ENGINE_UNIFORM_BUFFER_H
#define MINI_ENGINE_UNIFORM_BUFFER_H

#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"

namespace MiniEngine {
namespace Vk {

class UniformBuffer : public DeviceBuffer
{
public:
    UniformBuffer() = delete;
    UniformBuffer(const std::shared_ptr<LogicalDevice>& device, const uint32_t size)
        : DeviceBuffer(
              device, size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
              VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
    {
    }

private:
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_UNIFORM_BUFFER_H
