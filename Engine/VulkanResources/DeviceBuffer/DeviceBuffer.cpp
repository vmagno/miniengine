#include "DeviceBuffer.h"

#include <cstring>

#include "Engine/Util.h"
#include "Engine/VulkanResources/CommandBuffer.h"
#include "Engine/VulkanResources/Image/Image.h"
#include "Engine/VulkanResources/PhysicalDevice.h"
#include "Engine/VulkanResources/VulkanMemoryAllocator.h"

namespace MiniEngine {
namespace Vk {

DeviceBuffer::DeviceBuffer(
    const std::shared_ptr<LogicalDevice>& device, const VkDeviceSize size, const VkBufferUsageFlags usage,
    const VkMemoryPropertyFlags properties)
    : device_(device)
    , allocator_(device->getAllocator())
    , size_(size)
{
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType              = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size               = size_;
    bufferInfo.usage              = usage;
    bufferInfo.sharingMode        = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.preferredFlags          = properties;

    checkVk(vmaCreateBuffer(allocator_->get(), &bufferInfo, &allocInfo, &buffer_, &allocation_, nullptr));
}

DeviceBuffer::~DeviceBuffer()
{
    vmaDestroyBuffer(allocator_->get(), buffer_, allocation_);
}

void DeviceBuffer::copyData(const void* srcData, const size_t size) const
{
    if (size > size_)
    {
        std::cerr << "Trying to copy too much data to device buffer: " << size << " bytes, but buffer only holds "
                  << size_ << " bytes" << std::endl;
    }

    void* dest;
    checkVk(vmaMapMemory(device_->getAllocator()->get(), allocation_, &dest));
    std::memcpy(dest, srcData, static_cast<size_t>(std::min(size, size_)));
    vmaUnmapMemory(device_->getAllocator()->get(), allocation_);
}

void DeviceBuffer::copyToImage(const std::shared_ptr<Image>& image)
{
    auto command = CommandBuffer::singleTimeCommand(device_);

    VkBufferImageCopy region = {};
    region.bufferOffset      = 0;
    region.bufferRowLength   = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel       = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount     = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {image->getResolution().x, image->getResolution().y, 1};

    vkCmdCopyBufferToImage(command->get(), buffer_, image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    command->submitAndWait();
}

void DeviceBuffer::copyToBuffer(const VkBuffer destBuffer)
{
    const auto commandBuffer = CommandBuffer::singleTimeCommand(device_);

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset    = 0; // Optional
    copyRegion.dstOffset    = 0; // Optional
    copyRegion.size         = size_;
    vkCmdCopyBuffer(commandBuffer->get(), buffer_, destBuffer, 1, &copyRegion);

    commandBuffer->submitAndWait();
}

void DeviceBuffer::copyToBuffer(const std::shared_ptr<DeviceBuffer>& destBuffer)
{
    copyToBuffer(destBuffer->get());
}

void DeviceBuffer::copyToHost(void** dest, const size_t size) const
{
    if (size > size_)
    {
        std::cerr << "Trying to copy too much data from device buffer: " << size << " bytes, but buffer only holds "
                  << size_ << " bytes" << std::endl;
    }

    checkVk(vmaMapMemory(device_->getAllocator()->get(), allocation_, dest));
}

void DeviceBuffer::unMap() const
{
    vmaUnmapMemory(allocator_->get(), allocation_);
}

} // namespace Vk
} // namespace MiniEngine
