#ifndef MINI_ENGINE_DEVICE_BUFFER_H
#define MINI_ENGINE_DEVICE_BUFFER_H

#include <memory>

#include "Engine/Vertex/VertexList.h"
#include "Engine/VulkanResources/Image/Image.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/VulkanMemoryAllocator.h"

namespace MiniEngine {
namespace Vk {

/**
 * @brief A buffer allocated on the device
 */
class DeviceBuffer
{
public:
    DeviceBuffer() = delete;

    /**
     * @brief Create a new device buffer and allocate memory for it.
     * @param device Device on which the buffer will be created
     * @param size Size of the buffer in bytes
     * @param usage What the buffer will be used for (staging, reading, etc.)
     * @param properties Properties of the memory to allocate
     */
    DeviceBuffer(
        const std::shared_ptr<LogicalDevice>& device, const VkDeviceSize size, const VkBufferUsageFlags usage,
        const VkMemoryPropertyFlags properties);
    ~DeviceBuffer();

    //!@{ \name Getters
    VkBuffer     get() const { return buffer_; }   //!< Get the Vulkan handle to the buffer
    VkDeviceSize getSize() const { return size_; } //!< Size of the buffer in bytes
    //!@}

    //!@{ \name Copy functions

    //! Copy data from RAM to this buffer
    void copyData(const void* srcData, const size_t size) const;

    template <class T>
    void copyData(const T* srcData, const uint64_t size) const
    {
        copyData(static_cast<const void*>(srcData), size);
    }

    template <class T>
    void copyData(const std::vector<T>& dataVec) const
    {
        copyData(dataVec.data(), dataVec.size() * sizeof(T));
    }

    template <class T>
    void copyData(const T& data) const
    {
        copyData(static_cast<const void*>(&data), sizeof(T));
    }

    void copyToImage(const std::shared_ptr<Image>& image); //!< Copy the content of this buffer to a given image
    void copyToBuffer(const VkBuffer destBuffer);          //!< Copy the content of this buffer to another device buffer
    void copyToBuffer(const std::shared_ptr<DeviceBuffer>& destBuffer); //!< Copy to another device buffer

    void copyToHost(void** dest, const size_t size) const;

    template <class T>
    void copyToHost(T*& dest, const size_t size) const
    {
        copyToHost(reinterpret_cast<void**>(&dest), size);
    }
    //!@}

    //!@{ \name Factory functions
    static std::shared_ptr<DeviceBuffer>
        makeStagingBuffer(const std::shared_ptr<Vk::LogicalDevice>& device, const VkDeviceSize size)
    {
        return std::make_shared<DeviceBuffer>(
            device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    }
    //!@}

    void unMap() const;

private:
    std::shared_ptr<LogicalDevice>         device_;    //!< Device on which the buffer is created
    std::shared_ptr<VulkanMemoryAllocator> allocator_; //!< Handle to the memory allocator for this buffer's memory

protected:
    VkBuffer      buffer_;     //!< Handle to the buffer
    VmaAllocation allocation_; //!< Memory and allocation info
    VkDeviceSize  size_;       //!< Size of the buffer in bytes
};

template <>
inline void DeviceBuffer::copyData(const VertexList& list) const
{
    copyData(list.data(), list.size());
}

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_DEVICE_BUFFER_H
