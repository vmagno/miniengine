#include "BufferObject.h"

namespace MiniEngine {

Vk::BufferObject::BufferObject(
    const std::shared_ptr<Vk::LogicalDevice>& device, const VertexList& vertices, const bool dynamic /* = false */)
    : DeviceBuffer(
          device, vertices.size(), VK_BUFFER_USAGE_TRANSFER_DST_BIT | usageToUsageFlag(Usage::Vertex),
          usageToMemoryFlags(Usage::Vertex))
    , numElements_(vertices.getNumVertices())
    , indexType_(indexType())
{
    if (dynamic)
    {
        copyData(vertices.data(), size_);
    }
    else
    {
        const auto stagingBuffer = DeviceBuffer::makeStagingBuffer(device, size_);
        stagingBuffer->copyData(vertices.data(), size_);
        stagingBuffer->copyToBuffer(buffer_);
    }
}

Vk::BufferObject::BufferObject(const std::shared_ptr<Vk::LogicalDevice>& device, const IndexList& indices)
    : DeviceBuffer(
          device, indices.size(), VK_BUFFER_USAGE_TRANSFER_DST_BIT | usageToUsageFlag(Usage::Index),
          usageToMemoryFlags(Usage::Index))
    , numElements_(indices.getNumIndices())
    , indexType_(indexType(indices.getIndexSize()))
{
    const auto stagingBuffer = DeviceBuffer::makeStagingBuffer(device, size_);
    stagingBuffer->copyData(indices.data(), size_);
    stagingBuffer->copyToBuffer(buffer_);
}

} // namespace MiniEngine
