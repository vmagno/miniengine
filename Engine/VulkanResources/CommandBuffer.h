#ifndef MINI_ENGINE_COMMAND_BUFFER_H
#define MINI_ENGINE_COMMAND_BUFFER_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/CommandPool.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/Pipeline/ComputePipeline.h"
#include "Engine/VulkanResources/Synchronization/Fence.h"

namespace MiniEngine {
namespace Vk {

class CommandBuffer
{
public:
    CommandBuffer() = delete;
    CommandBuffer(const std::shared_ptr<LogicalDevice>& device);
    ~CommandBuffer();

    inline const VkCommandBuffer& get() const { return buffer_; }

    void begin() const;
    void beginOneTime() const;
    void beginSimultaneous() const;
    void end() const;
    void submit(const VkQueue queue, const std::shared_ptr<Vk::Fence>& fence = nullptr) const;
    void submitAndWait() const;

    void recordComputeCommand(
        const std::shared_ptr<ComputePipeline>& pipeline, const std::shared_ptr<DescriptorSet>& descriptorSet,
        const glm::uvec3 numWorkGroups) const;

    static std::shared_ptr<CommandBuffer> singleTimeCommand(const std::shared_ptr<LogicalDevice>& device);

private:
    std::shared_ptr<LogicalDevice> device_; //!< Device on which the buffer is created
    std::shared_ptr<CommandPool>   pool_;   //!< Pool from which the buffer is allocated
    VkCommandBuffer                buffer_; //!< Handle to the buffers
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_COMMAND_BUFFER_H
