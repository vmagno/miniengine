#define VMA_IMPLEMENTATION
#include "VulkanMemoryAllocator.h"

namespace MiniEngine {
namespace Vk {

VulkanMemoryAllocator::VulkanMemoryAllocator(
    const std::shared_ptr<LogicalDevice>& logicalDevice, const std::shared_ptr<PhysicalDevice>& physicalDevice)
{
    VmaAllocatorCreateInfo allocatorInfo = {};
    allocatorInfo.physicalDevice         = physicalDevice->get();
    allocatorInfo.device                 = logicalDevice->get();

    vmaCreateAllocator(&allocatorInfo, &allocator_);
}

VulkanMemoryAllocator::~VulkanMemoryAllocator()
{
    vmaDestroyAllocator(allocator_);
}

} // namespace Vk
} // namespace MiniEngine
