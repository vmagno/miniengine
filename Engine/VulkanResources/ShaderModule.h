#ifndef MINI_ENGINE_SHADER_MODULE_H
#define MINI_ENGINE_SHADER_MODULE_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class ShaderModule
{
public:
    ShaderModule() = delete;
    ShaderModule(const std::shared_ptr<LogicalDevice>& device, const std::string& filename);
    ~ShaderModule();

    VkShaderModule get() const { return module_; }

private:
    std::shared_ptr<LogicalDevice> device_;
    VkShaderModule                 module_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_SHADER_MODULE_H
