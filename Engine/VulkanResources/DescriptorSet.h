#ifndef MINI_ENGINE_DESCRIPTOR_SET_H
#define MINI_ENGINE_DESCRIPTOR_SET_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/DescriptorPool.h"
#include "Engine/VulkanResources/DeviceBuffer/UniformBuffer.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/Texture/Texture.h"

namespace MiniEngine {
namespace Vk {

class DescriptorSet
{
public:
    static std::shared_ptr<DescriptorSet> makeGraphicsDescriptorSet(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
        const std::shared_ptr<DescriptorSetLayout>& layout, const std::vector<std::shared_ptr<UniformBuffer>>& ubos,
        const std::shared_ptr<Texture>& texture);

    static std::shared_ptr<DescriptorSet> makeComputeDescriptorSet(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
        const std::shared_ptr<DescriptorSetLayout>& layout, const std::vector<std::shared_ptr<DeviceBuffer>>& buffers);

    DescriptorSet() = delete;
    DescriptorSet(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
        const std::shared_ptr<DescriptorSetLayout>& layout, const std::vector<std::shared_ptr<UniformBuffer>>& ubos,
        const std::shared_ptr<Texture>& texture, const std::vector<std::shared_ptr<DeviceBuffer>>& storageBuffers);

    ~DescriptorSet();

    const VkDescriptorSet& get() const { return descriptorSet_; }

private:
    DescriptorSet(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorPool>& pool,
        const std::shared_ptr<DescriptorSetLayout>& layout);

private:
    const std::shared_ptr<LogicalDevice>  device_;        //!< Device on which the descriptor set is created
    const std::shared_ptr<DescriptorPool> pool_;          //!< Pool from which the sets are allocated
    VkDescriptorSet                       descriptorSet_; //!< Handle to the descriptorSet
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_DESCRIPTOR_SET_H
