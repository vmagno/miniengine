#include "LogicalDevice.h"

#include <iostream>
#include <unordered_set>

#include "Engine/MaterialType.h"
#include "Engine/Util.h"
#include "Engine/VulkanCommon.h"
#include "Engine/VulkanResources/CommandPool.h"
#include "Engine/VulkanResources/PhysicalDevice.h"
#include "Engine/VulkanResources/VulkanInstance.h"
#include "Engine/VulkanResources/VulkanMemoryAllocator.h"

namespace MiniEngine {
namespace Vk {

LogicalDevice::LogicalDevice(const std::shared_ptr<Surface>& surface, const std::shared_ptr<VulkanInstance>& instance)
    : physicalDevice_(std::make_shared<PhysicalDevice>(surface, instance))
{
    // Assemble info that will be used to initialize the device creation info

    // Find the set of unique queues, so that if a queue has more than one of the required capacities, only one queue is
    // created rather than one per capacity
    std::unordered_set<uint32_t> uniqueQueueFamilies = {physicalDevice_->getGraphicsQueueId(),
                                                        physicalDevice_->getPresentQueueId(),
                                                        physicalDevice_->getComputeQueueId()};

    // Queue creation info
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    const float                          queuePriority = 1.0f;
    for (const uint32_t queueFamily : uniqueQueueFamilies)
    {
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType                   = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex        = queueFamily;
        queueCreateInfo.queueCount              = 1;
        queueCreateInfo.pQueuePriorities        = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    // Device features
    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy        = VK_TRUE;
    deviceFeatures.fillModeNonSolid         = VK_TRUE; // To be able to draw wireframe
    deviceFeatures.wideLines                = VK_TRUE; // To be able to draw lines with width other than 1

    // Initialize the actual creation info
    VkDeviceCreateInfo createInfo   = {};
    createInfo.sType                = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos    = queueCreateInfos.data();
    createInfo.pEnabledFeatures     = &deviceFeatures;

    createInfo.enabledExtensionCount   = static_cast<uint32_t>(Global::deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = Global::deviceExtensions.data();

    createInfo.enabledLayerCount = 0;
    if (instance->usingValidationLayers())
    {
        createInfo.enabledLayerCount   = static_cast<uint32_t>(Global::validationLayers.size());
        createInfo.ppEnabledLayerNames = Global::validationLayers.data();
    }

    // Actual logical device creation. This also create the queues
    checkVk(vkCreateDevice(physicalDevice_->get(), &createInfo, nullptr, &device_));

    // Retrieve handles to queues. Index = 0 since there is only 1 queue (?? could be more than one)
    // TODO verify that index = 0 thingy
    vkGetDeviceQueue(device_, physicalDevice_->getGraphicsQueueId(), 0, &graphicsQueue_);
    vkGetDeviceQueue(device_, physicalDevice_->getPresentQueueId(), 0, &presentQueue_);
    vkGetDeviceQueue(device_, physicalDevice_->getComputeQueueId(), 0, &computeQueue_);
}

LogicalDevice::~LogicalDevice()
{
    vkDestroyDevice(device_, nullptr);
}

void LogicalDevice::waitIdle()
{
    checkVk(vkDeviceWaitIdle(device_));
}

uint32_t LogicalDevice::getGraphicsQueueFamilyIndex() const
{
    return physicalDevice_->getGraphicsQueueId();
}

uint32_t LogicalDevice::getComputeQueueFamilyIndex() const
{
    return physicalDevice_->getComputeQueueId();
}

const std::shared_ptr<MaterialType> LogicalDevice::getMaterialInstance(const MatType materialType)
{
    const auto matId = MaterialType::getMatId(materialType);

    if (materialTypes_.size() <= matId)
    {
        materialTypes_.resize(matId + 1);
    }

    std::shared_ptr<MaterialType> tmpHolder; // To avoid releasing the weak pointer too early
    if (materialTypes_[matId].lock() == nullptr)
    {
        tmpHolder             = MaterialType::createInstance(shared_from_this(), materialType);
        materialTypes_[matId] = tmpHolder;
    }

    return materialTypes_[matId].lock();
}

const std::shared_ptr<CommandPool> LogicalDevice::createCommandPool()
{
    auto handle  = std::make_shared<CommandPool>(shared_from_this());
    commandPool_ = handle;
    return handle;
}

const std::shared_ptr<VulkanMemoryAllocator> LogicalDevice::createMemoryAllocator()
{
    auto handle      = std::make_shared<VulkanMemoryAllocator>(shared_from_this(), physicalDevice_);
    memoryAllocator_ = handle;
    return handle;
}

const std::shared_ptr<VulkanMemoryAllocator> LogicalDevice::createDevOnlyMemoryAllocator()
{
    auto handle             = std::make_shared<VulkanMemoryAllocator>(shared_from_this(), physicalDevice_);
    devOnlyMemoryAllocator_ = handle;
    return handle;
}

} // namespace Vk
} // namespace MiniEngine
