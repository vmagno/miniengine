#ifndef MINI_ENGINE_PHYSICAL_DEVICE_H
#define MINI_ENGINE_PHYSICAL_DEVICE_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/Common.h"
#include "Engine/VulkanResources/Surface.h"

namespace MiniEngine {
namespace Vk {

/**
 * @brief Handle to the physical device used for computing. Contains utility functions for easier query to the device.
 */
class PhysicalDevice
{
public: // Nested structs
    struct QueueFamilyIndices;

    /**
     * @brief Details of swap chain capabilities on a device.
     */
    struct SwapChainSupportDetails
    {
        VkSurfaceCapabilitiesKHR        capabilities_;
        std::vector<VkSurfaceFormatKHR> formats_;
        std::vector<VkPresentModeKHR>   presentModes_;
    };

public:
    PhysicalDevice() = delete;

    PhysicalDevice(const std::shared_ptr<Surface>& windowSurface, const std::shared_ptr<VulkanInstance>& instance);

    //!@{ \name Getters
    inline const VkPhysicalDevice& get() const { return physicalDevice_; } //!< Get the Vulkan handle to the device
    inline uint32_t                getGraphicsQueueId() const { return graphicsFamilyIndex_; }
    inline uint32_t                getPresentQueueId() const { return presentFamilyIndex_; }
    inline uint32_t                getComputeQueueId() const { return computeFamilyIndex_; }

    inline const SwapChainSupportDetails&  getSwapChainSupportDetails() const { return swapChainSupportDetails_; }
    inline const std::shared_ptr<Surface>& getSurface() const { return windowSurface_; }
    //!@}

    void setNewSurface(const std::shared_ptr<Surface>& newSurface);

    /**
     * @brief Check for the swap chain support information. Need to update when the window is resized (a new surface is
     * created)
     */
    void updateSwapChainSupport();

    /**
     * @brief Determine the image format to use for the depth buffer
     * @return The selected format
     */
    VkFormat findDepthFormat();

    /**
     * @brief Find the index of a memory type that has the right type and properties
     * @param typeFilter The memory type we want
     * @param properties The properties we are looking for
     * @return The index of a suitable memory type. Will throw an exception if none is found.
     */
    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const;

private:
    std::shared_ptr<VulkanInstance> instance_;       //!< Vulkan instance associated with the device
    std::shared_ptr<Surface>        windowSurface_;  //!< Surface on which frames will be presented
    VkPhysicalDevice                physicalDevice_; //!< Handle to the device

    uint32_t                graphicsFamilyIndex_;     //!< Index of the graphics queue family selected
    uint32_t                presentFamilyIndex_;      //!< Index of the present queue family selected
    uint32_t                computeFamilyIndex_;      //!< Index of the compute queue family selected
    SwapChainSupportDetails swapChainSupportDetails_; //!< Details of the swap chain as selected

private: // functions
    /**
     * @brief Loop through the available devices on the system and select the most/first appropriate one.
     */
    VkPhysicalDevice selectDevice();

    /**
     * @brief Check for the queue family indices to use. Need to update when the window is resized (a new surface is
     * created)
     */
    void updateQueueFamilies();

    /**
     * @brief Check whether the given device fulfills the criteria needed by the application
     * @param device The physical device to check
     * @return Whether the device can be used by the application
     */
    bool isDeviceSuitable(const VkPhysicalDevice& device) const;

    /**
     * @brief Find the ID of each needed queue family (they may overlap) on the given device and return them in a
     * struct.
     * @param device The physical device on which to check for queue family availability
     * @return The set of indices of required queue families
     */
    const QueueFamilyIndices getAvailableQueueFamilyIndices(const VkPhysicalDevice& device) const;

    /// Check the given device for support for the extensions listed in a global variable
    bool checkDeviceExtensionSupport(const VkPhysicalDevice& device) const;

    /**
     * @brief Verify whether the device supports swap chains (to present images to the screen. Some devices might
     * not have any graphics capability). Might not be strictly necessary if we already checked for the presence of a
     * presentation queue family.
     * @param device The physical device queried
     * @return A struct containing the support details
     */
    const SwapChainSupportDetails querySwapChainSupport(const VkPhysicalDevice& device) const;

    /**
     * @brief Finds whether the (physical) device supports the given formats
     * @param candidates Formats that are desired (ordered by preference)
     * @return The first format in [candidates] that is supported by the device
     */
    VkFormat findSupportedFormat(
        const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

public: // nested structs
    struct QueueFamilyIndices
    {
        uint32_t graphicsFamily_; //!< Index of a queue family with graphics capability
        uint32_t presentFamily_;  //!< Index of a queue family with present capability
        uint32_t computeFamily_;  //!< Index of a queue family with compute capability

        QueueFamilyIndices()
            : graphicsFamily_(INVALID)
            , presentFamily_(INVALID)
            , computeFamily_(INVALID)
        {
        }

        /// Check whether all needed queue families exist
        bool isComplete() const
        {
            return graphicsFamily_ != INVALID && presentFamily_ != INVALID && computeFamily_ != INVALID;
        }
    };
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_PHYSICAL_DEVICE_H
