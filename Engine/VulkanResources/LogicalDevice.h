#ifndef MINI_ENGINE_LOGICAL_DEVICE_H
#define MINI_ENGINE_LOGICAL_DEVICE_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

namespace MiniEngine {

enum class MatType;
class MaterialType;

namespace Vk {

class DescriptorSetLayout;
class CommandPool;
class PhysicalDevice;
class Surface;
class VulkanInstance;
class VulkanMemoryAllocator;

/**
 * @brief A logical device used by a window to interact with a physical device.
 */
class LogicalDevice : public std::enable_shared_from_this<LogicalDevice>
{
public:
    LogicalDevice() = delete;
    ~LogicalDevice();

    static std::shared_ptr<LogicalDevice>
        create(const std::shared_ptr<Surface>& surface, const std::shared_ptr<VulkanInstance>& instance)
    {
        return std::shared_ptr<LogicalDevice>(new LogicalDevice(surface, instance));
    }

    void waitIdle();

    inline VkDevice                              get() const { return device_; }
    uint32_t                                     getGraphicsQueueFamilyIndex() const;
    uint32_t                                     getComputeQueueFamilyIndex() const;
    inline VkQueue                               getGraphicsQueue() const { return graphicsQueue_; }
    inline VkQueue                               getPresentQueue() const { return presentQueue_; }
    inline VkQueue                               getComputeQueue() const { return computeQueue_; }
    const std::shared_ptr<CommandPool>           getCommandPool() { return commandPool_.lock(); }
    const std::shared_ptr<VulkanMemoryAllocator> getAllocator() { return memoryAllocator_.lock(); }
    const std::shared_ptr<VulkanMemoryAllocator> getDevOnlyAllocator() { return devOnlyMemoryAllocator_.lock(); }

    inline const std::shared_ptr<PhysicalDevice>& getPhysicalDevice() const { return physicalDevice_; }

    inline void setPhysicalDevice(const std::shared_ptr<PhysicalDevice>& newPhysicalDevice)
    {
        physicalDevice_ = newPhysicalDevice;
    }

    const std::shared_ptr<MaterialType> getMaterialInstance(const MatType materialType);

    const std::shared_ptr<CommandPool>           createCommandPool();
    const std::shared_ptr<VulkanMemoryAllocator> createMemoryAllocator();
    const std::shared_ptr<VulkanMemoryAllocator> createDevOnlyMemoryAllocator();

private:
    VkDevice                        device_; //!< Handle to the Vulkan logical device (interface with the physical one)
    std::shared_ptr<PhysicalDevice> physicalDevice_;
    std::weak_ptr<CommandPool>      commandPool_;          //!< Command pool associated with the device
    std::weak_ptr<VulkanMemoryAllocator> memoryAllocator_; //!< Used to allocate buffer + image memory on the device
    std::weak_ptr<VulkanMemoryAllocator>
        devOnlyMemoryAllocator_; //!< Used to allocate buffer + image memory on the device

    VkQueue graphicsQueue_; //!< Handle to the graphics queue instance created along with the (logical) device
    VkQueue presentQueue_;  //!< Handle to the present queue instance created along with the (logical) device
    VkQueue computeQueue_;  //!< Handle to the compute queue instance created along with the device

    std::vector<std::weak_ptr<MaterialType>> materialTypes_;

protected:
    LogicalDevice(const std::shared_ptr<Surface>& surface, const std::shared_ptr<VulkanInstance>& instance);
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_LOGICAL_DEVICE_H
