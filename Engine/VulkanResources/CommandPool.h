#ifndef MINI_ENGINE_COMMAND_POOL_H
#define MINI_ENGINE_COMMAND_POOL_H

#include <memory>

#include <vulkan/vulkan.h>

#include "LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class CommandPool
{
public:
    CommandPool() = delete;
    CommandPool(const std::shared_ptr<LogicalDevice>& device);
    ~CommandPool();

    VkCommandPool get() const { return pool_; }

private:
    std::shared_ptr<LogicalDevice> device_; //!< Pointer to device on which the pool is created (circular dependency)
    VkCommandPool                  pool_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_COMMAND_POOL_H
