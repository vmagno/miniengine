#include "GraphicsPipeline.h"

#include "Engine/MaterialType.h"
#include "Engine/Model/Model.h"
#include "Engine/Util.h"
#include "Engine/Vertex/Vertex_col_tex.h"

namespace MiniEngine {
namespace Vk {

GraphicsPipeline::GraphicsPipeline(
    const std::shared_ptr<LogicalDevice>& device, const int32_t offsetX, const int32_t offsetY,
    const glm::uvec2 resolution, const std::shared_ptr<Model>& model, const std::shared_ptr<RenderPass>& renderPass)
    : Pipeline(device)
    //    , pipelineLayout_(nullptr)
    , renderPass_(renderPass)
    , materialType_(device_->getMaterialInstance(model->getMaterialType()))
    , model_(model)
    , offsetX_(offsetX)
    , offsetY_(offsetY)
    , resolution_(resolution)
{
    // TODO put the push constant size in a better place
    pipelineLayout_ =
        std::make_shared<PipelineLayout>(device_, materialType_->getDescriptorSetLayout(), sizeof(glm::mat4));
    create();
}

GraphicsPipeline::~GraphicsPipeline()
{
}

const std::shared_ptr<DescriptorSetLayout>& GraphicsPipeline::getDescriptorSetLayout() const
{
    return materialType_->getDescriptorSetLayout();
}

void GraphicsPipeline::resize(const int32_t newStartX, const int32_t newStartY, const glm::uvec2 newResolution)
{
    offsetX_    = newStartX;
    offsetY_    = newStartY;
    resolution_ = newResolution;
    create();
}

void GraphicsPipeline::updateIfNeeded()
{
    const auto model = model_.lock();
    if (model->requiresPipelineUpdate())
    {
        create();
        model->resetPipelineUpdate();
    }
}

void GraphicsPipeline::create()
{
    if (hasPipeline_)
    {
        vkDestroyPipeline(device_->get(), pipeline_, nullptr);
    }
    else
    {
        hasPipeline_ = true;
    }

    const auto model = model_.lock();

    ////////////////////////
    /// Programmable stages

    // Vertex shader stage
    VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
    vertShaderStageInfo.sType                           = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage                           = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module                          = materialType_->getVertexShader()->get();
    vertShaderStageInfo.pName                           = "main";

    // Fragment shader stage
    VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
    fragShaderStageInfo.sType                           = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage                           = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module                          = materialType_->getFragmentShader()->get();
    fragShaderStageInfo.pName                           = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

    /////////////////
    /// Fixed stages

    auto bindingDescription    = materialType_->getBindingDescription();
    auto attributeDescriptions = materialType_->getAttributeDescriptions();

    // Vertex input (format of vertex data: bindings + attributes)
    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType                                = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount        = 1;
    vertexInputInfo.pVertexBindingDescriptions           = &bindingDescription; // Optional
    vertexInputInfo.vertexAttributeDescriptionCount      = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputInfo.pVertexAttributeDescriptions         = attributeDescriptions.data(); // Optional

    // Input assembly: type of primitives and whether to enable primitive restart (for breaking strips)
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType                                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology                               = static_cast<VkPrimitiveTopology>(model->getPrimitiveType());
    inputAssembly.primitiveRestartEnable                 = VK_FALSE;

    // Viewport
    VkViewport viewport = {};
    viewport.x          = static_cast<float>(offsetX_);
    viewport.y          = static_cast<float>(offsetY_);
    viewport.width      = static_cast<float>(resolution_.x);
    viewport.height     = static_cast<float>(resolution_.y);
    viewport.minDepth   = 0.0f;
    viewport.maxDepth   = 1.0f;

    // Scissor (kind of like a stencil, or a window superimposed on the viewport)
    // Here we just want to draw the whole viewport, so set to the same position and extent
    VkRect2D scissor = {};
    scissor.offset   = {offsetX_, offsetY_};
    scissor.extent   = {resolution_.x, resolution_.y};

    // Viewport state (combination of viewports + scissors, can be multiple of each)
    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount                     = 1;
    viewportState.pViewports                        = &viewport;
    viewportState.scissorCount                      = 1;
    viewportState.pScissors                         = &scissor;

    // Rasterizer
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType                                  = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable                       = VK_FALSE; // wrt near + far planes
    rasterizer.rasterizerDiscardEnable                = VK_FALSE; // Whether to ignore the geometry...
    rasterizer.polygonMode                            = static_cast<VkPolygonMode>(model->getPolygonMode());
    rasterizer.lineWidth                              = model->getLineWidth();           // In number of fragments
    rasterizer.cullMode                               = VK_CULL_MODE_BACK_BIT;           // Front or back or none
    rasterizer.frontFace                              = VK_FRONT_FACE_COUNTER_CLOCKWISE; // Clockwise or counter
    rasterizer.depthBiasEnable                        = VK_FALSE;
    rasterizer.depthBiasConstantFactor                = 0.0f; // Optional
    rasterizer.depthBiasClamp                         = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor                   = 0.0f; // Optional

    // Multisampling (for anti-aliasing)
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType                                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable                  = VK_FALSE;
    multisampling.rasterizationSamples                 = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading                     = 1.0f;     // Optional
    multisampling.pSampleMask                          = nullptr;  // Optional
    multisampling.alphaToCoverageEnable                = VK_FALSE; // Optional
    multisampling.alphaToOneEnable                     = VK_FALSE; // Optional

    // Depth + stencil buffer
    VkPipelineDepthStencilStateCreateInfo depthStencil = {};

    depthStencil.sType                 = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable       = VK_TRUE;
    depthStencil.depthWriteEnable      = VK_TRUE;
    depthStencil.depthCompareOp        = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds        = 0.0f; // Optional
    depthStencil.maxDepthBounds        = 1.0f; // Optional

    depthStencil.stencilTestEnable = VK_FALSE; // No stencil
    depthStencil.front             = {};       // Optional
    depthStencil.back              = {};       // Optional

    // Color blending
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {}; // Blending settings per framebuffer
    colorBlendAttachment.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable         = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;  // Optional
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.colorBlendOp        = VK_BLEND_OP_ADD;      // Optional
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;  // Optional
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.alphaBlendOp        = VK_BLEND_OP_ADD;      // Optional

    VkPipelineColorBlendStateCreateInfo colorBlending = {}; // Global blending settings (contains the per-buffer ones)
    colorBlending.sType                               = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable                       = VK_FALSE;
    colorBlending.logicOp                             = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount                     = 1;
    colorBlending.pAttachments                        = &colorBlendAttachment;
    colorBlending.blendConstants[0]                   = 0.0f; // Optional
    colorBlending.blendConstants[1]                   = 0.0f; // Optional
    colorBlending.blendConstants[2]                   = 0.0f; // Optional
    colorBlending.blendConstants[3]                   = 0.0f; // Optional

    /*
    // Dynamic states (can be configured dynamically, when drawing)
    VkDynamicState dynamicStates[] = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH};

    VkPipelineDynamicStateCreateInfo dynamicState = {};
    dynamicState.sType                            = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount                = 2;
    dynamicState.pDynamicStates                   = dynamicStates;
    */

    // The pipeline!
    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount                   = 2;
    pipelineInfo.pStages                      = shaderStages;
    pipelineInfo.pVertexInputState            = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState          = &inputAssembly;
    pipelineInfo.pViewportState               = &viewportState;
    pipelineInfo.pRasterizationState          = &rasterizer;
    pipelineInfo.pMultisampleState            = &multisampling;
    pipelineInfo.pDepthStencilState           = &depthStencil;
    pipelineInfo.pColorBlendState             = &colorBlending;
    pipelineInfo.pDynamicState                = nullptr; // Optional
    pipelineInfo.layout                       = pipelineLayout_->get();
    pipelineInfo.renderPass                   = renderPass_->get();
    pipelineInfo.subpass                      = 0;
    pipelineInfo.basePipelineHandle           = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex            = -1;             // Optional

    // Actual pipeline creation!
    checkVk(vkCreateGraphicsPipelines(device_->get(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline_));
}

} // namespace Vk
} // namespace MiniEngine
