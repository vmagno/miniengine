#ifndef MINI_ENGINE_PIPELINE_LAYOUT_H
#define MINI_ENGINE_PIPELINE_LAYOUT_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class PipelineLayout
{
public:
    PipelineLayout() = delete;
    PipelineLayout(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorSetLayout>& descriptorSetLayout,
        const uint32_t pushConstantSize);
    ~PipelineLayout();

    VkPipelineLayout get() const { return pipelineLayout_; }

private:
    std::shared_ptr<LogicalDevice>       device_;
    std::shared_ptr<DescriptorSetLayout> descriptorSetLayout_;

    VkPipelineLayout pipelineLayout_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_PIPELINE_LAYOUT_H
