#include "ComputePipeline.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

ComputePipeline::ComputePipeline(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<ShaderModule>& shaderModule,
    const std::shared_ptr<DescriptorSetLayout>& descriptorSetLayout,
    const std::shared_ptr<DescriptorSet>&       descriptorSet)
    : Pipeline(device)
    , shaderModule_(shaderModule)
    , descriptorSetLayout_(descriptorSetLayout)
    , descriptorSet_(descriptorSet)
{
    pipelineLayout_ = std::make_shared<PipelineLayout>(device_, descriptorSetLayout_, 0);
    create();
}

ComputePipeline::~ComputePipeline()
{
}

void ComputePipeline::create()
{
    VkPipelineShaderStageCreateInfo shaderStageCreateInfo = {};
    shaderStageCreateInfo.sType                           = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStageCreateInfo.stage                           = VK_SHADER_STAGE_COMPUTE_BIT;
    shaderStageCreateInfo.module                          = shaderModule_->get();
    shaderStageCreateInfo.pName                           = "main";

    VkComputePipelineCreateInfo pipelineCreateInfo = {};
    pipelineCreateInfo.sType                       = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stage                       = shaderStageCreateInfo;
    pipelineCreateInfo.layout                      = pipelineLayout_->get();

    /*
      Now, we finally create the compute pipeline.
    */
    checkVk(vkCreateComputePipelines(device_->get(), VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &pipeline_));
}

} // namespace Vk
} // namespace MiniEngine
