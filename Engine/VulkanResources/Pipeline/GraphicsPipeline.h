#ifndef MINI_ENGINE_GRAPHICS_PIPELINE_H
#define MINI_ENGINE_GRAPHICS_PIPELINE_H

#include <memory>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/RenderPass.h"
#include "Pipeline.h"

namespace MiniEngine {

enum class MatType;
class Model;

namespace Vk {

class GraphicsPipeline : public Pipeline
{
public:
    //    GraphicsPipeline() = delete;

    /**
     * @brief Constructor
     * @param device Device on which the pipeline is created
     * @param offsetX Starting position of the viewport (horizontally)
     * @param offsetY Starting position of the viewport (vertically)
     * @param resolution Resolution of the viewport in pixels
     * @param model The model that will be displayed with this pipeline
     * @param renderPass Render pass associated with this pipeline
     */
    GraphicsPipeline(
        const std::shared_ptr<LogicalDevice>& device, const int32_t offsetX, const int32_t offsetY,
        const glm::uvec2 resolution, const std::shared_ptr<Model>& model,
        const std::shared_ptr<RenderPass>& renderPass);
    ~GraphicsPipeline() override;

    const std::shared_ptr<PipelineLayout>&      getLayout() const { return pipelineLayout_; }
    const std::shared_ptr<DescriptorSetLayout>& getDescriptorSetLayout() const;
    const std::shared_ptr<MaterialType>&        getMaterialType() const { return materialType_; }

    /**
     * @brief Change the size of the viewport in which the pipeline is drawing
     * @param newStartX New horizontal starting point (in pixels)
     * @param newStartY New vertical starting point (from the top, in pixels)
     * @param newWidth New width (in pixels)
     * @param newHeight New height (in pixels)
     */
    void resize(const int32_t newStartX, const int32_t newStartY, const glm::uvec2 newResolution);

    /**
     * @brief Recreate the pipeline if a change has been marked in the model this pipeline is drawing
     */
    void updateIfNeeded();

private:
    std::shared_ptr<RenderPass>   renderPass_;
    std::shared_ptr<MaterialType> materialType_;
    std::weak_ptr<Model>          model_; //!< Model that will be displayed through this pipeline

    int32_t    offsetX_;    //!< Starting position (in pixels) of the viewport in which this pipeline is drawing
    int32_t    offsetY_;    //!< Starting position (in pixels) of the viewport in which this pipeline is drawing
    glm::uvec2 resolution_; //!< Resolution (in pixels) of the viewport in which this pipeline is drawing

    bool hasPipeline_ = false; //!< Whether the Vulkan object has been created

private: // functions
    /**
     * @brief Create the pipeline on the device, according to the parameters specified by class members.
     */
    void create();
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_GRAPHICS_PIPELINE_H
