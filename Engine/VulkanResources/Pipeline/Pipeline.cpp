#include "Pipeline.h"

namespace MiniEngine {
namespace Vk {

Pipeline::~Pipeline()
{
    vkDestroyPipeline(device_->get(), pipeline_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
