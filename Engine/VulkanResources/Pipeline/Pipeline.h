#ifndef MINI_ENGINE_PIPELINE_H
#define MINI_ENGINE_PIPELINE_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"
#include "PipelineLayout.h"

namespace MiniEngine {
namespace Vk {

class Pipeline
{
public:
    Pipeline() = delete;
    Pipeline(const std::shared_ptr<LogicalDevice>& device)
        : device_(device)
    {
    }

    VkPipeline get() const { return pipeline_; }

    const std::shared_ptr<PipelineLayout> getLayout() const { return pipelineLayout_; }

    virtual ~Pipeline();

protected:
    const std::shared_ptr<LogicalDevice> device_;
    std::shared_ptr<PipelineLayout>      pipelineLayout_;
    VkPipeline                           pipeline_; //!< Handle to the Vulkan pipeline object
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_PIPELINE_H
