#include "PipelineLayout.h"

#include "Engine/GlmInclude.h"
#include "Engine/Util.h"
#include "Engine/VulkanResources/DescriptorSetLayout.h"

namespace MiniEngine {
namespace Vk {

PipelineLayout::PipelineLayout(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DescriptorSetLayout>& descriptorSetLayout,
    const uint32_t pushConstantSize)
    : device_(device)
    , descriptorSetLayout_(descriptorSetLayout)
{
    // Pipeline layout (used for uniform variables, that are dynamically specified)
    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType                      = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount             = 1;                              // Optional
    pipelineLayoutInfo.pSetLayouts                = &(descriptorSetLayout_->get()); // Optional

    if (pushConstantSize > 0)
    {
        VkPushConstantRange pushConstantRange = {};
        pushConstantRange.stageFlags          = VK_SHADER_STAGE_VERTEX_BIT;
        pushConstantRange.offset              = 0;
        pushConstantRange.size                = pushConstantSize;

        pipelineLayoutInfo.pushConstantRangeCount = 1;
        pipelineLayoutInfo.pPushConstantRanges    = &pushConstantRange;
    }

    checkVk(vkCreatePipelineLayout(device_->get(), &pipelineLayoutInfo, nullptr, &pipelineLayout_));
}

PipelineLayout::~PipelineLayout()
{
    vkDestroyPipelineLayout(device_->get(), pipelineLayout_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
