#ifndef MINI_ENGINE_COMPUTE_PIPELINE_H
#define MINI_ENGINE_COMPUTE_PIPELINE_H

#include "Engine/VulkanResources/DescriptorSet.h"
#include "Engine/VulkanResources/Pipeline/Pipeline.h"
#include "Engine/VulkanResources/ShaderModule.h"

namespace MiniEngine {
namespace Vk {

class ComputePipeline : public Pipeline
{
public:
    ComputePipeline(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<ShaderModule>& shaderModule,
        const std::shared_ptr<DescriptorSetLayout>& descriptorSetLayout,
        const std::shared_ptr<DescriptorSet>&       descriptorSet);

    ~ComputePipeline() override;

private:
    const std::shared_ptr<ShaderModule>        shaderModule_;
    const std::shared_ptr<DescriptorSetLayout> descriptorSetLayout_;
    const std::shared_ptr<DescriptorSet>       descriptorSet_;

private: // functions
    void create();
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_COMPUTE_PIPELINE_H
