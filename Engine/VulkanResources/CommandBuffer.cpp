#include "CommandBuffer.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

CommandBuffer::CommandBuffer(const std::shared_ptr<LogicalDevice>& device)
    : device_(device)
    , pool_(device->getCommandPool())
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool                 = device_->getCommandPool()->get();
    allocInfo.commandBufferCount          = 1;

    checkVk(vkAllocateCommandBuffers(device_->get(), &allocInfo, &buffer_));
}

CommandBuffer::~CommandBuffer()
{
    vkFreeCommandBuffers(device_->get(), pool_->get(), 1, &buffer_);
}

void CommandBuffer::begin() const
{
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    checkVk(vkBeginCommandBuffer(buffer_, &beginInfo));
}

void CommandBuffer::beginOneTime() const
{
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags                    = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    checkVk(vkBeginCommandBuffer(buffer_, &beginInfo));
}

void CommandBuffer::beginSimultaneous() const
{
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags                    = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

    checkVk(vkBeginCommandBuffer(buffer_, &beginInfo));
}

void CommandBuffer::end() const
{
    checkVk(vkEndCommandBuffer(buffer_));
}

void CommandBuffer::submit(const VkQueue queue, const std::shared_ptr<Vk::Fence>& fence) const
{
    VkSubmitInfo submitInfo       = {};
    submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers    = &buffer_;

    checkVk(vkQueueSubmit(queue, 1, &submitInfo, fence == nullptr ? VK_NULL_HANDLE : fence->get()));
}

void CommandBuffer::submitAndWait() const
{
    end();

    // Hardcoded to submit on graphics queue for now
    const VkQueue queue = device_->getGraphicsQueue();
    submit(queue);
    checkVk(vkQueueWaitIdle(queue));
}

void CommandBuffer::recordComputeCommand(
    const std::shared_ptr<ComputePipeline>& pipeline, const std::shared_ptr<DescriptorSet>& descriptorSet,
    const glm::uvec3 numWorkGroups) const
{
    begin();

    vkCmdBindPipeline(buffer_, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline->get());
    vkCmdBindDescriptorSets(
        buffer_, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline->getLayout()->get(), 0, 1, &descriptorSet->get(), 0, nullptr);
    vkCmdDispatch(buffer_, numWorkGroups.x, numWorkGroups.y, numWorkGroups.z);

    end();
}

std::shared_ptr<CommandBuffer> CommandBuffer::singleTimeCommand(const std::shared_ptr<LogicalDevice>& device)
{
    auto buffer = std::make_shared<CommandBuffer>(device);
    buffer->beginOneTime();
    return buffer;
}

} // namespace Vk
} // namespace MiniEngine
