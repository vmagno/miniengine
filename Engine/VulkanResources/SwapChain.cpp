#include "SwapChain.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

SwapChain::SwapChain(const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution)
    : device_(device)
{
    create(resolution);
    renderPass_ = std::make_shared<RenderPass>(device_, imageFormat_, depthFormat_);
    createFramebuffers();
}

SwapChain::~SwapChain()
{
    vkDestroySwapchainKHR(device_->get(), swapChain_, nullptr);
}

void SwapChain::createFramebuffers()
{
    const auto imageViews = createImageViews();

    framebuffers_.clear();
    for (const auto& imageView : imageViews)
    {
        framebuffers_.push_back(std::make_shared<Framebuffer>(
            device_, renderPass_, extent_.width, extent_.height, imageView, depthResources_->getView()));
    }
}

void SwapChain::resize(const glm::uvec2 resolution)
{
    vkDestroySwapchainKHR(device_->get(), swapChain_, nullptr);

    create(resolution);
    renderPass_ = std::make_shared<RenderPass>(device_, imageFormat_, depthFormat_);
    createFramebuffers();
}

void SwapChain::create(const glm::uvec2 resolution)
{
    const auto physicalDevice   = device_->getPhysicalDevice();
    surface_                    = physicalDevice->getSurface();
    const auto swapChainSupport = physicalDevice->getSwapChainSupportDetails();

    // Choose some best parameters
    const VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats_);
    const VkPresentModeKHR   presentMode   = chooseSwapPresentMode(swapChainSupport.presentModes_);

    extent_ = chooseSwapExtent(swapChainSupport.capabilities_, {resolution.x, resolution.y});

    uint32_t imageCount = swapChainSupport.capabilities_.minImageCount + 1;
    // Make sure we don't ask for more than max (if max is set to more than 0)
    if (swapChainSupport.capabilities_.maxImageCount > 0 && imageCount > swapChainSupport.capabilities_.maxImageCount)
    {
        imageCount = swapChainSupport.capabilities_.maxImageCount;
    }

    // Creation info
    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType                    = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface                  = surface_->get();
    createInfo.minImageCount            = imageCount;
    createInfo.imageFormat              = surfaceFormat.format;
    createInfo.imageColorSpace          = surfaceFormat.colorSpace;
    createInfo.imageExtent              = extent_;
    createInfo.imageArrayLayers         = 1;                                   // 1 unless you want stereo
    createInfo.imageUsage               = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; // We render directly to the images

    uint32_t queueFamilyIndices[] = {physicalDevice->getGraphicsQueueId(), physicalDevice->getPresentQueueId()};

    // Choose image sharing mode
    if (physicalDevice->getGraphicsQueueId() != physicalDevice->getPresentQueueId())
    {
        createInfo.imageSharingMode      = VK_SHARING_MODE_CONCURRENT; // Can share without thinking about synching
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices   = queueFamilyIndices;
    }
    else
    {
        createInfo.imageSharingMode      = VK_SHARING_MODE_EXCLUSIVE; // Only one queue, so use the more efficient mode
        createInfo.queueFamilyIndexCount = 0;                         // Optional
        createInfo.pQueueFamilyIndices   = nullptr;                   // Optional
    }

    createInfo.preTransform   = swapChainSupport.capabilities_.currentTransform; // Don't apply any transform on images
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;               // Don't blend with other windows
    createInfo.presentMode    = presentMode;
    createInfo.clipped        = VK_TRUE;        // Don't care about pixels when they are occluded by another window
    createInfo.oldSwapchain   = VK_NULL_HANDLE; // Assume we only ever create one swap chain

    // Actual swap chain creation
    checkVk(vkCreateSwapchainKHR(device_->get(), &createInfo, nullptr, &swapChain_));

    // Remember some variables
    numImages_   = imageCount;
    imageFormat_ = surfaceFormat.format;

    depthFormat_ = device_->getPhysicalDevice()->findDepthFormat();

    createDepthResources();
    createImageViews();
}

const std::vector<std::shared_ptr<ImageView>> SwapChain::createImageViews()
{
    // Retrieve image handles
    std::vector<VkImage> images;
    vkGetSwapchainImagesKHR(device_->get(), swapChain_, &numImages_, nullptr);
    images.resize(numImages_);
    vkGetSwapchainImagesKHR(device_->get(), swapChain_, &numImages_, images.data());

    std::vector<std::shared_ptr<ImageView>> imageViews;
    for (const VkImage image : images)
    {
        imageViews.push_back(std::make_shared<ImageView>(device_, image, imageFormat_, VK_IMAGE_ASPECT_COLOR_BIT));
    }

    return imageViews;
}

void SwapChain::createDepthResources()
{
    depthResources_ = std::make_shared<ImageResource>(
        device_, glm::uvec2(extent_.width, extent_.height), depthFormat_, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

    // Only need to transition it once, so can do it here rather than during the render pass
    depthResources_->getImage()->transitionLayout(
        depthFormat_, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
}

const VkSurfaceFormatKHR SwapChain::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
    // No preferred format, so we simply choose what we figure is best
    if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
    {
        return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    }

    // Loop through available formats and select the one we're looking for if it's there
    for (const auto& availableFormat : availableFormats)
    {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM &&
            availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            return availableFormat;
        }
    }

    // Didn't find the one we wanted, so just take the first one
    return availableFormats[0];
}

VkPresentModeKHR SwapChain::chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes)
{
    // VK_PRESENT_MODE_FIFO_KHR is guaranteed to be there, so just return it if the preferred option is not available
    VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

    // Choose VK_PRESENT_MODE_MAILBOX_KHR if it's available (something like triple buffering)
    // Since VK_PRESENT_MODE_FIFO_KHR is not always *well* supported, we also prefer VK_PRESENT_MODE_IMMEDIATE_KHR
    for (const auto& availablePresentMode : availablePresentModes)
    {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            return availablePresentMode;
        }
        else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
        {
            bestMode = availablePresentMode;
        }
    }

    return bestMode;
}

const VkExtent2D
    SwapChain::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, const VkExtent2D& desiredExtent)
{
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    {
        // Window manager does not allow an extent different from the resolution of the window
        return capabilities.currentExtent;
    }
    else
    {
        // Choose the best extent that fits between minImageExtent and maxImageExtent
        VkExtent2D actualExtent = desiredExtent;

        actualExtent.width = std::max(
            capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(
            capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

} // namespace Vk
} // namespace MiniEngine
