#ifndef MINI_ENGINE_VULKAN_INSTANCE_H
#define MINI_ENGINE_VULKAN_INSTANCE_H

#include <vector>

#include <vulkan/vulkan.h>

namespace MiniEngine {
namespace Vk {

/**
 * @brief Class that contains application states and interfaces with Vulkan.
 */
class VulkanInstance
{
public:
    VulkanInstance() = delete;
    /**
     * @brief Create the instance
     * @param enableValidationLayers Whether to use validation layers
     */
    VulkanInstance(const bool enableValidationLayers);
    ~VulkanInstance();

    //!@{ \name Getters
    inline const VkInstance& get() const { return instance_; } //!< Get Vulkan handle to the instance
    inline bool              usingValidationLayers() const { return areValidationLayersActive_; }
    //!@}

    /**
     * @brief Get the list of physical devices accessible through this instance
     * @return The list of VkPhysicalDevice objects
     */
    const std::vector<VkPhysicalDevice> getPhysicalDevices() const;

private:
    bool                     areValidationLayersActive_; //!< Whether we are using the validation layers
    VkInstance               instance_;                  //!< Application states, interface with Vulkan
    VkDebugReportCallbackEXT debugCallback_;             //!< Function called by the validation layers

private: // functions
    /**
     * @brief Check whether all validation layers named in Global::validationLayers are supported by the device/driver.
     * @return True if all requested layers are supported, false otherwise
     */
    bool checkValidationLayerSupport();

    /**
     * @brief Gather the list of extension required by the application to work properly. Currently needs some
     * extensions for GLFW and for the validation layers.
     * @return The list of extension names
     */
    std::vector<const char*> getRequiredExtensions();

    /**
     * @brief Create a debug callback. Calls createDebugReportCallbackEXT().
     */
    void createDebugCallback();

    /**
     * @brief Function called by the validation layers when an error occurs
     * @param flags
     * @param objType
     * @param obj
     * @param location
     * @param code
     * @param layerPrefix
     * @param msg
     * @param userData
     * @return
     */
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code,
        const char* layerPrefix, const char* msg, void* userData);

    /**
     * @brief We want to create a debug report callback object, but the create() function is not loaded, so we need to
     * first find its address. This is done by calling vkGetInstanceProcAddr(). We then directly call the function (if
     * successful) and return the result, or return an error.
     * @param pCreateInfo Debug callback create info
     * @return The result of the callback creation function or an error code (if the function was not found)
     */
    VkResult createDebugReportCallbackEXT(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo);

    /**
     * @brief Similarly to createDebugReportCallbackEXT(), the destroy function for the callback is not loaded, so we
     * first need to find it, then call it.
     */
    void destroyDebugReportCallbackEXT();
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_VULKAN_INSTANCE_H
