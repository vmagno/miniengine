#include "CommandPool.h"

#include "Engine/Util.h"
#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

CommandPool::CommandPool(const std::shared_ptr<LogicalDevice>& device)
    : device_(device)
{
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType                   = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex        = device_->getGraphicsQueueFamilyIndex();
    poolInfo.flags                   = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional

    checkVk(vkCreateCommandPool(device_->get(), &poolInfo, nullptr, &pool_));
}

CommandPool::~CommandPool()
{
    vkDestroyCommandPool(device_->get(), pool_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
