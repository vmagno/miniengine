#include "DescriptorSetLayout.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

DescriptorSetLayout::DescriptorSetLayout(
    const std::shared_ptr<LogicalDevice>& device, const unsigned int numUbos, const unsigned int numSamplers,
    const unsigned int numComputeStorageBuffers)
    : device_(device)
    , numUbos_(numUbos)
    , numSamplers_(numSamplers)
    , numComputeStorageBuffers_(numComputeStorageBuffers)
{
    std::vector<VkDescriptorSetLayoutBinding> bindings(numUbos_ + numSamplers_ + numComputeStorageBuffers_);

    for (unsigned int iUbo = 0; iUbo < numUbos_; iUbo++)
    {
        bindings[iUbo]                    = {};
        bindings[iUbo].binding            = iUbo;
        bindings[iUbo].descriptorCount    = 1;
        bindings[iUbo].descriptorType     = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        bindings[iUbo].stageFlags         = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT;
        bindings[iUbo].pImmutableSamplers = nullptr;
    }

    for (unsigned int iSampler = 0; iSampler < numSamplers_; iSampler++)
    {
        const unsigned int index = iSampler + numUbos_;

        bindings[index]                    = {};
        bindings[index].binding            = iSampler + numUbos_;
        bindings[index].descriptorCount    = 1;
        bindings[index].descriptorType     = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        bindings[index].stageFlags         = VK_SHADER_STAGE_FRAGMENT_BIT;
        bindings[index].pImmutableSamplers = nullptr;
    }

    for (unsigned int iStorage = 0; iStorage < numComputeStorageBuffers_; iStorage++)
    {
        const unsigned int index = iStorage + numUbos_ + numSamplers_;

        bindings[index]                    = {};
        bindings[index].binding            = iStorage + numUbos_ + numSamplers_;
        bindings[index].descriptorCount    = 1;
        bindings[index].descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        bindings[index].stageFlags         = VK_SHADER_STAGE_COMPUTE_BIT;
        bindings[index].pImmutableSamplers = nullptr;
    }

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType                           = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount                    = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings                       = bindings.data();

    checkVk(vkCreateDescriptorSetLayout(device_->get(), &layoutInfo, nullptr, &layout_));
}

DescriptorSetLayout::~DescriptorSetLayout()
{
    vkDestroyDescriptorSetLayout(device_->get(), layout_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
