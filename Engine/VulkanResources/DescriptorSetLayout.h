#ifndef MINI_ENGINE_DESCRIPTOR_SET_LAYOUT_H
#define MINI_ENGINE_DESCRIPTOR_SET_LAYOUT_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class DescriptorSetLayout
{
public:
    DescriptorSetLayout() = delete;

    /**
     * @brief Create a basic descriptor set layout that can have UBOs (in vertex shader) and texture samplers (in
     * fragment shader). No arrays.
     * @param device The device on which the set layout is created
     * @param numUbos How many UBOs in the layout
     * @param numSamplers How many samplers in the layout
     */
    DescriptorSetLayout(
        const std::shared_ptr<LogicalDevice>& device, const unsigned int numUbos, const unsigned int numSamplers,
        const unsigned int numComputeStorageBuffers);

    ~DescriptorSetLayout();

    const VkDescriptorSetLayout& get() const { return layout_; }

private:
    std::shared_ptr<LogicalDevice> device_; //!< Device on which the layout is created

    VkDescriptorSetLayout layout_;                   //!< Handle to Vulkan object
    unsigned int          numUbos_;                  //!< Number of UBOs in the layout
    unsigned int          numSamplers_;              //!< Number of texture samplers in the layout
    unsigned int          numComputeStorageBuffers_; //!< Number of storage buffers used in compute shaders
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_DESCRIPTOR_SET_LAYOUT_H
