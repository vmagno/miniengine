#include "VulkanInstance.h"

#include <cstdint>
#include <cstring>
#include <iostream>
#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>

#include "Engine/Util.h"
#include "Engine/VulkanCommon.h"

namespace MiniEngine {
namespace Vk {

VulkanInstance::VulkanInstance(const bool enableValidationLayers)
    : areValidationLayersActive_(false)
{
    if (enableValidationLayers)
    {
        if (checkValidationLayerSupport())
        {
            areValidationLayersActive_ = true;
        }
        else
        {
            std::cerr << "[VulkanInstance] Validation layers are requested but not available" << std::endl;
        }
    }

    // App info needed by instance info
    VkApplicationInfo appInfo  = {};
    appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName   = "Vulkan App";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName        = "No engine";
    appInfo.engineVersion      = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion         = VK_API_VERSION_1_1;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType                = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo     = &appInfo;

    // Extensions
    auto extensions                    = getRequiredExtensions();
    createInfo.enabledExtensionCount   = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    // Layers
    createInfo.enabledLayerCount = 0;
    if (enableValidationLayers)
    {
        createInfo.enabledLayerCount   = static_cast<uint32_t>(Global::validationLayers.size());
        createInfo.ppEnabledLayerNames = Global::validationLayers.data();
    }

    // Actual instance creation
    checkVk(vkCreateInstance(&createInfo, nullptr, &instance_));

    if (!enableValidationLayers)
        return;

    if (areValidationLayersActive_)
    {
        createDebugCallback();
    }
}

VulkanInstance::~VulkanInstance()
{
    destroyDebugReportCallbackEXT();
    vkDestroyInstance(instance_, nullptr);
}

const std::vector<VkPhysicalDevice> VulkanInstance::getPhysicalDevices() const
{
    // Find out how many devices on this host support Vulkan
    uint32_t deviceCount = 0;
    checkVk(vkEnumeratePhysicalDevices(instance_, &deviceCount, nullptr));

    if (deviceCount == 0)
    {
        throw std::runtime_error("Failed to find a GPU with Vulkan support!");
    }

    std::vector<VkPhysicalDevice> devices(deviceCount); // Allocate space for the physical devices
    checkVk(vkEnumeratePhysicalDevices(instance_, &deviceCount, devices.data())); // Get the devices

    return devices;
}

bool VulkanInstance::checkValidationLayerSupport()
{
    // Find out how many layers
    uint32_t layerCount;
    checkVk(vkEnumerateInstanceLayerProperties(&layerCount, nullptr));

    // Get the layers
    std::vector<VkLayerProperties> availableLayers(layerCount);
    checkVk(vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data()));

    // Verify that all requested layers are available
    for (const char* layerName : Global::validationLayers)
    {
        bool layerFound = false;

        for (const auto& layerProperties : availableLayers)
        {
            if (std::strcmp(layerName, layerProperties.layerName) == 0)
            {
                layerFound = true;
                break;
            }
        }

        if (!layerFound)
        {
            return false;
        }
    }

    return true;
}

std::vector<const char*> VulkanInstance::getRequiredExtensions()
{
    uint32_t     glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if (areValidationLayersActive_)
    {
        extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    return extensions;
}

void VulkanInstance::createDebugCallback()
{
    VkDebugReportCallbackCreateInfoEXT createInfo = {};
    createInfo.sType                              = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags                              = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback                        = debugCallback;

    checkVk(createDebugReportCallbackEXT(&createInfo));
}

VkBool32 VulkanInstance::debugCallback(
    VkDebugReportFlagsEXT /*flags*/, VkDebugReportObjectTypeEXT /*objType*/, uint64_t /*obj*/, size_t /*location*/,
    int32_t /*code*/, const char* /*layerPrefix*/, const char* msg, void* /*userData*/)
{
    std::cerr << "[Validation layer] " << msg << std::endl;
    return VK_FALSE;
}

VkResult VulkanInstance::createDebugReportCallbackEXT(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo)
{
    // First find the function address
    auto func = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(
        vkGetInstanceProcAddr(instance_, "vkCreateDebugReportCallbackEXT"));

    if (func != nullptr)
    {
        // Call and return its return value
        return func(instance_, pCreateInfo, nullptr, &debugCallback_);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void VulkanInstance::destroyDebugReportCallbackEXT()
{
    auto func = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(
        vkGetInstanceProcAddr(instance_, "vkDestroyDebugReportCallbackEXT"));

    if (func != nullptr)
    {
        // Call it
        func(instance_, debugCallback_, nullptr);
    }
}

} // namespace Vk
} // namespace MiniEngine
