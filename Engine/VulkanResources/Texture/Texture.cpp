#include "Texture.h"

#include <stb/stb_image.h>

#include "Engine/VulkanCommon.h"
#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"

namespace MiniEngine {
namespace Vk {

Texture::Texture(const std::shared_ptr<LogicalDevice>& device)
    : Texture(device, SamplerOptions())
{
}

Texture::Texture(const std::shared_ptr<LogicalDevice>& device, const SamplerOptions& options)
    : sampler_(std::make_shared<TextureSampler>(device, options))
{
}

Texture::Texture(const std::shared_ptr<TextureSampler>& sampler)
    : sampler_(sampler)
{
}

Texture::Texture(const std::shared_ptr<LogicalDevice>& device, const std::string& texFilename)
    : Texture(device)
{
    int      texWidth, texHeight, texChannels;
    stbi_uc* pixels =
        stbi_load((Global::TEXTURE_DIR + texFilename).c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    //    VkDeviceSize imageSize = static_cast<VkDeviceSize>(texWidth * texHeight * 4);

    if (pixels == nullptr)
    {
        throw std::runtime_error("Failed to load texture image \"" + texFilename + "\"");
    }

    textureFile_ = texFilename;

    const uint32_t imageSize = static_cast<uint32_t>(texWidth * texHeight * 4);

    const auto stagingBuffer = DeviceBuffer::makeStagingBuffer(device, imageSize);
    stagingBuffer->copyData(pixels, imageSize);
    stbi_image_free(pixels);

    imageResource_ = std::make_shared<ImageResource>(
        device, glm::uvec2(texWidth, texHeight), VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT);

    imageResource_->getImage()->transitionLayout(
        VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    stagingBuffer->copyToImage(imageResource_->getImage());

    imageResource_->getImage()->transitionLayout(
        VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

} // namespace Vk
} // namespace MiniEngine
