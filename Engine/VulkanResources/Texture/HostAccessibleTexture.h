#ifndef MINI_ENGINE_HOST_ACCESSIBLE_TEXTURE_H
#define MINI_ENGINE_HOST_ACCESSIBLE_TEXTURE_H

#include "Engine/Pixel.h"
#include "VariableTexture.h"

namespace MiniEngine {
namespace Vk {

class HostAccessibleTexture : public VariableTexture
{
public:
    HostAccessibleTexture(
        const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution,
        const SamplerOptions& samplerOptions);

    std::vector<Pixel>& getEditablePixels() { return pixels_; }

private:
    std::vector<Pixel> pixels_;

private: // functions
    void doManualTextureBufferUpdate() override;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_HOST_ACCESSIBLE_TEXTURE_H
