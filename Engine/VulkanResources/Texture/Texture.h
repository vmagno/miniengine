#ifndef MINI_ENGINE_TEXTURE_H
#define MINI_ENGINE_TEXTURE_H

#include <memory>

#include "Engine/VulkanResources/Image/ImageResource.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/Texture/TextureSampler.h"

namespace MiniEngine {
namespace Vk {

class Texture
{
protected:
    Texture(const std::shared_ptr<LogicalDevice>& device);
    Texture(const std::shared_ptr<LogicalDevice>& device, const SamplerOptions& options);
    Texture(const std::shared_ptr<TextureSampler>& sampler);

public:
    Texture(const std::shared_ptr<LogicalDevice>& device, const std::string& texFilename);

    const auto& getResource() const { return imageResource_; }
    const auto& getSampler() const { return sampler_; }
    const auto& getDevice() const { return imageResource_->getDevice(); }
    const auto& getFilename() const { return textureFile_; }

protected:
    std::shared_ptr<ImageResource> imageResource_;

private:
    std::shared_ptr<TextureSampler> sampler_;
    std::string                     textureFile_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_TEXTURE_H
