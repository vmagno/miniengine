#include "VariableTexture.h"

#include "Engine/Pixel.h"
#include "Engine/VulkanResources/Image/ImageResource.h"
#include "HostAccessibleTexture.h"

namespace MiniEngine {
namespace Vk {

VariableTexture::VariableTexture(const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution)
    : VariableTexture(device, resolution, SamplerOptions())
{
}

VariableTexture::VariableTexture(
    const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution, const SamplerOptions& samplerOptions)
    : VariableTexture(
          device,
          std::make_shared<DeviceBuffer>(
              device, resolution.x * resolution.y * sizeof(Pixel),
              VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
          resolution, samplerOptions)
{
}

VariableTexture::VariableTexture(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DeviceBuffer>& textureBuffer,
    const glm::uvec2 resolution, const SamplerOptions& samplerOptions)
    : Texture(device, samplerOptions)
    , textureBuffer_(textureBuffer)
{
    imageResource_ = std::make_shared<ImageResource>(
        device, resolution, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT);
    imageResource_->getImage()->transitionLayout(
        VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    imageResource_->getImage()->transitionLayout(
        VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

VariableTexture::~VariableTexture()
{
}

std::shared_ptr<VariableTexture> VariableTexture::create(
    const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution, const SamplerOptions& samplerOptions,
    const bool deviceOnly)
{
    if (deviceOnly)
        return std::make_shared<VariableTexture>(device, resolution, samplerOptions);
    else
        return std::make_shared<HostAccessibleTexture>(device, resolution, samplerOptions);
}

void VariableTexture::updateTexture()
{
    doManualTextureBufferUpdate();
    copyBufferToImage();
}

void VariableTexture::copyBufferToImage()
{
    imageResource_->getImage()->transitionLayout(
        VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    textureBuffer_->copyToImage(imageResource_->getImage());
    imageResource_->getImage()->transitionLayout(
        VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

} // namespace Vk
} // namespace MiniEngine
