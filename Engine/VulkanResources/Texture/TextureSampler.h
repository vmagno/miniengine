#ifndef MINI_ENGINE_TEXTURE_SAMPLER_H
#define MINI_ENGINE_TEXTURE_SAMPLER_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

struct SamplerOptions
{
    VkFilter             minFilter_     = VK_FILTER_LINEAR;
    VkFilter             magFilter_     = VK_FILTER_LINEAR;
    VkSamplerAddressMode addressMode_   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    float                maxAnisotropy_ = 8;
};

class TextureSampler
{
public:
    TextureSampler() = delete;
    TextureSampler(const std::shared_ptr<LogicalDevice>& device, const SamplerOptions& options);
    ~TextureSampler();

    VkSampler get() const { return sampler_; }

private:
    std::shared_ptr<LogicalDevice> device_;  //!< Device on which the sampler is created
    VkSampler                      sampler_; //!< Handle to the sampler
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_TEXTURE_SAMPLER_H
