#include "TextureSampler.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

TextureSampler::TextureSampler(const std::shared_ptr<LogicalDevice>& device, const SamplerOptions& options)
    : device_(device)
{
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType               = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter           = options.magFilter_;
    samplerInfo.minFilter           = options.minFilter_;

    samplerInfo.addressModeU = options.addressMode_;
    samplerInfo.addressModeV = options.addressMode_;
    samplerInfo.addressModeW = options.addressMode_;

    samplerInfo.anisotropyEnable = options.maxAnisotropy_ > 0;
    samplerInfo.maxAnisotropy    = options.maxAnisotropy_;

    samplerInfo.borderColor             = VK_BORDER_COLOR_INT_OPAQUE_BLACK; // When clamping to border
    samplerInfo.unnormalizedCoordinates = VK_FALSE;                         // Coordinates between 0 and 1
    samplerInfo.compareEnable           = VK_FALSE;
    samplerInfo.compareOp               = VK_COMPARE_OP_ALWAYS;

    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod     = 0.0f;
    samplerInfo.maxLod     = 0.0f;

    checkVk(vkCreateSampler(device_->get(), &samplerInfo, nullptr, &sampler_));
}

TextureSampler::~TextureSampler()
{
    vkDestroySampler(device_->get(), sampler_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
