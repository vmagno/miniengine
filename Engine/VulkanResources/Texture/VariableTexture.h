#ifndef MINI_ENGINE_VARIABLE_TEXTURE_H
#define MINI_ENGINE_VARIABLE_TEXTURE_H

#include <vector>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"
#include "Engine/VulkanResources/Texture/Texture.h"

namespace MiniEngine {
namespace Vk {

class VariableTexture : public Texture
{
public:
    /**
     * @brief Constructor with default texture options
     * @param device Device on which the texture will reside
     * @param resolution Width and height of the texture in pixels
     */
    VariableTexture(const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution);

    /**
     * @brief Constructor with specific sampler options
     * @param device Device on which the texture will reside
     * @param resolution Width/height in pixels
     * @param samplerOptions Sampler options
     */
    VariableTexture(
        const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution,
        const SamplerOptions& samplerOptions);

    virtual ~VariableTexture();

    static std::shared_ptr<VariableTexture> create(
        const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution, const SamplerOptions& samplerOptions,
        const bool deviceOnly);

    const auto& getTextureBuffer() const { return textureBuffer_; }
    void        updateTexture();

protected:
    std::shared_ptr<DeviceBuffer> textureBuffer_;

private: // functions
    void copyBufferToImage();

    virtual void doManualTextureBufferUpdate() {}

protected: // functions
    /**
     * @brief Constructor with specific buffer and sampler options
     * @param device Device on which the texture will reside
     * @param textureBuffer Buffer where it is stored
     * @param resolution Width/height in pixels
     * @param samplerOptions Sampler options
     */
    VariableTexture(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<DeviceBuffer>& textureBuffer,
        const glm::uvec2 resolution, const SamplerOptions& samplerOptions);
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_VARIABLE_TEXTURE_H
