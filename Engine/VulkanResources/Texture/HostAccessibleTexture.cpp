#include "HostAccessibleTexture.h"

#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"

namespace MiniEngine {
namespace Vk {

HostAccessibleTexture::HostAccessibleTexture(
    const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution, const SamplerOptions& samplerOptions)
    : VariableTexture(
          device,
          std::make_shared<DeviceBuffer>(
              device, resolution.x * resolution.y * sizeof(Pixel), VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
              VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
          resolution, samplerOptions)
{
    const uint32_t numPixels = resolution.x * resolution.y;
    pixels_.resize(numPixels);

    for (uint32_t i = 0; i < numPixels; i++)
    {
        Pixel& pix = pixels_[i];

        if ((i % 2) == ((i / resolution.x) % 2))
        {
            pix = {255, 0, 0, 255};
        }
        else
        {
            pix = {0, 0, 255, 255};
        }
    }

    updateTexture();
}

void HostAccessibleTexture::doManualTextureBufferUpdate()
{
    textureBuffer_->copyData(pixels_.data(), pixels_.size() * sizeof(Pixel));
}

} // namespace Vk
} // namespace MiniEngine
