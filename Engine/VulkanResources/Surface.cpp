#include "Surface.h"

namespace MiniEngine {
namespace Vk {

Surface::~Surface()
{
    vkDestroySurfaceKHR(instance_->get(), windowSurface_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
