#ifndef MINI_ENGINE_RENDER_PASS_H
#define MINI_ENGINE_RENDER_PASS_H

#include <memory>

#include "vulkan/vulkan.h"

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class RenderPass
{
public:
    RenderPass() = delete;
    RenderPass(const std::shared_ptr<LogicalDevice>& device, const VkFormat imageFormat, const VkFormat depthFormat);
    ~RenderPass();

    VkRenderPass get() const { return renderPass_; }

private:
    std::shared_ptr<LogicalDevice> device_;
    VkRenderPass                   renderPass_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_RENDER_PASS_H
