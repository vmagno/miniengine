#ifndef MINI_ENGINE_IMAGE_RESOURCE
#define MINI_ENGINE_IMAGE_RESOURCE

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/Image/Image.h"
#include "Engine/VulkanResources/Image/ImageView.h"
#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class ImageResource
{
public:
    ImageResource() = delete;
    ImageResource(
        const std::shared_ptr<LogicalDevice>& device, const glm::uvec2& resolution, const VkFormat format,
        const VkImageTiling tiling, const VkImageUsageFlags usage, const VkMemoryPropertyFlags properties,
        const VkImageAspectFlags aspectFlags);

    inline const auto& getImage() const { return image_; }
    inline const auto& getView() const { return view_; }
    inline const auto& getDevice() const { return image_->getDevice(); }

    template <class T>
    void copyDataToImage(const T* srcData, const uint64_t size) const;

private:
    std::shared_ptr<Image>     image_;
    std::shared_ptr<ImageView> view_;
};

} // namespace Vk
} // namespace MiniEngine

#include "ImageResource.inl"

#endif // MINI_ENGINE_IMAGE_RESOURCE
