#ifndef MINI_ENGINE_IMAGE_VIEW_H
#define MINI_ENGINE_IMAGE_VIEW_H

#include <iostream>
#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class ImageView
{
public:
    ImageView() = delete;
    ImageView(
        const std::shared_ptr<LogicalDevice>& device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
    ~ImageView();

    VkImageView get() const { return view_; }

private:
    std::shared_ptr<LogicalDevice> device_;

    VkImageView view_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_IMAGE_VIEW_H
