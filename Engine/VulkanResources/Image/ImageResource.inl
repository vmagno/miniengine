#ifndef MINI_ENGINE_IMAGE_RESOURCE_INL
#define MINI_ENGINE_IMAGE_RESOURCE_INL

#include "ImageResource.h"

#include <cstring>

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

template <class T>
void ImageResource::copyDataToImage(const T* srcData, const uint64_t size) const
{
    void* dest;
    checkVk(vmaMapMemory(image_->getDevice()->getAllocator()->get(), image_->getAllocation(), &dest));
    std::memcpy(dest, srcData, static_cast<size_t>(size));
    vmaUnmapMemory(image_->getDevice()->getAllocator()->get(), image_->getAllocation());
}

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_IMAGE_RESOURCE_INL
