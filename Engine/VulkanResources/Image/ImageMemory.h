#ifndef MINI_ENGINE_DEVICE_MEMORY_H
#define MINI_ENGINE_DEVICE_MEMORY_H

#include <memory>

#include "Engine/VulkanResources/Image/Image.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/VulkanMemoryAllocator.h"

namespace MiniEngine {
namespace Vk {

/**
 * @brief Device memory associated with an image
 */
class ImageMemory
{
public:
    ImageMemory() = delete;

    /**
     * @brief Constructor: Allocate memory on the device and bind it to an image
     * @param device Device on which to allocate
     * @param image Image to which the memory will be bound
     * @param properties What kind of memory to allocate (desired properties)
     */
    ImageMemory(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<Image>& image,
        VkMemoryPropertyFlags properties);

    ~ImageMemory();

    inline VmaAllocation getAllocation() const { return allocation_; }

private:
    //    std::shared_ptr<LogicalDevice> device_;
    std::shared_ptr<VulkanMemoryAllocator> allocator_;  //!< Who allocated the memory used
    VmaAllocation                          allocation_; //!< Memory + details of the allocation
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_DEVICE_MEMORY_H
