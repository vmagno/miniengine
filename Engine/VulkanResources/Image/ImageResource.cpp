#include "ImageResource.h"

namespace MiniEngine {
namespace Vk {

ImageResource::ImageResource(
    const std::shared_ptr<LogicalDevice>& device, const glm::uvec2& resolution, const VkFormat format,
    const VkImageTiling tiling, const VkImageUsageFlags usage, const VkMemoryPropertyFlags properties,
    const VkImageAspectFlags aspectFlags)
    : image_(std::make_shared<Image>(device, resolution, format, tiling, usage, properties))
    , view_(std::make_shared<ImageView>(device, image_->get(), format, aspectFlags))
{
}

} // namespace Vk
} // namespace MiniEngine
