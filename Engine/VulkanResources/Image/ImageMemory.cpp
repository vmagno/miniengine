#include "ImageMemory.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

ImageMemory::ImageMemory(
    const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<Image>& image, VkMemoryPropertyFlags properties)
    : allocator_(device->getAllocator())
{
    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.preferredFlags          = properties;

    checkVk(vmaAllocateMemoryForImage(allocator_->get(), image->get(), &allocInfo, &allocation_, nullptr));
    checkVk(vmaBindImageMemory(allocator_->get(), allocation_, image->get()));
}

ImageMemory::~ImageMemory()
{
    vmaFreeMemory(allocator_->get(), allocation_);
}

} // namespace Vk
} // namespace MiniEngine
