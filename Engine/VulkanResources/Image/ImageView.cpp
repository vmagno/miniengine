#include "ImageView.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

ImageView::ImageView(
    const std::shared_ptr<LogicalDevice>& device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags)
    : device_(device)
    , view_(VK_NULL_HANDLE)
{
    VkImageViewCreateInfo viewInfo           = {};
    viewInfo.sType                           = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image                           = image;
    viewInfo.viewType                        = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format                          = format;
    viewInfo.subresourceRange.aspectMask     = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel   = 0;
    viewInfo.subresourceRange.levelCount     = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount     = 1;

    checkVk(vkCreateImageView(device_->get(), &viewInfo, nullptr, &view_));
}

ImageView::~ImageView()
{
    vkDestroyImageView(device_->get(), view_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
