#ifndef MINI_ENGINE_IMAGE_H
#define MINI_ENGINE_IMAGE_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/VulkanMemoryAllocator.h"

namespace MiniEngine {
namespace Vk {

class Image
{
public:
    Image() = delete;
    Image(
        const std::shared_ptr<LogicalDevice>& device, const glm::uvec2& resolution, VkFormat format,
        VkImageTiling tiling, VkImageUsageFlags usage, const VkMemoryPropertyFlags properties);
    ~Image();

    //! \name Getters
    //! @{
    inline VkImage     get() const { return image_; }
    inline const auto& getResolution() const { return resolution_; }

    inline const auto& getDevice() const { return device_; }
    inline auto        getAllocation() const { return allocation_; }
    //! @}

    void transitionLayout(VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

private:
    std::shared_ptr<LogicalDevice> device_; //!< Device on which the image resides
    VkImage                        image_;  //!< Handle to the image

    std::shared_ptr<VulkanMemoryAllocator> allocator_;  //!< Who allocated the memory used
    VmaAllocation                          allocation_; //!< Memory + details of the allocation

    glm::uvec2 resolution_; //!< Width/height in pixels
    VkFormat   format_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_IMAGE_H
