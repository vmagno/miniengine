#ifndef MINI_ENGINE_VULKAN_MEMORY_ALLOCATOR_H
#define MINI_ENGINE_VULKAN_MEMORY_ALLOCATOR_H

#include <memory>

#include "LogicalDevice.h"
#include "PhysicalDevice.h"

#ifdef __GNUC__
// Avoid tons of warnings with VMA code
#pragma GCC system_header
#endif

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include "vk_mem_alloc.h"
#pragma clang diagnostic pop

namespace MiniEngine {
namespace Vk {

class VulkanMemoryAllocator
{
public:
    VulkanMemoryAllocator() = delete;
    VulkanMemoryAllocator(
        const std::shared_ptr<LogicalDevice>& logicalDevice, const std::shared_ptr<PhysicalDevice>& physicalDevice);
    ~VulkanMemoryAllocator();

    VmaAllocator get() const { return allocator_; }

private:
    VmaAllocator allocator_; //!< Handle to the allocator
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_VULKAN_MEMORY_ALLOCATOR_H
