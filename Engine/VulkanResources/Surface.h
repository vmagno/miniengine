#ifndef MINI_ENGINE_SURFACE_H
#define MINI_ENGINE_SURFACE_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/VulkanInstance.h"

namespace MiniEngine {
namespace Vk {

/**
 * @brief Logical representation of an application's window. Can be used to query available formats, capabilities,
 * present support, present modes, etc.
 */
class Surface
{
public:
    Surface() = delete;
    Surface(const std::shared_ptr<VulkanInstance>& instance, const VkSurfaceKHR& windowSurface)
        : instance_(instance)
        , windowSurface_(windowSurface)
    {
    }
    ~Surface();

    inline const VkSurfaceKHR& get() const { return windowSurface_; }

private:
    std::shared_ptr<VulkanInstance> instance_;

    VkSurfaceKHR windowSurface_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_SURFACE_H
