#ifndef MINI_ENGINE_SWAP_CHAIN_H
#define MINI_ENGINE_SWAP_CHAIN_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/GlmInclude.h"
#include "Engine/VulkanResources/Framebuffer.h"
#include "Engine/VulkanResources/Image/ImageResource.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/RenderPass.h"

namespace MiniEngine {
namespace Vk {

/**
 * @brief A set of buffers to which we can render. These buffer must then be presented to the window for drawing.
 */
class SwapChain
{
public:
    SwapChain() = delete;
    SwapChain(const std::shared_ptr<LogicalDevice>& device, const glm::uvec2 resolution);
    ~SwapChain();

    inline VkSwapchainKHR get() const { return swapChain_; }
    inline const auto&    getDevice() const { return device_; }
    inline const auto&    getRenderPass() const { return renderPass_; }

    inline uint32_t getNumImages() const { return numImages_; }
    inline VkFormat getImageFormat() const { return imageFormat_; }
    inline VkFormat getDepthFormat() const { return depthFormat_; }
    inline uint32_t getWidth() const { return extent_.width; }
    inline uint32_t getHeight() const { return extent_.height; }

    inline VkExtent2D                          getExtent() const { return extent_; }
    inline const std::shared_ptr<Framebuffer>& getFramebuffer(const uint32_t index) const
    {
        return framebuffers_[index];
    }

    const std::shared_ptr<ImageResource>& getDepthResources() { return depthResources_; }

    void resize(const glm::uvec2 resolution);

private:
    std::shared_ptr<LogicalDevice> device_;     //!< Pointer to device for which the swap chain is created
    std::shared_ptr<Surface>       surface_;    //!< Window surface to which it will present
    std::shared_ptr<RenderPass>    renderPass_; //!< Detail about framebuffers used during rendering

    VkSwapchainKHR swapChain_; //!< Buffer infrastructure of images waiting to be presented

    std::vector<std::shared_ptr<Framebuffer>> framebuffers_; //!< To bind the image attachments from the render pass
    std::shared_ptr<ImageResource>            depthResources_;

    uint32_t   numImages_;   //!< Number of images in the swap chain
    VkFormat   imageFormat_; //!< Remember the format used when creating the swap chain
    VkExtent2D extent_;      //!< Remember the extent used when creating the swap chain
    VkFormat   depthFormat_; //!< Format used for depth buffer

private: // functions
    /// Create the swap chain itself
    void create(const glm::uvec2 resolution);

    const std::vector<std::shared_ptr<ImageView>> createImageViews();

    void createDepthResources();
    void createFramebuffers();

    /**
     * @brief Choose the format in which colors are stored in a surface
     * @param availableFormats Available formats, as queried by querySwapChainSupport()
     * @return The format to be used when creating the swap chain
     */
    static const VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);

    /**
     * @brief Choose the best available present mode.
     * @param availablePresentModes Available present modes, as queried by querySwapChainSupport()
     * @return The present mode to be used when creating the swap chain
     */
    static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);

    /**
     * @brief Choose the most appropriate extent for the swap chain (*could* be different from window resolution)
     * @param capabilities Contains current extent as well as min and max possible extents
     * @return The most appropriate extent (as far as we know)
     */
    const VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, const VkExtent2D& desiredExtent);
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_SWAP_CHAIN_H
