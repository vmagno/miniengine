#ifndef MINI_ENGINE_DESCRIPTOR_POOL_H
#define MINI_ENGINE_DESCRIPTOR_POOL_H

#include <memory>

#include <vulkan/vulkan.h>

#include "LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class DescriptorPool
{
public:
    static std::shared_ptr<DescriptorPool>
        makeGraphicsDescriptorPool(const std::shared_ptr<LogicalDevice>& device, const uint32_t numImages);
    static std::shared_ptr<DescriptorPool>
        makeComputeDescriptorPool(const std::shared_ptr<LogicalDevice>& device, const uint32_t numSets);

    DescriptorPool() = delete;
    DescriptorPool(
        const std::shared_ptr<LogicalDevice>& device, const uint32_t numImages, const uint32_t numStorageBuffers);

    ~DescriptorPool();

    VkDescriptorPool get() const { return pool_; }

private:
    std::shared_ptr<LogicalDevice> device_; //!< Device on which the pool is created
    VkDescriptorPool               pool_;   //!< Handle to the pool
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_DESCRIPTOR_POOL_H
