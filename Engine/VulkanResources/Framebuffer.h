#ifndef MINI_ENGINE_FRAMEBUFFER_H
#define MINI_ENGINE_FRAMEBUFFER_H

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/Image/ImageView.h"
#include "Engine/VulkanResources/LogicalDevice.h"
#include "Engine/VulkanResources/RenderPass.h"

namespace MiniEngine {
namespace Vk {

class Framebuffer
{
public:
    Framebuffer() = delete;

    /**
     * @brief Framebuffer
     * @param device Device on which the framebuffer will be created
     * @param renderPass Render pass associated with the buffer
     * @param swapChainImageView Image view drawn to during the render pass
     * @param depthImageView Depth buffer used when rendering (may be nullptr)
     */
    Framebuffer(
        const std::shared_ptr<LogicalDevice>& device, const std::shared_ptr<RenderPass>& renderPass,
        const uint32_t width, const uint32_t height, const std::shared_ptr<ImageView>& swapChainImageView,
        const std::shared_ptr<ImageView>& depthImageView);

    ~Framebuffer();

    VkFramebuffer get() const { return buffer_; }

private:
    std::shared_ptr<LogicalDevice>          device_;     //!< Device on which the framebuffer is created
    std::shared_ptr<RenderPass>             renderPass_; //!< Render pass associated with the framebuffer
    std::vector<std::shared_ptr<ImageView>> imageViews_; //!< Image views used as attachments by the render pass

    VkFramebuffer buffer_; //!< Handle to the Vulkan framebuffer
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_FRAMEBUFFER_H
