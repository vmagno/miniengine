#include "PhysicalDevice.h"

#include <iostream>
#include <unordered_set>

#include "Engine/Util.h"
#include "Engine/VulkanCommon.h"
#include "Engine/VulkanResources/VulkanInstance.h"

namespace MiniEngine {
namespace Vk {

PhysicalDevice::PhysicalDevice(
    const std::shared_ptr<Surface>& windowSurface, const std::shared_ptr<VulkanInstance>& instance)
    : instance_(instance)
{
    setNewSurface(windowSurface);
}

void PhysicalDevice::setNewSurface(const std::shared_ptr<Surface>& newSurface)
{
    windowSurface_  = newSurface;
    physicalDevice_ = selectDevice();
    updateQueueFamilies();
    updateSwapChainSupport();
    // TODO query and set compute capabilities
}

void PhysicalDevice::updateQueueFamilies()
{
    const auto queueFamilies = getAvailableQueueFamilyIndices(physicalDevice_);
    graphicsFamilyIndex_     = queueFamilies.graphicsFamily_;
    presentFamilyIndex_      = queueFamilies.presentFamily_;
    computeFamilyIndex_      = queueFamilies.computeFamily_;
}

void PhysicalDevice::updateSwapChainSupport()
{
    swapChainSupportDetails_ = querySwapChainSupport(physicalDevice_);
}

VkFormat PhysicalDevice::findDepthFormat()
{
    return findSupportedFormat(
        {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT}, VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

uint32_t PhysicalDevice::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const
{
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice_, &memProperties);

    // Find a suitable memory type with the specified properties
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
    {
        if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
        {
            return i;
        }
    }

    throw std::runtime_error("Failed to find suitable memory type!");
}

VkPhysicalDevice PhysicalDevice::selectDevice()
{
    const std::vector<VkPhysicalDevice> devices = instance_->getPhysicalDevices();

    //    std::cout << "[PhysicalDevice] " << devices.size() << " available Vulkan device(s)" << std::endl;

    // Choose an appropriate device
    for (const auto& device : devices)
    {
        if (isDeviceSuitable(device))
        {
            return device;
        }
    }

    throw std::runtime_error("Failed to find a suitable GPU!");
}

bool PhysicalDevice::isDeviceSuitable(const VkPhysicalDevice& device) const
{
    if (device == VK_NULL_HANDLE)
    {
        throw std::runtime_error("Device is null");
    }

    // Get properties and features
    VkPhysicalDeviceProperties deviceProperties;
    VkPhysicalDeviceFeatures   deviceFeatures;
    vkGetPhysicalDeviceProperties(device, &deviceProperties);
    vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

    const QueueFamilyIndices indices                = getAvailableQueueFamilyIndices(device);
    const bool               areExtensionsSupported = checkDeviceExtensionSupport(device);

    bool isSwapChainAdequate = false;
    if (areExtensionsSupported)
    {
        const SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
        isSwapChainAdequate = !swapChainSupport.formats_.empty() && !swapChainSupport.presentModes_.empty();
    }

    return (indices.isComplete() && areExtensionsSupported && isSwapChainAdequate && deviceFeatures.samplerAnisotropy);
}

const PhysicalDevice::QueueFamilyIndices
    PhysicalDevice::getAvailableQueueFamilyIndices(const VkPhysicalDevice& device) const
{
    // Retrieve queue family info: get count, then allocate, then get the properties
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    // Find a queue that has graphics capabilities and one with present capabilities
    QueueFamilyIndices indices;
    for (uint32_t i = 0; i < queueFamilies.size(); i++)
    {
        const auto& queueFamily = queueFamilies[i];

        if (queueFamily.queueCount > 0)
        {
            // Check whether this queue has graphics support
            if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
            {
                indices.graphicsFamily_ = i;
            }

            // Check whether the surface and queue have present support
            // We treat the graphics and present queues as separate for a more general approach, but they will
            // often be the same
            VkBool32 presentSupport = false;
            checkVk(vkGetPhysicalDeviceSurfaceSupportKHR(device, i, windowSurface_->get(), &presentSupport));
            if (presentSupport)
            {
                indices.presentFamily_ = i;
            }

            // Check for compute capability
            if (queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT)
            {
                indices.computeFamily_ = i;
            }

            if (indices.isComplete())
                break;
        }
    }

    return indices;
}

bool PhysicalDevice::checkDeviceExtensionSupport(const VkPhysicalDevice& device) const
{
    // Get list of available extensions: get count, allocate, then get properties
    uint32_t extensionCount;
    checkVk(vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr));

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    checkVk(vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data()));

    // Compile set of required extensions
    std::unordered_set<std::string> requiredExtensions(
        Global::deviceExtensions.begin(), Global::deviceExtensions.end());

    // Remove available extensions from the set of required ones, until there are no unsatisfied required extensions
    for (const auto& extension : availableExtensions)
    {
        requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
}

const PhysicalDevice::SwapChainSupportDetails
    PhysicalDevice::querySwapChainSupport(const VkPhysicalDevice& device) const
{
    SwapChainSupportDetails details;

    // Surface capabilities
    checkVk(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, windowSurface_->get(), &details.capabilities_));

    // Surface formats
    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, windowSurface_->get(), &formatCount, nullptr);

    if (formatCount > 0)
    {
        details.formats_.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, windowSurface_->get(), &formatCount, details.formats_.data());
    }

    // Surface present modes
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, windowSurface_->get(), &presentModeCount, nullptr);

    if (presentModeCount > 0)
    {
        details.presentModes_.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(
            device, windowSurface_->get(), &presentModeCount, details.presentModes_.data());
    }

    return details;
}

VkFormat PhysicalDevice::findSupportedFormat(
    const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
    for (VkFormat format : candidates)
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physicalDevice_, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
        {
            return format;
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
        {
            return format;
        }
    }

    throw std::runtime_error("Failed to find a supported format!");
}

} // namespace Vk
} // namespace MiniEngine
