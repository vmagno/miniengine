#include "ShaderModule.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

ShaderModule::ShaderModule(const std::shared_ptr<LogicalDevice>& device, const std::string& filename)
    : device_(device)
{
    const std::vector<char> shaderCode = Util::readFile(filename);

    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType                    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize                 = shaderCode.size();
    createInfo.pCode                    = reinterpret_cast<const uint32_t*>(shaderCode.data());

    checkVk(vkCreateShaderModule(device_->get(), &createInfo, nullptr, &module_));
}

ShaderModule::~ShaderModule()
{
    vkDestroyShaderModule(device_->get(), module_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
