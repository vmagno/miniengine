#ifndef MINI_ENGINE_SEMAPHORE_H
#define MINI_ENGINE_SEMAPHORE_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

/**
 * @brief Class used for device queue synchronization
 */
class Semaphore
{
public:
    Semaphore() = delete;
    Semaphore(const std::shared_ptr<LogicalDevice>& device);
    ~Semaphore();

    inline VkSemaphore get() const { return semaphore_; }

private:
    std::shared_ptr<LogicalDevice> device_;
    VkSemaphore                    semaphore_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_SEMAPHORE_H
