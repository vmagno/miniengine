#ifndef MINI_ENGINE_FENCE_H
#define MINI_ENGINE_FENCE_H

#include <memory>

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {
namespace Vk {

class Fence
{
public:
    Fence() = delete;
    Fence(const std::shared_ptr<LogicalDevice>& device, const bool signaled);
    ~Fence();

    inline VkFence get() const { return fence_; }

    void wait();
    void reset();

private:
    std::shared_ptr<LogicalDevice> device_;
    VkFence                        fence_;
};

} // namespace Vk
} // namespace MiniEngine

#endif // MINI_ENGINE_FENCE_H
