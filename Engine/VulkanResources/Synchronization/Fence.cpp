#include "Fence.h"

#include <limits>

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

Fence::Fence(const std::shared_ptr<LogicalDevice>& device, const bool signaled)
    : device_(device)
{
    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags             = signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

    checkVk(vkCreateFence(device_->get(), &fenceInfo, nullptr, &fence_));
}

Fence::~Fence()
{
    vkDestroyFence(device_->get(), fence_, nullptr);
}

void Fence::wait()
{
    checkVk(vkWaitForFences(device_->get(), 1, &fence_, VK_TRUE, std::numeric_limits<uint64_t>::max()));
}

void Fence::reset()
{
    checkVk(vkResetFences(device_->get(), 1, &fence_));
}

} // namespace Vk
} // namespace MiniEngine
