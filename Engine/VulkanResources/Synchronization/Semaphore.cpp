#include "Semaphore.h"

#include "Engine/Util.h"

namespace MiniEngine {
namespace Vk {

Semaphore::Semaphore(const std::shared_ptr<LogicalDevice>& device)
    : device_(device)
{
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    checkVk(vkCreateSemaphore(device_->get(), &semaphoreInfo, nullptr, &semaphore_));
}

Semaphore::~Semaphore()
{
    vkDestroySemaphore(device_->get(), semaphore_, nullptr);
}

} // namespace Vk
} // namespace MiniEngine
