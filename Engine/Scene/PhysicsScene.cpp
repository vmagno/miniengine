#include "PhysicsScene.h"

#include "Engine/SceneSolver/ForwardEulerSceneSolver.h"

namespace MiniEngine {

PhysicsScene::PhysicsScene()
{
    solver_ = std::make_shared<ForwardEulerSceneSolver>();
}

void PhysicsScene::animate(const float deltaS)
{
    solver_->solve(deltaS, bodies_, forceSources_);
    Scene::animate(deltaS);
}

void PhysicsScene::addBody(const std::shared_ptr<PhysicsBody>& body)
{
    bodies_.push_back(body);
    Scene::addObject(body);
}

} // namespace MiniEngine
