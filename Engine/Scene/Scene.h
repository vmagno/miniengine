#ifndef MINI_ENGINE_SCENE_H
#define MINI_ENGINE_SCENE_H

#include <memory>
#include <vector>

#include "Engine/Common.h"
#include "Engine/GlmInclude.h"

namespace MiniEngine {

class DisplayItem;
class Camera;

/**
 * @brief Current state of the world: all objects and cameras.
 */
class Scene
{
public:
    Scene() {}
    virtual ~Scene() {}

    /**
     * @brief Add an object to the scene
     * @param object Shared pointer to the object to add
     */
    inline void addObject(const std::shared_ptr<DisplayItem>& object) { objects_.push_back(object); }

    //!@{ \name Getters
    inline uint32_t getNumObjects() const { return static_cast<uint32_t>(objects_.size()); }
    inline const std::vector<std::shared_ptr<DisplayItem>>& getObjects() const { return objects_; }
    inline const std::shared_ptr<Camera>& getCamera(const uint32_t index) const { return cameras_[index]; }
    //!@}

    /**
     * @brief Add a camera to this scene
     * @param camera Shared pointer to the camera object
     */
    void addCamera(const std::shared_ptr<Camera>& camera);

    /**
     * @brief animate Animate the scene
     * @param deltaS For how long to animate (in seconds)
     */
    virtual void animate(const float deltaS);

private:
    std::vector<std::shared_ptr<DisplayItem>> objects_; //!< All items in the scene
    std::vector<std::shared_ptr<Camera>>      cameras_; //!< Cameras in the scene (also found in items)
};

} // namespace MiniEngine

#endif // MINI_ENGINE_SCENE_H
