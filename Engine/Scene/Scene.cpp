#include "Scene.h"

#include "Engine/Camera/Camera.h"
#include "Engine/DisplayItem.h"

namespace MiniEngine {

void Scene::addCamera(const std::shared_ptr<Camera>& camera)
{
    cameras_.push_back(camera);
    objects_.push_back(camera);
}

void Scene::animate(const float deltaS)
{
    for (auto item : objects_)
    {
        item->animate(deltaS);
    }
}

} // namespace MiniEngine
