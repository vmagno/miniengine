#ifndef MINI_ENGINE_PHYSICS_SCENE_H
#define MINI_ENGINE_PHYSICS_SCENE_H

#include "Engine/ForceField/ForceField.h"
#include "Engine/PhysicsBody.h"
#include "Engine/Scene/Scene.h"
#include "Engine/SceneSolver/SceneSolver.h"

namespace MiniEngine {

class PhysicsScene : public Scene
{
public:
    PhysicsScene();
    void animate(const float deltaS) override;

    void addBody(const std::shared_ptr<PhysicsBody>& body);
    void addForceSource(const std::shared_ptr<ForceField>& forceSource) { forceSources_.push_back(forceSource); }

private:
    std::vector<std::shared_ptr<PhysicsBody>> bodies_;
    std::vector<std::shared_ptr<ForceField>>  forceSources_;
    std::shared_ptr<SceneSolver>              solver_;
};

} // namespace MiniEngine

#endif // MINI_ENGINE_PHYSICS_SCENE_H
