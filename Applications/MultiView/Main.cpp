#include <iostream>
#include <stdexcept>

#include "MultiViewApplication.h"

int main(void)
{
    MiniEngine::MultiViewApplication app;

    try
    {
        app.run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}
