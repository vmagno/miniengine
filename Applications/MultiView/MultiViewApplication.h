#ifndef MULTI_VIEW_APPLICATION_H
#define MULTI_VIEW_APPLICATION_H

#include "Engine/Application.h"

namespace MiniEngine {

class MultiViewApplication : public Application
{
public:
private:
    void initScene() override;
    void initViews() override;
};

} // namespace MiniEngine

#endif // MULTI_VIEW_APPLICATION_H
