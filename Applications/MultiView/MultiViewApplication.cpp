#include "MultiViewApplication.h"

#include <vector>

#include "Engine/Camera/Camera.h"
#include "Engine/DisplayItem.inl"
#include "Engine/Scene/Scene.h"
#include "Engine/View/View.h"

namespace MiniEngine {

class SpecialDisplayItem : public DisplayItem
{
    using DisplayItem::DisplayItem;

public:
    void animate(float deltaT) override;
};

class SpecialDisplayItem2 : public DisplayItem
{
    using DisplayItem::DisplayItem;

public:
    void animate(float deltaT) override;
};

using Vertex = Vertex_col_tex;

void MultiViewApplication::initScene()
{
    const std::vector<Vertex> objectVertices = {{{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                                                {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
                                                {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
                                                {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}};

    const std::vector<uint16_t> objectIndices = {0, 1, 2, 2, 3, 0};

    const auto item0 = DisplayItem::makeWeepingAngelSquare(device_);
    const auto item1 = DisplayItem::makeWeepingAngelSquare<SpecialDisplayItem2>(device_);

    item0->setPosition(glm::vec3(0, 1, 0));
    item1->setPosition(glm::vec3(0, 0, 0));

    const auto camera = Camera::makeCamera(device_, {-1, 0, .2}, {1.0, 0, 0.0}, {0.0, 0.0, 1.0}, false);
    const auto secondCamera =
        std::make_shared<Camera>(camera->getModel(), glm::vec3(2, 0, 2), glm::vec3(-2, 0, -2), glm::vec3(0, 0, 1));
    //        std::make_shared<Camera>(camera->getModel(), camera->getVertexShader(), camera->getFragmentShader(),
    //                                 glm::vec3(1, 0, 1), Global::FORWARD + Global::UP * 1.f, Global::UP +
    //                                 Global::LEFT);
    //        std::make_shared<Camera>(camera->getModel(), camera->getVertexShader(), camera->getFragmentShader(),
    //                                 glm::vec3(1, 0, 1), Global::FORWARD + Global::LEFT * 0.1f, Global::UP);

    secondCamera->setListenToInput();

    scene_->addObject(item0);
    scene_->addObject(item1);
    scene_->addCamera(camera);
    scene_->addCamera(secondCamera);
}

void MultiViewApplication::initViews()
{
    views_.push_back(
        std::make_shared<View>(glm::vec2(0.0f, 0.0f), glm::vec2(0.8f, 0.8f), scene_, scene_->getCamera(1)));
    views_.push_back(
        std::make_shared<View>(glm::vec2(0.7f, 0.7f), glm::vec2(0.3f, 0.3f), scene_, scene_->getCamera(0)));
}

void SpecialDisplayItem::animate(float deltaT)
{
    const auto& pos = getPosition();
    if (pos.x < 1)
    {
        setPosition(glm::vec3(pos.x + deltaT, pos.y, pos.z));
    }
    else
    {
        setPosition(glm::vec3(-1, pos.y, pos.z));
    }
}

void SpecialDisplayItem2::animate(float deltaT)
{
    const auto& pos = getPosition();
    if (pos.x > -1)
    {
        setPosition(glm::vec3(pos.x - deltaT, pos.y, pos.z));
    }
    else
    {
        setPosition(glm::vec3(1, pos.y, pos.z));
    }
}

} // namespace MiniEngine
