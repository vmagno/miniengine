#ifndef RAY_TRACING_APPLICATION_H
#define RAY_TRACING_APPLICATION_H

#include "Engine/Application.h"

#include "Engine/Camera/Camera.h"
#include "Engine/VulkanResources/DeviceBuffer/UniformBuffer.h"
#include "Engine/VulkanResources/Texture/VariableTexture.h"

namespace MiniEngine {

class ComputeProgram;

class RayTracingApplication : public Application
{
private:
    std::shared_ptr<Vk::UniformBuffer> gridSizeUbo_;
    std::shared_ptr<Vk::UniformBuffer> cameraUbo_;
    std::shared_ptr<Vk::UniformBuffer> shapesUbo_;

    std::shared_ptr<Vk::DeviceBuffer> verticesBuffer_;
    std::shared_ptr<Vk::DeviceBuffer> indicesBuffer_;

    std::shared_ptr<ComputeProgram>      computeProgram_;
    std::shared_ptr<Vk::VariableTexture> displayedImage_;
    std::shared_ptr<Camera>              camera_;

    std::shared_ptr<Model> model_;

    float       shaderTime_ = 0.0f;
    const float alpha_      = 0.1f;

protected:
    void animate(const float deltaS) override;

private:
    void initScene() override;
    void initViews() override;
};

} // namespace MiniEngine

#endif // RAY_TRACING_APPLICATION_H
