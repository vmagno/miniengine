#include "RayTracingApplication.h"

int main(void)
{
    MiniEngine::RayTracingApplication app;
    app.run();

    return 0;
}
