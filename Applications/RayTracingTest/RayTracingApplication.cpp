#include "RayTracingApplication.h"

#include "Engine/ComputeProgram.h"
#include "Engine/Loader.h"
#include "Engine/Pixel.h"
#include "Engine/Scene/Scene.h"
#include "Engine/View/PreRenderedView.h"

#include "ShaderConstants.h"

// static constexpr glm::uvec2 GRID_SIZE = {160, 90};
static constexpr glm::uvec2 GRID_SIZE = {1920, 1080};

namespace MiniEngine {

struct CameraInfo
{
    glm::vec3 position_;
    float     halfHeight_;
    glm::vec4 orientation_;
    glm::vec2 screenSize_;
    glm::vec2 pixelSize_;

    CameraInfo(const glm::vec3& position, const glm::vec4& orientation, const float halfHeight)
        : position_(position)
        , halfHeight_(halfHeight)
        , orientation_(orientation)
    {
        screenSize_.y    = halfHeight_ * 2;
        const auto ratio = static_cast<float>(GRID_SIZE.x) / GRID_SIZE.y;
        screenSize_.x    = screenSize_.y * ratio;
        pixelSize_       = screenSize_ / glm::vec2(GRID_SIZE);
    }

    void print() const
    {
        std::cout << "Position: " << position_ << std::endl;
        std::cout << "Orientation: " << orientation_ << std::endl;
        std::cout << "Width: " << screenSize_.x << ", height: " << screenSize_.y << std::endl;
        std::cout << "Pixel width: " << pixelSize_.x << ", pixel height: " << pixelSize_.y << std::endl;
    }
};

struct ShapeInfo
{
    unsigned int numTriangles_   = 0;
    unsigned int triangleOffset_ = 0;
    unsigned int vertexOffset_   = 0;
    unsigned int dummy_          = 0;
};

void RayTracingApplication::animate(const float deltaS)
{
    Application::animate(deltaS);

    const auto&      camPos           = camera_->getPosition();
    const glm::vec4& orientation      = camera_->getOrientation().asVec4();
    const float      screenHalfHeight = camera_->getFovHeight() / camera_->getZoomFactor();
    CameraInfo       camInfo(camPos, orientation, screenHalfHeight);

    //    camInfo.print();

    cameraUbo_->copyData(camInfo);
    const auto time = computeProgram_->run(device_->getComputeQueue());
    displayedImage_->updateTexture();

    shaderTime_ = shaderTime_ * (1.0f - alpha_) + time * alpha_;

    std::stringstream title;
    title << "Ray marching - Potential FPS " << 1000.0f / shaderTime_;
    windowTitle_ = title.str();
}

void RayTracingApplication::initScene()
{
    camera_ = Camera::makeCamera(device_, {0.0, 0.0, 10.0}, {0.0, 0.0, -1.0}, {0.0, 1.0, 0.0}, false);
    camera_->setListenToInput(true);
    camera_->setBaseSpeed(5.f);
    scene_->addCamera(camera_);
}

void RayTracingApplication::initViews()
{
    VirtualScreenParams screenParams;
    screenParams.resolution_                = GRID_SIZE;
    screenParams.samplerOptions_.magFilter_ = VK_FILTER_LINEAR;
    screenParams.samplerOptions_.minFilter_ = VK_FILTER_LINEAR;

    const auto view = std::make_shared<PreRenderedView>(
        device_, glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), screenParams, scene_, camera_);
    views_.push_back(view);

    displayedImage_ = view->getScreenTexture();

    gridSizeUbo_ = std::make_shared<Vk::UniformBuffer>(device_, sizeof(GRID_SIZE));
    gridSizeUbo_->copyData(GRID_SIZE);

    cameraUbo_ = std::make_shared<Vk::UniformBuffer>(device_, sizeof(CameraInfo));
    shapesUbo_ = std::make_shared<Vk::UniformBuffer>(device_, sizeof(ShapeInfo));

    model_ = Loader("cube.obj").toModel(device_, true);

    ShapeInfo shapeInfo;
    shapeInfo.numTriangles_ = model_->getNumIndices() / 3;
    shapesUbo_->copyData(shapeInfo);

    std::vector<glm::vec4> vertexPositions;
    for (const auto& vert : model_->getVertices())
    {
        vertexPositions.push_back(glm::vec4(vert.position_, 1.f));
    }

    std::vector<glm::uvec4> triangles;
    const auto&             indices = model_->getIndices();
    for (uint32_t i = 0; i < indices.getNumIndices() / 3; i++)
    {
        triangles.push_back(glm::uvec4(indices[i * 3], indices[i * 3 + 1], indices[i * 3 + 2], 0));
    }

    verticesBuffer_ = std::make_shared<Vk::DeviceBuffer>(
        device_, vertexPositions.size() * sizeof(glm::vec4), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    verticesBuffer_->copyData(vertexPositions);

    indicesBuffer_ = std::make_shared<Vk::DeviceBuffer>(
        device_, triangles.size() * sizeof(glm::uvec4), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    indicesBuffer_->copyData(triangles);

    ComputeProgramParams computeParams;
    computeParams.numWorkGroups_.x = std::ceil(GRID_SIZE.x / float(WORKGROUP_SIZE));
    computeParams.numWorkGroups_.y = std::ceil(GRID_SIZE.y / float(WORKGROUP_SIZE));
    computeParams.shaderFilename_  = "RayTracing.comp.spv";
    computeParams.uniformBuffers_  = {gridSizeUbo_, cameraUbo_, shapesUbo_};
    computeParams.storageBuffers_  = {displayedImage_->getTextureBuffer(), verticesBuffer_, indicesBuffer_};

    computeProgram_ = std::make_shared<ComputeProgram>(device_, computeParams);
}

} // namespace MiniEngine
