#include "ComputeShaderApplication.h"

int main(void)
{
    MiniEngine::ComputeShaderApplication app;
    app.run();

    return 0;
}
