#ifndef COMPUTE_SHADER_APPLICATION_H
#define COMPUTE_SHADER_APPLICATION_H

#include "Engine/Application.h"

#include <vulkan/vulkan.h>

#include "Engine/VulkanResources/DeviceBuffer/DeviceBuffer.h"

namespace MiniEngine {

class ComputeProgram;

class ComputeShaderApplication : public Application
{
public:
    void run() override;

private:
    const unsigned int WIDTH          = 1920; // Size of rendered mandelbrot set.
    const unsigned int HEIGHT         = 1024; // Size of renderered mandelbrot set.
    const unsigned int WORKGROUP_SIZE = 32;   // Workgroup size in compute shader.

    // The pixels of the rendered mandelbrot set are in this format:
    struct Pixel
    {
        float r, g, b, a;
    };

    /*
     * The pipeline specifies the pipeline that all graphics and compute commands pass though in Vulkan.
     * We will be creating a simple compute pipeline in this application.
     */
    std::shared_ptr<ComputeProgram> computeProgram_;

    /*
     * The mandelbrot set will be rendered to this buffer.
     * The memory that backs the buffer is bufferMemory.
     */
    std::shared_ptr<Vk::DeviceBuffer> buffer_;

private:
    void saveRenderedImage();
};

} // namespace MiniEngine

#endif // COMPUTE_SHADER_APPLICATION_H
