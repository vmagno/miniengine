#include "ComputeShaderApplication.h"

#include <cmath>

#include "Engine/ComputeProgram.h"
#include "Engine/VulkanCommon.h"
#include "lodepng.h"

namespace MiniEngine {

void ComputeShaderApplication::run()
{
    Application::init();

    buffer_ = std::make_shared<Vk::DeviceBuffer>(
        device_, sizeof(Pixel) * WIDTH * HEIGHT, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    ComputeProgramParams params;
    params.numWorkGroups_.x = std::ceil(WIDTH / float(WORKGROUP_SIZE));
    params.numWorkGroups_.y = std::ceil(HEIGHT / float(WORKGROUP_SIZE));
    params.shaderFilename_  = "shader.comp.spv";
    params.storageBuffers_  = {buffer_};

    computeProgram_ = std::make_shared<ComputeProgram>(device_, params);
    computeProgram_->run(device_->getComputeQueue());

    // The former command rendered a mandelbrot set to a buffer.
    // Save that buffer as a png on disk.
    saveRenderedImage();
}

void ComputeShaderApplication::saveRenderedImage()
{
    Pixel* pmappedMemory;
    buffer_->copyToHost(pmappedMemory, buffer_->getSize());

    // Get the color data from the buffer, and cast it to bytes.
    // We save the data to a vector.
    std::vector<unsigned char> image;
    image.reserve(WIDTH * HEIGHT * 4);
    for (unsigned int i = 0; i < WIDTH * HEIGHT; i += 1)
    {
        image.push_back(static_cast<unsigned char>(255.0f * (pmappedMemory[i].r)));
        image.push_back(static_cast<unsigned char>(255.0f * (pmappedMemory[i].g)));
        image.push_back(static_cast<unsigned char>(255.0f * (pmappedMemory[i].b)));
        image.push_back(static_cast<unsigned char>(255.0f * (pmappedMemory[i].a)));
    }

    // Done reading, so unmap.
    buffer_->unMap();

    // Now we save the acquired color data to a .png.
    unsigned error = lodepng::encode("mandelbrot.png", image, WIDTH, HEIGHT);
    if (error)
    {
        printf("encoder error %d: %s", error, lodepng_error_text(error));
    }
}

} // namespace MiniEngine
