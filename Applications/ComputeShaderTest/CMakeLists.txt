cmake_minimum_required(VERSION 3.12)

project(ComputeShaderTest)

set(HEADER_FILES
    ComputeShaderApplication.h
    )

set(SOURCE_FILES
    Main.cpp
    ComputeShaderApplication.cpp
    lodepng.cpp
    )

set(SHADER_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/Shaders/shader.comp
    )

find_package(MiniEngine REQUIRED)

add_executable(${PROJECT_NAME} ${HEADER_FILES} ${SOURCE_FILES} ${SHADER_FILES})
target_link_libraries(${PROJECT_NAME} MiniEngine::MiniEngine)

include(${CMAKE_PREFIX_PATH}/cmake/ShaderCompile.cmake)
