#include <iostream>
#include <stdexcept>

#include "TriangleApplication.h"

int main(void)
{
    MiniEngine::TriangleApplication app;

    try
    {
        app.run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}
