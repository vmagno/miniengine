#ifndef TRIANGLE_APPLICATION_H
#define TRIANGLE_APPLICATION_H

#include "Engine/Application.h"

namespace MiniEngine {

class TriangleApplication : public Application
{
public:
private:
    void initScene() override;
};

} // namespace MiniEngine

#endif // TRIANGLE_APPLICATION_H
