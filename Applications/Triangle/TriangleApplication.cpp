#include "TriangleApplication.h"

#include <vector>

#include "Engine/DisplayItem.h"
#include "Engine/Scene/Scene.h"
#include "Engine/Vertex/Vertex_col_tex.h"
#include "Engine/VulkanCommon.h"
#include "Engine/VulkanResources/ShaderModule.h"
#include "Engine/VulkanResources/Texture/Texture.h"

namespace MiniEngine {

class SpecialDisplayItem : public DisplayItem
{
    using DisplayItem::DisplayItem;

public:
    void animate(float deltaT) override;
};

using Vertex = Vertex_col_tex;

const std::vector<Vertex> objectVertices = {{{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                                            {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
                                            {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
                                            {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}},
                                            {{-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                                            {{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
                                            {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
                                            {{-0.5f, 0.5f, -0.5f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}};

const std::vector<uint16_t> objectIndices0 = {0, 1, 2, 2, 3, 0};
const std::vector<uint32_t> objectIndices1 = {4, 5, 6, 6, 7, 4};

void TriangleApplication::initScene()
{
    const auto texture = std::make_shared<Vk::Texture>(device_, Global::TEXTURE_DIR + "weeping_angel_512.jpg");
    const auto model0  = std::make_shared<Model>(objectVertices, objectIndices0, texture);
    const auto model1  = std::make_shared<Model>(objectVertices, objectIndices1, texture);

    const auto item0 = std::make_shared<DisplayItem>(model0);
    const auto item1 = std::make_shared<SpecialDisplayItem>(model1);

    item0->setPosition(glm::vec3(0, 1, 0));
    item1->setPosition(glm::vec3(0, 0, 1));

    scene_->addObject(item0);
    scene_->addObject(item1);
}

void SpecialDisplayItem::animate(float deltaT)
{
    const auto& pos = getPosition();
    if (pos.x < 1)
    {
        setPosition(glm::vec<3, Real>(pos.x + Real(0.002f * deltaT), pos.y, pos.z));
    }
    else
    {
        setPosition(glm::vec<3, Real>(-1, pos.y, pos.z));
    }
}

} // namespace MiniEngine
