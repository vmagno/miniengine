#ifndef VARIABLE_TEXTURE_TEST_APPLICATION_H
#define VARIABLE_TEXTURE_TEST_APPLICATION_H

#include "Engine/Application.h"

#include "Engine/VulkanResources/Texture/HostAccessibleTexture.h"

namespace MiniEngine {

class VariableTextureTestApplication : public Application
{
protected:
    void animate(const float deltaS) override;

private:
    std::shared_ptr<Vk::HostAccessibleTexture> screenTexture_;

    void initScene() override;
    void initViews() override;
};

} // namespace MiniEngine

#endif // VARIABLE_TEXTURE_TEST_APPLICATION_H
