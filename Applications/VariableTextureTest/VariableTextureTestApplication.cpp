#include "VariableTextureTestApplication.h"

//#include <iostream>

#include "Engine/Camera/Camera.h"
#include "Engine/Scene/Scene.h"
#include "Engine/View/PreRenderedView.h"

namespace MiniEngine {

void VariableTextureTestApplication::animate(const float deltaS)
{
    if (screenTexture_ == nullptr)
    {
        screenTexture_ = std::dynamic_pointer_cast<Vk::HostAccessibleTexture>(
            std::dynamic_pointer_cast<PreRenderedView>(views_[0])->getScreenTexture());
    }

    auto& pixels = screenTexture_->getEditablePixels();

    Pixel newPix = pixels[0];
    if (newPix.r < 255)
        newPix.r += std::ceil(255 * deltaS);
    else
        newPix.r = 0;

    newPix.b  = 255 - newPix.r;
    pixels[0] = newPix;

    // std::cerr << "UPDATING" << std::endl;
    //    std::dynamic_pointer_cast<RayTracedView>(views_[0])->updateScreen();
    screenTexture_->updateTexture();
}

void VariableTextureTestApplication::initScene()
{
    const auto camera = Camera::makeCamera(device_, {-1, 0, .2}, {1.0, 0, 0.0}, {0.0, 0.0, 1.0}, false);
    scene_->addCamera(camera);
}

void VariableTextureTestApplication::initViews()
{
    VirtualScreenParams params;
    params.resolution_ = {4, 4};
    params.deviceOnly_ = false;
    const auto view    = std::make_shared<PreRenderedView>(
        device_, glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), params, scene_, scene_->getCamera(0));
    views_.push_back(view);
    screenTexture_ = view->getHostScreenTexture();
}

} // namespace MiniEngine
