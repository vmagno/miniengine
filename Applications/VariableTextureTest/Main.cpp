#include <iostream>
#include <stdexcept>

#include "VariableTextureTestApplication.h"

int main(void)
{
    MiniEngine::VariableTextureTestApplication app;

    try
    {
        app.run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}
