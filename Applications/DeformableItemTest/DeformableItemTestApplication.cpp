#include "DeformableItemTestApplication.h"

#include "Engine/Camera/Camera.h"
#include "Engine/DisplayItem.h"
#include "Engine/Loader.h"
#include "Engine/Quaternion.h"
#include "Engine/Scene/Scene.h"

namespace MiniEngine {

class RotatingItem : public DisplayItem
{
    using DisplayItem::DisplayItem;

public:
    void animate(const float dt) override;
};

void DeformableItemTestApplication::initScene()
{
    auto camera = Camera::makeCamera(device_, {0.0, 0.0, 10.0}, {0.0, 0.0, -1.0}, {0.0, 1.0, 0.0}, false);
    camera->setListenToInput(true);
    scene_->addCamera(camera);

    auto angel = DisplayItem::makeWeepingAngelSquare(device_);
    angel->setPosition({2.f, 0.f, 0.f});

    cube_ = Model::makeCube(device_, true);

    scene_->addObject(DisplayItem::makeAxisSet(device_));
    scene_->addObject(std::make_shared<RotatingItem>(cube_));
    scene_->addObject(angel);

    const auto angelModel    = angel->getModel();
    const auto angelDynModel = angelModel;

    if (angelDynModel == nullptr)
    {
        std::cerr << "ERRRRRROOOOOOORRRRRRR" << std::endl;
    }

    Loader     loader("cube.obj");
    const auto m = loader.toModel(device_);
    scene_->addObject(std::make_shared<DisplayItem>(m, glm::vec3(2.f)));
}

void DeformableItemTestApplication::animate(const float dt)
{
    Application::animate(dt);

    static glm::vec3 position  = glm::vec3(-1, -1, -1);
    static glm::vec3 increment = glm::vec3(0.01f, 0.01f, 0.01f);

    if (glm::length(position) > 2.0f)
        increment = glm::vec3(-0.01f, -0.01f, -0.01f);
    else if (glm::length(position) < 0.8f)
        increment = glm::vec3(0.01f, 0.01f, 0.01f);

    position -= increment;

    cube_->moveVertex(0, position);
}

void RotatingItem::animate(const float dt)
{
    static auto baseRotation = Util::Quaternion<Real>::fromEulerAngles(
        glm::radians(Real(180)), glm::radians(Real(90)), glm::radians(Real(45)));
    rotate(baseRotation * dt * 0.2f);
}

} // namespace MiniEngine
