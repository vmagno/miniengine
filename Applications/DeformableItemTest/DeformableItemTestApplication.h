#ifndef DEFORMABLE_ITEM_TEST_APPLICATION_H
#define DEFORMABLE_ITEM_TEST_APPLICATION_H

#include "Engine/Application.h"
#include "Engine/Model/Model.h"

namespace MiniEngine {

class DeformableItemTestApplication : public Application
{
private:
    std::shared_ptr<Model> cube_;

private:
    void initScene() override;
    void animate(const float dt) override;
};

} // namespace MiniEngine

#endif // DEFORMABLE_ITEM_TEST_APPLICATION_H
