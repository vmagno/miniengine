
#include "DeformableItemTestApplication.h"

int main(void)
{
    auto app = std::make_shared<MiniEngine::DeformableItemTestApplication>();
    app->run();

    return 0;
}
