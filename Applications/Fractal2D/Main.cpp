#include "Fractal2dApplication.h"

int main(void)
{
    MiniEngine::Fractal2dApplication app;
    app.run();

    return 0;
}
