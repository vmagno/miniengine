#ifndef FRACTAL_2D_APPLICATION_H
#define FRACTAL_2D_APPLICATION_H

#include "Engine/Application.h"

#include "Engine/VulkanResources/Texture/VariableTexture.h"
#include "Engine/VulkanResources/DeviceBuffer/UniformBuffer.h"
#include "Engine/Camera/Camera.h"

namespace MiniEngine {

class ComputeProgram;

class Fractal2dApplication : public Application
{
private:
    std::shared_ptr<Vk::UniformBuffer>   gridSizeUbo_;
    std::shared_ptr<Vk::UniformBuffer>   cameraUbo_;
    std::shared_ptr<ComputeProgram>      computeProgram_;
    std::shared_ptr<Vk::VariableTexture> displayedImage_;
    std::shared_ptr<Camera> camera_;

protected:
    void animate(const float deltaS) override;

private:
    void initScene() override;
    void initViews() override;
};

} // namespace MiniEngine

#endif // FRACTAL_2D_APPLICATION_H
