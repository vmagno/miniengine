#include "Fractal2dApplication.h"

#include "Engine/ComputeProgram.h"
#include "Engine/Pixel.h"
#include "Engine/Scene/Scene.h"
#include "Engine/View/PreRenderedView.h"

#include "ShaderConstants.h"

static const uint32_t WIDTH  = 1920;
static const uint32_t HEIGHT = 1080;

namespace MiniEngine {

struct CameraPosition
{
    float x_;
    float y_;
    float zoomFactor_ = 1.0f;

    CameraPosition(const float x, const float y, const float zoomFactor)
        : x_(x)
        , y_(y)
        , zoomFactor_(zoomFactor)
    {
    }
};

void Fractal2dApplication::animate(const float deltaS)
{
    Application::animate(deltaS);

    const auto     camPosLong = camera_->getPosition();
    CameraPosition camPos(camPosLong[0], camPosLong[1], camera_->getZoomFactor());

    cameraUbo_->copyData(&camPos, sizeof(camPos));
    computeProgram_->run(device_->getComputeQueue());
    displayedImage_->updateTexture();
}

void Fractal2dApplication::initScene()
{
    camera_ = Camera::makeCamera(device_, {0.0, 0.0, 0.0}, {0.0, 0.0, -1.0}, {0.0, 1.0, 0.0}, true);
    camera_->setListenToInput(true);
    camera_->setBaseSpeed(1.0f);
    camera_->setPositionLimits({-0.414193, -0.446943, -0.5}, {0.391531, 0.446943, 0.5});
    scene_->addCamera(camera_);
}

void Fractal2dApplication::initViews()
{
    VirtualScreenParams params;
    params.resolution_ = {WIDTH, HEIGHT};
    const auto view    = std::make_shared<PreRenderedView>(
        device_, glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), params, scene_, scene_->getCamera(0));
    views_.push_back(view);

    displayedImage_ = view->getScreenTexture();

    struct
    {
        uint width_;
        uint height_;
    } gridSize;

    gridSizeUbo_ = std::make_shared<Vk::UniformBuffer>(device_, sizeof(gridSize));
    cameraUbo_   = std::make_shared<Vk::UniformBuffer>(device_, sizeof(CameraPosition));

    ComputeProgramParams programParams;
    programParams.numWorkGroups_.x = std::ceil(WIDTH / float(WORKGROUP_SIZE));
    programParams.numWorkGroups_.y = std::ceil(HEIGHT / float(WORKGROUP_SIZE));
    programParams.shaderFilename_  = "Mandelbrot.comp.spv";
    programParams.uniformBuffers_  = {gridSizeUbo_, cameraUbo_};
    programParams.storageBuffers_  = {displayedImage_->getTextureBuffer()};

    computeProgram_ = std::make_shared<ComputeProgram>(device_, programParams);

    gridSize.width_  = WIDTH;
    gridSize.height_ = HEIGHT;

    const auto&    camPos = camera_->getPosition();
    CameraPosition cameraPosition(camPos.x, camPos.y, camera_->getZoomFactor());

    gridSizeUbo_->copyData(&gridSize, sizeof(gridSize));
    cameraUbo_->copyData(&cameraPosition, sizeof(cameraPosition));
}

} // namespace MiniEngine
