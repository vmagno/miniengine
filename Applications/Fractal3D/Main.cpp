#include "Fractal3dApplication.h"

int main(void)
{
    MiniEngine::Fractal3dApplication app;
    app.run();

    return 0;
}
