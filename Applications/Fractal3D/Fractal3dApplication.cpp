#include "Fractal3dApplication.h"

#include "Engine/ComputeProgram.h"
#include "Engine/Pixel.h"
#include "Engine/Scene/Scene.h"
#include "Engine/View/PreRenderedView.h"

#include "ShaderConstants.h"

static const uint32_t WIDTH  = 800;
static const uint32_t HEIGHT = 450;

namespace MiniEngine {

struct CameraInfo
{
    glm::vec3 position_;
    float     halfHeight_;
    glm::vec4 orientation_;
    float     fullHeight_;
    float     width_;
    float     pixelHeight_;
    float     pixelWidth_;

    CameraInfo(const glm::vec3& position, const glm::vec4& orientation, const float halfHeight)
        : position_(position)
        , halfHeight_(halfHeight)
        , orientation_(orientation)
    {
        fullHeight_      = halfHeight_ * 2;
        const auto ratio = static_cast<float>(WIDTH) / HEIGHT;
        width_           = fullHeight_ * ratio;
        pixelWidth_      = width_ / WIDTH;
        pixelHeight_     = fullHeight_ / HEIGHT;
    }

    void print() const
    {
        std::cout << "Position: " << position_ << std::endl;
        std::cout << "Orientation: " << orientation_ << std::endl;
        std::cout << "Width: " << width_ << ", height: " << fullHeight_ << std::endl;
        std::cout << "Pixel width: " << pixelWidth_ << ", pixel height: " << pixelHeight_ << std::endl;
    }
};

void Fractal3dApplication::animate(const float deltaS)
{
    Application::animate(deltaS);

    const auto&      camPos           = camera_->getPosition();
    const glm::vec4& orientation      = camera_->getOrientation().asVec4();
    const float      screenHalfHeight = camera_->getFovHeight() / camera_->getZoomFactor();
    CameraInfo       camInfo(camPos, orientation, screenHalfHeight);

    //    camInfo.print();

    cameraUbo_->copyData(camInfo);
    const auto time = computeProgram_->run(device_->getComputeQueue());
    displayedImage_->updateTexture();

    shaderTime_ = shaderTime_ * (1.0f - alpha_) + time * alpha_;

    std::stringstream title;
    title << "Ray marching - Potential FPS " << 1000.0f / shaderTime_;
    windowTitle_ = title.str();
}

void Fractal3dApplication::initScene()
{
    camera_ = Camera::makeCamera(device_, {0.0, 0.0, 10.0}, {0.0, 0.0, -1.0}, {0.0, 1.0, 0.0}, false);
    camera_->setListenToInput(true);
    //    camera_->setBaseSpeed(1.0f);
    //    camera_->setPositionLimits({-0.414193, -0.446943, -0.5}, {0.391531, 0.446943, 0.5});
    scene_->addCamera(camera_);
}

void Fractal3dApplication::initViews()
{
    VirtualScreenParams params;

    params.resolution_                = {WIDTH, HEIGHT};
    params.samplerOptions_.magFilter_ = VK_FILTER_LINEAR;
    params.samplerOptions_.minFilter_ = VK_FILTER_LINEAR;

    const auto view = std::make_shared<PreRenderedView>(
        device_, glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), params, scene_, camera_);
    views_.push_back(view);

    displayedImage_ = view->getScreenTexture();

    struct
    {
        uint width_;
        uint height_;
    } gridSize;

    gridSizeUbo_ = std::make_shared<Vk::UniformBuffer>(device_, sizeof(gridSize));
    cameraUbo_   = std::make_shared<Vk::UniformBuffer>(device_, sizeof(CameraInfo));

    ComputeProgramParams programParams;
    programParams.numWorkGroups_.x = std::ceil(WIDTH / float(WORKGROUP_SIZE));
    programParams.numWorkGroups_.y = std::ceil(HEIGHT / float(WORKGROUP_SIZE));
    programParams.shaderFilename_  = "RayMarching.comp.spv";
    programParams.uniformBuffers_  = {gridSizeUbo_, cameraUbo_};
    programParams.storageBuffers_  = {displayedImage_->getTextureBuffer()};

    computeProgram_ = std::make_shared<ComputeProgram>(device_, programParams);

    gridSize.width_  = WIDTH;
    gridSize.height_ = HEIGHT;

    //    const auto&    camPos = camera_->getPosition();
    //    CameraPosition cameraPosition(camPos.x, camPos.y, camera_->getZoomFactor());

    gridSizeUbo_->copyData(gridSize);
    //    cameraUbo_->copyData(&cameraPosition, sizeof(cameraPosition));
}

} // namespace MiniEngine
