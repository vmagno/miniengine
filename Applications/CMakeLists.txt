cmake_minimum_required(VERSION 3.12)

project(MiniEngineApplications)

#add_subdirectory(Triangle)
add_subdirectory(ComputeShaderTest)
add_subdirectory(DeformableItemTest)
add_subdirectory(Fractal2D)
add_subdirectory(Fractal3D)
#add_subdirectory(MultiView)
add_subdirectory(RayTracingTest)
#add_subdirectory(PhysicsSceneTest)
#add_subdirectory(SolarSystem)
add_subdirectory(VariableTextureTest)
