#include "SolarSystem.h"

#include "Engine/DisplayItem.inl"
#include "SpaceBody.h"

namespace MiniEngine {

void SolarSystem::animate(const float deltaS)
{
    Scene::animate(deltaS);

    availableTime_ += deltaS;

    while (availableTime_ > deltaT_)
    {
        availableTime_ -= deltaT_;

        // Symplectic Euler
        {
            const auto deltaV = getForce(earth_->getPosition(), sun_->getPosition()) * deltaT_;
            earth_->setVelocity(earth_->getVelocity() + deltaV);
            earth_->translate(earth_->getVelocity() * deltaT_);
        }

        // Basic forward Euler
        {
            const auto deltaV = getForce(forwardEuler_->getPosition(), sun_->getPosition()) * deltaT_;
            forwardEuler_->translate(forwardEuler_->getVelocity() * deltaT_);
            forwardEuler_->setVelocity(forwardEuler_->getVelocity() + deltaV);
        }

        // Backward Euler (too much numerical damping...)
        {
            glm::vec3 v_newton;
            glm::vec3 x_newton;

            {
                const auto x_0 = backwardEuler_->getPosition();
                const auto v_0 = backwardEuler_->getVelocity();

                auto x_guess = x_0;
                auto v_guess = v_0;

                uint32_t iNewton;
                for (iNewton = 0; iNewton < 5; iNewton++)
                {
                    const auto      K        = getForceDeriv(x_guess, sun_->getPosition());
                    const glm::mat3 jacobian = glm::mat3(1.f) - deltaT_ * deltaT_ * K;
                    const glm::vec3 value    = v_guess - v_0 - deltaT_ * getForce(x_guess, sun_->getPosition());

                    const auto correction = -jacobian * value;
                    v_guess               = v_guess + correction;
                    x_guess               = x_0 + deltaT_ * v_guess;

                    if (glm::length(value) < 2e-6f || glm::length(correction) < 2e-6f)
                        break;
                }

                x_newton = x_guess;
                v_newton = v_guess;
            }

            const auto f_0 = getForce(backwardEuler_->getPosition(), sun_->getPosition());
            const auto v_0 = backwardEuler_->getVelocity();

            //            auto v_next = v_0;
            //            auto x_next = backwardEuler_->getPosition();
            //            for (uint32_t iNewton = 0; iNewton < 10; iNewton++)
            //            {
            //                const auto      K        = getForceDeriv(x_next, sun_->getPosition());
            const auto      K        = getForceDeriv(backwardEuler_->getPosition(), sun_->getPosition());
            const glm::mat3 jacobian = glm::mat3(1.f) - deltaT_ * deltaT_ * K;
            const auto      deltaV   = glm::inverse(jacobian) * deltaT_ * (f_0 + deltaT_ * (K * v_0));
            //                v_next += deltaV;
            //                x_next += v_next * deltaT_;
            //            }

            //            backwardEuler_->setVelocity(v_next);
            //            backwardEuler_->setPosition(x_next);
            backwardEuler_->setVelocity(backwardEuler_->getVelocity() + deltaV);
            backwardEuler_->translate(backwardEuler_->getVelocity() * deltaT_);

            //            backwardEuler_->setVelocity(v_newton);
            //            backwardEuler_->setPosition(x_newton);
            std::cout << "Velocity diff: " << glm::length(v_newton - backwardEuler_->getVelocity())
                      << ", deltaV: " << glm::length(deltaV) << std::endl;
        }
    }
}

void SolarSystem::create(const std::shared_ptr<Vk::LogicalDevice>& device)
{
    sun_ = DisplayItem::makeSphere<SpaceBody>(device, glm::vec3(1.f, 1.f, 0.f));
    sun_->setPolygonMode(PolygonMode::line);
    sun_->setScale(0.7f);

    const glm::vec3 startingPos(1.f, 0.f, 0.f);

    earth_ = DisplayItem::makeSphere<SpaceBody>(device, glm::vec3(0.f, 0.f, 1.f));
    earth_->setScale(0.3f);
    earth_->setPosition(startingPos);
    earth_->setVelocity(glm::vec3(0.0f, 0.0f, 1.0f));

    forwardEuler_ = DisplayItem::makeSphere<SpaceBody>(device, glm::vec3(0.5f, 0.5f, 0.0f));
    forwardEuler_->setScale(0.3f);
    forwardEuler_->setPosition(startingPos);
    forwardEuler_->setVelocity(glm::vec3(0.0f, 0.0f, 1.0f));

    backwardEuler_ = DisplayItem::makeSphere<SpaceBody>(device, glm::vec3(0.0f, 1.0f, 0.0f));
    backwardEuler_->setScale(0.3f);
    backwardEuler_->setPosition(startingPos);
    backwardEuler_->setVelocity(glm::vec3(0.0f, 0.0f, 1.0f));

    addObject(sun_);
    addObject(earth_);
    addObject(forwardEuler_);
    addObject(backwardEuler_);
}

const glm::vec3 SolarSystem::getForce(const glm::vec3& planetPos, const glm::vec3& sunPos)
{
    const auto  posVec = planetPos - sunPos;
    const float dist   = glm::length(posVec);

    const auto force = -posVec / (dist * dist * dist);

    return force;
}

const glm::mat3 SolarSystem::getForceDeriv(const glm::vec3& planetPos, const glm::vec3& sunPos)
{
    // With finite differences
    //    const float EPSILON = 1e-3f;

    //    const auto fxp = getForce(planetPos + glm::vec3(EPSILON, 0.f, 0.f), sunPos);
    //    const auto fxn = getForce(planetPos - glm::vec3(EPSILON, 0.f, 0.f), sunPos);

    //    const auto fyp = getForce(planetPos + glm::vec3(0.f, EPSILON, 0.f), sunPos);
    //    const auto fyn = getForce(planetPos - glm::vec3(0.f, EPSILON, 0.f), sunPos);

    //    const auto fzp = getForce(planetPos + glm::vec3(0.f, 0.f, EPSILON), sunPos);
    //    const auto fzn = getForce(planetPos - glm::vec3(0.f, 0.f, EPSILON), sunPos);

    //    const auto dfdx = (fxp - fxn) / (2 * EPSILON);
    //    const auto dfdy = (fyp - fyn) / (2 * EPSILON);
    //    const auto dfdz = (fzp - fzn) / (2 * EPSILON);

    //    glm::mat3 result(dfdx[0], dfdy[0], dfdz[0], dfdx[1], dfdy[1], dfdz[1], dfdx[2], dfdy[2], dfdz[2]);

    // Analytically
    const glm::vec3 dispVec = sunPos - planetPos;
    const float     dist    = glm::length(dispVec);

    const float x      = -dispVec.x;
    const float y      = -dispVec.y;
    const float z      = -dispVec.z;
    const float dist_3 = dist * dist * dist;
    const float dist_5 = dist_3 * dist * dist;

    const float dfxdx = 3.0f * x * x / dist_5 - 1.0f / dist_3;
    const float dfxdy = 3.0f * y * x / dist_5;
    const float dfxdz = 3.0f * z * x / dist_5;

    const float dfydx = 3.0f * x * y / dist_5;
    const float dfydy = 3.0f * y * y / dist_5 - 1.0f / dist_3;
    const float dfydz = 3.0f * z * y / dist_5;

    const float dfzdx = 3.0f * x * z / dist_5;
    const float dfzdy = 3.0f * y * z / dist_5;
    const float dfzdz = 3.0f * z * z / dist_5 - 1.0f / dist_3;

    const glm::mat3 result(dfxdx, dfxdy, dfxdz, dfydx, dfydy, dfydz, dfzdx, dfzdy, dfzdz);

    return result;
}

} // namespace MiniEngine
