#include "SolarSystemApplication.h"

#include "Engine/Camera/Camera.h"
#include "Engine/DisplayItem.inl"
#include "Engine/View/View.h"
#include "SolarSystem.h"

namespace MiniEngine {

void SolarSystemApplication::initScene()
{
    auto solarSystem = std::make_shared<SolarSystem>();
    scene_           = solarSystem;

    const auto camera =
        Camera::makeCamera(device_, 5.f * (Global::BACK + Global::UP), Global::FORWARD - Global::UP, Global::UP, false);

    solarSystem->create(device_);

    scene_->addCamera(camera);
    scene_->addObject(DisplayItem::makeAxisSet(device_));
}

void SolarSystemApplication::initViews()
{
    views_.push_back(std::make_shared<View>(glm::vec2(0, 0), glm::vec2(1, 1), scene_, scene_->getCamera(0)));
}

} // namespace MiniEngine
