#ifndef SOLAR_SYSTEM_APPLICATION_H
#define SOLAR_SYSTEM_APPLICATION_H

#include "Engine/Application.h"

namespace MiniEngine {

class SolarSystemApplication : public Application
{
    using Super = Application;

    void initScene() override;
    void initViews() override;
};

} // namespace MiniEngine

#endif // SOLAR_SYSTEM_APPLICATION_H
