#include "SpaceBody.h"

namespace MiniEngine {

void SpaceBody::animate(const float deltaS)
{
    rotate(Util::Quaternion<Real>::fromAngleAxis(glm::radians(rotationSpeed_) * deltaS, Global::UP));
}

} // namespace MiniEngine
