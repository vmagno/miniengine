#include <iostream>
#include <stdexcept>

#include "SolarSystemApplication.h"

int main(void)
{
    MiniEngine::SolarSystemApplication app;

    try
    {
        app.run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}
