#ifndef SPACE_BODY_H
#define SPACE_BODY_H

#include "Engine/DisplayItem.h"

namespace MiniEngine {

class SpaceBody : public DisplayItem
{
    using DisplayItem::DisplayItem;

public:
    void animate(const float deltaS) override;

    const glm::vec3& getVelocity() const { return velocity_; }
    void             setVelocity(const glm::vec3& newVelocity) { velocity_ = newVelocity; }

private:
    glm::vec3 velocity_      = glm::vec3(0.0f);
    float     rotationSpeed_ = 90.f;
};

} // namespace MiniEngine

#endif // SPACE_BODY_H
