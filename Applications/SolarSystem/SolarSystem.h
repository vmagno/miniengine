#ifndef SOLAR_SYSTEM_H
#define SOLAR_SYSTEM_H

#include <memory>

#include "Engine/Scene/Scene.h"
#include "Engine/VulkanResources/LogicalDevice.h"

namespace MiniEngine {

class SpaceBody;

class SolarSystem : public Scene
{
public:
    void animate(const float deltaS) override;
    void create(const std::shared_ptr<Vk::LogicalDevice>& device);

private:
    std::shared_ptr<SpaceBody> sun_;
    std::shared_ptr<SpaceBody> earth_;
    std::shared_ptr<SpaceBody> forwardEuler_;
    std::shared_ptr<SpaceBody> backwardEuler_;

    float availableTime_ = 0.0f;
    float deltaT_        = 1.0f / 60;

private: // functions
    const glm::vec3 getForce(const glm::vec3& planetPos, const glm::vec3& sunPos);
    const glm::mat3 getForceDeriv(const glm::vec3& planetPos, const glm::vec3& sunPos);
};

} // namespace MiniEngine

#endif // SOLAR_SYSTEM_H
