
#include "PhysicsSceneTestApplication.h"

int main(void)
{
    auto app = std::make_shared<MiniEngine::PhysicsSceneTestApplication>();
    app->run();

    return 0;
}
