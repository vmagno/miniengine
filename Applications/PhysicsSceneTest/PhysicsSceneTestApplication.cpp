#include "PhysicsSceneTestApplication.h"

#include "Engine/Camera/Camera.h"
#include "Engine/ForceField/Gravity.h"
#include "Engine/ForceField/PlaneForceField.h"
#include "Engine/Model/Model.h"

namespace MiniEngine {

void PhysicsSceneTestApplication::initScene()
{
    physicsScene_ = std::make_shared<PhysicsScene>();
    scene_        = physicsScene_;

    auto camera = Camera::makeCamera(device_, {0.0, 10.0, 10.0}, {0.0, -1.0, -1.0}, {0.0, 1.0, 0.0}, false);
    camera->setListenToInput(true);
    scene_->addCamera(camera);

    const auto cubeModel = Model::makeCube(device_);
    auto       body      = std::make_shared<PhysicsBody>(cubeModel);
    body->setMass(1.0f);
    physicsScene_->addBody(body);

    physicsScene_->addForceSource(std::make_shared<Gravity>());
    physicsScene_->addForceSource(std::make_shared<PlaneForceField>(glm::vec3(0, -2, 0), glm::vec3(0, 1, 0), 10.f));
}

} // namespace MiniEngine
