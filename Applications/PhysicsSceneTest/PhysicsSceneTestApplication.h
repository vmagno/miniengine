#ifndef PHYSICS_SCENE_TEST_APPLICATION_H
#define PHYSICS_SCENE_TEST_APPLICATION_H

#include "Engine/Application.h"
#include "Engine/Scene/PhysicsScene.h"

namespace MiniEngine {

class PhysicsSceneTestApplication : public Application
{
private:
    std::shared_ptr<PhysicsScene> physicsScene_;

private:
    void initScene() override;
};

} // namespace MiniEngine

#endif // PHYSICS_SCENE_TEST_APPLICATION_H
